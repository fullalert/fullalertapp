const { spawn } = require('child_process');


class ProcessHandler {
    constructor(verbose = true) {
        this.verbose = verbose === true;
    }

    log() {
        if (this.verbose) {
            console.log.apply(console, arguments);
        }
    };

    run(command, commandArgs, opts = {}) {
        return new Promise((success, fail) => {
            const cordova = spawn(command, commandArgs, opts);
            let output = '';
            let err_output = '';
            cordova.stdout.on('data', (data) => {
                this.log(`stdout: ${data}`);
                output += data;
            });

            cordova.stderr.on('data', (data) => {
                this.log(`stderr: ${data}`);
                err_output += data;
            });

            cordova.on('close', (code) => {
                this.log(`child process exited with code ${code}`);
                if (code === 0) {
                    success(output);
                }
                else {
                    fail(new Error(err_output));
                }
            });
        });
    }
}

module.exports = {
    ProcessHandler
};