import '../styles.scss';
import React, { Component } from 'react';
// import { List, ListItem, Subheader } from 'material-ui';
import { ComingSoon } from 'js/components/app/ComingSoon';

class AchievementsLog extends Component {
  render() {
    const { palette /* , dictionary */ } = this.props;
    const style = { color: palette.textColor };

    return (
      <div className="achievements-content" style={style}>
        <div className="achievements-log">
          <ComingSoon />
        </div>
      </div>
    );
  }
}

export default AchievementsLog;
