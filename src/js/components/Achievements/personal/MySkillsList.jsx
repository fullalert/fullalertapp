import React, { Component } from 'react';
import { List, FontIcon, ListItem, Divider /* , Subheader */ } from 'material-ui';
import decamelize from 'decamelize';

const translPrefix = '_gm_skitem_';

const SKILL_ICONS_MAP = {
  learner: 'school',
  reporter: 'bullhorn',
  reviewer: 'thumb-up'
};

function getInnerLabel(statName, stats, dictionary) {
  const pointValue = stats[`${statName}Points`];

  const countValue = stats[`${statName}Count`];
  const sign = pointValue > 0 ? '+' : '';
  const countValueString = typeof countValue === 'number' ? `${countValue}` : '';

  const pointValueString = typeof pointValue === 'number' ? `(${sign}${pointValue} pts)` : '';
  const dictKey = `${translPrefix}${decamelize(statName, ' ')
    .split(' ')
    .join('_')}`;

  return (
    <div className="inner-label">
      <span>{dictionary(dictKey)}</span>
      <b>{`\u00A0${countValueString} ${pointValueString}`}</b>
    </div>
  );
}

const getUniqueSkillNames = skillNames =>
  Array.from(new Set(skillNames.map(sn => sn.replace(/Count|Points/, ''))));

const SkillsListItem = ({ stats, skillName, dictionary }) => [
  <ListItem
    key={skillName}
    leftIcon={<FontIcon className={`mdi mdi-${SKILL_ICONS_MAP[skillName]}`} />}
    className="skills-list-item"
    primaryText={dictionary(`_gm_skheader_${skillName}`).toUpperCase()}
    secondaryText={`${stats['totalPoints']} pts`}
    initiallyOpen={false}
    primaryTogglesNestedList={true}
    nestedItems={getUniqueSkillNames(Object.keys(stats))
      .sort()
      .map(statName => (
        <ListItem
          key={statName}
          className="skills-list-item-inner"
          primaryText={getInnerLabel(statName, stats, dictionary)}
        />
      ))}
  />,
  <Divider key={skillName + '-div'} inset={true} />
];

class MySkillsList extends Component {
  render() {
    const { skills, palette, dictionary } = this.props;
    const skillsItems = Object.keys(skills).filter(s => s !== 'socializer');
    // console.log('MySkillsList', skills, skillsItems, palette);

    // const allLabels = Array.from(
    //   new Set(
    //     skillsItems
    //       .map(skillName =>
    //         Object.keys(skills[skillName])
    //           .map(statName => getInnerLabelTXT(statName))
    //           .filter(str => str !== null)
    //           .sort()
    //       )
    //       .reduce((prev, next) => [...prev, ...next], [])
    //   )
    // ).join('\n');

    // console.log(allLabels);

    const style = { color: palette.textColor };

    if (skillsItems.length === 0) {
      return (
        <div className="skills-list" style={style}>
          {`${dictionary('_loading')}...`}
        </div>
      );
    } else {
      return (
        <List className="skills-list" style={style}>
          {skillsItems.map((skillCategory, i) => (
            <SkillsListItem
              key={i}
              skillName={skillCategory}
              stats={skills[skillCategory]}
              dictionary={dictionary}
            />
          ))}
        </List>
      );
    }
  }
}

export default MySkillsList;
