import React, { Component } from 'react';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import { List, Divider, ListItem, Avatar, FontIcon } from 'material-ui';
import { Link } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';

const menuLinks = [
    {
        label: '_achievements_log',
        secondaryLabel: '_achievements_log_secondary',
        link: '/achievements/log',
        iconText: 'list'
    },
    {
        label: '_personal_achievements',
        secondaryLabel: '_personal_achievements_secondary',
        link: '/achievements/personal',
        iconText: 'perm_identity'
    },
    {
        label: '_community_achievements',
        secondaryLabel: '_community_achievements_secondary',
        link: '/achievements/community',
        iconText: 'group'
    }
];

const LinkItemIcon = ({ text }) => <FontIcon className="material-icons">{text}</FontIcon>;

class AchievementsLinks extends Component {
    _getNavLinkFor = (el) => <Link
        to={el.link}
    />

    _getMenuLinks = (el, i) =>
        <div key={i}>
            <ListItem
                leftAvatar={<Avatar icon={<LinkItemIcon text={el.iconText} />} />}
                primaryText={<div className="app-drawer__body-label">{this.props.dictionary(el.label)}</div>}
                secondaryText={this.props.dictionary(el.secondaryLabel)}
                containerElement={this._getNavLinkFor(el)}
                rightIcon={<ChevronRight />}
            />;
            <Divider />
        </div>

    render() {
        return (
            <List className="achievements-links">
                {menuLinks.map(this._getMenuLinks)}
            </List>);
    }
}

export default withDictionary(AchievementsLinks);