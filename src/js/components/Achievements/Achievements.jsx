import './styles.scss';
import React, { Component } from 'react';
import { ScrollableHeaderPage } from 'js/components';
import AchievementsHeader from './AchievementsHeader';
// import AchievementsLinks from './AchievementsLinks';
import { withDictionary } from 'ioc-localization';
import { NavLink, Redirect } from 'react-router-dom';
import { withRouter } from 'react-router';
import { withGamification } from 'ioc-api-interface';
import { BottomNavigation, FontIcon } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import qs from 'qs';

// import AchievementsLog from './log/AchievementsLog';
import CommunityAchievements from './community/CommunityAchievements';
import PersonalAchievements from './personal/PersonalAchievements';

import { ACHIEVEMENTS_COMPONENTS } from './AchievementsRedirectLock';
import { logMain } from 'js/utils/log';

const ROOKIE_REDIRECT_URI =
  '/congratulations?newContentType=level&newContentName=Rookie&scoreDifference=0&direction=Up';

const LinkItemIcon = ({ text, color }) => (
  <FontIcon className="material-icons" color={color}>
    {text}
  </FontIcon>
);

const COMPONENTS = {
  // log: [AchievementsLog, '_achievements_log'],
  personal: [PersonalAchievements, '_personal_achievements'],
  community: [CommunityAchievements, '_community_achievements']
};

class Achievements extends Component {
  // Return true if user is a rookie and never accessed this section
  _isTotalRookie(props) {
    const { currentLevel, score } = props.gamificationProfile;
    return currentLevel.toLowerCase() === 'rookie' && score <= 0;
  }

  _setGamificationStarted() {
    // mark this section as visited
    localStorage.setItem('gamification_started', 'true');
  }

  _checkGamificationStarted(props) {
    let started = true;
    if (this._isTotalRookie(props)) {
      // User is a rookie: return true only if he/she already visited this section
      const isStarted = localStorage.getItem('gamification_started');
      started = isStarted === 'true';
    } else {
      // User already has a non-rookie level
      this._setGamificationStarted();
    }
    return started;
  }

  constructor(props) {
    super(props);
    this.state = {
      gamificationStarted: this._checkGamificationStarted(props)
    };
  }

  componentDidMount() {
    if (this.state.gamificationStarted === false) {
    }
  }

  shouldComponentUpdate(prevProps) {
    const { tabname, cardname } = this.props.match.params;
    const tabChange = tabname !== prevProps.tabname || cardname !== prevProps.cardname;

    return tabChange;
    //prevProps.hasModalOpen !== this.props.hasModalOpen ? false : true;
  }

  _onBackButtonClick = () => {
    const { location, history } = this.props;
    const searchParams = qs.parse(location.search.replace(/^\?/, ''));
    logMain('Achievements Back Button', searchParams);
    if (searchParams.from === 'congratulations') {
      history.replace('/');
    } else {
      history.length ? history.goBack() : history.replace('/');
    }
  };

  render() {
    if (this.state.gamificationStarted === false) {
      this._setGamificationStarted(); // so next time it won't show the page
      return <Redirect to={ROOKIE_REDIRECT_URI} replace={true} />;
    } else {
      const { dictionary, muiTheme, match, history } = this.props;

      const palette = muiTheme.palette;
      const { tabname, cardname } = match.params;

      const unactiveItemStyle = { color: palette.textColor };
      const activeItemStyle = { color: palette.primary1Color };
      const selectedIndex = ACHIEVEMENTS_COMPONENTS[tabname || 'personal'];
      const SelectedComponent = COMPONENTS[tabname || 'personal'][0];
      const title = COMPONENTS[tabname || 'personal'][1];

      return (
        <div className="achievements page">
          <ScrollableHeaderPage
            className="achievements page-inner"
            containerID="achievements-page"
            pageTitle={title}
            headerTextColor={palette.textColor}
            headerBackgroundColor={palette.primary1Color}
            headerContent={<AchievementsHeader history={history} />}
            onBackButtonClick={this._onBackButtonClick}
            content={
              <SelectedComponent
                palette={palette}
                tabname={tabname}
                cardname={cardname}
                dictionary={dictionary}
              />
            }
          />
          <BottomNavigation
            style={{
              display: 'flex',
              position: 'fixed',
              bottom: 0,
              backgroundColor: palette.tabsColor,
              zIndex: 10
            }}
            className="achievements-content-navigation"
            selectedIndex={selectedIndex}
          >
            {/* <NavLink
            className="achievements-content-navigation-item"
            to="/achievements/log"
            replace={true}
            activeStyle={activeItemStyle}
            style={{
              ...unactiveItemStyle,
              color: selectedIndex === 0 ? palette.primary1Color : palette.textColor
            }}
          >
            <LinkItemIcon
              color={selectedIndex === 0 ? palette.primary1Color : palette.textColor}
              text="list"
            />
            <span className="achievements-content-navigation-item__label">
              {dictionary('_achievements_log')}
            </span>
          </NavLink> */}
            <NavLink
              className="achievements-content-navigation-item"
              to="/achievements/personal/skills"
              replace={true}
              activeStyle={activeItemStyle}
              style={{
                ...unactiveItemStyle,
                color: selectedIndex === 1 ? palette.primary1Color : palette.textColor,
                opacity: selectedIndex === 1 ? 1 : 0.5
              }}
            >
              <LinkItemIcon
                color={selectedIndex === 1 ? palette.primary1Color : palette.textColor}
                text="perm_identity"
              />
              <span className="achievements-content-navigation-item__label">
                {dictionary('_personal_achievements')}
              </span>
            </NavLink>
            <NavLink
              className="achievements-content-navigation-item"
              to="/achievements/community/leaderboard"
              replace={true}
              activeStyle={activeItemStyle}
              style={{
                ...unactiveItemStyle,
                color: selectedIndex === 2 ? palette.primary1Color : palette.textColor,
                opacity: selectedIndex === 2 ? 1 : 0.5
              }}
            >
              <LinkItemIcon
                color={selectedIndex === 2 ? palette.primary1Color : palette.textColor}
                text="group"
              />
              <span className="achievements-content-navigation-item__label">
                {dictionary('_community_achievements')}
              </span>
            </NavLink>
          </BottomNavigation>
        </div>
      );
    }
  }
}

export default withRouter(withGamification(withDictionary(muiThemeable()(Achievements))));
