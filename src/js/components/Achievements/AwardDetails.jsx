import React, { Component } from 'react';
// import { getAsset } from 'js/utils/getAssets';
import { withDictionary } from 'ioc-localization';
import { MedalIcon } from 'js/components/Achievements/personal/Medals';
import styled from 'styled-components';
import { getMedalImageAsset } from './personal/assetNamesMaps';

// import { UserAvatar } from './personal/Medals';
// import { MedalIcon /* , AwardBadge */ } from 'js/components/Achievements/personal/Medals';
// import {
//   // MEDAL_NAMES_MAP,
//   // BADGE_NAMES_MAP,
//   // AWARD_ICONS_MAP,
//   LEVEL_ICONS_MAP
//   // UNLOCKED_CONTENTS_MAP
// } from 'js/components/Achievements/personal/assetNamesMaps';

const AwardElement = styled.div.attrs(props => ({ className: 'award-details' }))`
  width: 100%;
  height: 35vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;

  .award-container {
    position: relative;
    transform: scale(1.5);
  }

  div.award-details-image {
    position: relative;
    overflow: hidden;
  }

  .badge {
    box-sizing: border-box;
    border: 2px solid rgba(255, 255, 255, 1);
  }

  /* Shine */
  div.award-details-image.shine {
    mask-image: radial-gradient(circle at 50% 50%, black 100%, rgba(0, 0, 0, 0) 50%);
    &:after {
      content: '';
      top: 0;
      transform: translateX(100%);
      width: 100%;
      height: 100%;
      position: absolute;
      z-index: 1;
      animation: slide 1s infinite;

      /* 
  CSS Gradient - complete browser support from http://www.colorzilla.com/gradient-editor/ 
  */
      background: linear-gradient(
        to right,
        rgba(255, 255, 255, 0) 0%,
        rgba(255, 255, 255, 0.8) 50%,
        rgba(128, 186, 232, 0) 99%,
        rgba(125, 185, 232, 0) 100%
      ); /* W3C */
    }
  }

  /* animation */

  @keyframes slide {
    0% {
      transform: translateX(-100%);
    }
    100% {
      transform: translateX(100%);
    }
  }

  .small-badge {
    width: 24px;
    height: 24px;
    box-sizing: border-box;
    border-radius: 100%;
    line-height: 24px;
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    font-weight: 500;
    align-content: center;
    justify-content: center;
    text-align: center;

    background-color: ${props => props.theme.palette.accent1Color};
    &.lock {
      filter: saturate(50%);
    }
    &.bottom-right {
      position: absolute;
      bottom: 0;
      right: 0;
    }
  }
`;

class AwardDetails extends Component {
  render() {
    const { item, itemType, descriptionKey, dictionary } = this.props;
    const description =
      itemType === 'medal' || itemType === 'badge'
        ? dictionary(descriptionKey, item.threshold)
        : dictionary(descriptionKey);

    const image = getMedalImageAsset(item, itemType);
    const shine = itemType !== 'level' && item.isLocked !== true;

    return (
      <AwardElement>
        <div className="description">{description}</div>
        <div className="award-container">
          <MedalIcon
            className={`${shine ? 'award-details-image shine' : 'award-details-image'} ${
              itemType === 'level' ? 'leveldesc' : itemType
            }`}
            src={image}
            isLocked={item.isLocked}
          />
        </div>
      </AwardElement>
    );
  }
}

export default withDictionary(AwardDetails);
