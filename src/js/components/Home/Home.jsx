import React, { Component } from 'react';
import { Header, Content } from 'js/components/app';
import {
  HeaderControls,
  SquaredLinksContainer,
  ActionButtonsContainer,
  SquaredLink,
  LinkText,
  LinkIcon
} from './commons';
import { compose } from 'redux';
import { /* IS_ANDROID, */ IS_IOS } from 'js/utils/getAppInfo';
import { withDictionary } from 'ioc-localization';
import { withLogin, withAPISettings, withMapLayers, withIREACTFeatures } from 'ioc-api-interface';
import { withLayersAndSettings } from 'js/modules/layersAndSettings';
import { TopCenteredPage } from 'js/components/app';
import { Redirect } from 'react-router-dom';
import BTDeviceButton from './BTDeviceButton';
import ReportsQueueIndicator from './ReportsQueueIndicator';
import MissionsButton from './MissionsButton';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import { getAsset } from 'js/utils/getAssets';
import ReportRequestButton from './ReportRequestButton';
import { NewReportButton, UserPositionUpdater } from 'js/components/MapTabs';
// import HardwareBackButtonHandler from 'js/components/app/HardwareBackButtonHandler';
import moment from 'moment';
import { logMain, logWarning, logError } from 'js/utils/log';

const enhance = compose(
  withLogin,
  withAPISettings,
  withMapLayers,
  withDictionary,
  withLayersAndSettings,
  withIREACTFeatures
);

const MAP_PICS = {
  profile_icon: '/assets/home/button-dashboard-profile.svg',
  dashboard_icon: '/assets/home/button-dashboard-dashboard.svg',
  map_icon: '/assets/home/button-dashboard-map.svg',
  reports_icon: '/assets/home/button-dashboard-review.svg',
  emcomms_icon: '/assets/home/button-dashboard-comms.svg',
  tips_icon: '/assets/home/button-dashboard-tips.svg',
  social_icon: '/assets/home/button-dashboard-social.svg',
  settings_icon: '/assets/home/button-dashboard-settings.svg',
  help_icon: '/assets/home/button-dashboard-help.svg'
};

// citizenOrder , proOrder: determ order wrt user type. -1 means hidden
// if needsLocation: true then the link won't be enabled until the location (liveAOI) is available
const LINKS_CONFIGURATIONS = [
  {
    to: '/achievements/personal/skills',
    icon: 'profile_icon',
    label: '_home_btn_profile',
    citizenOrder: 0,
    proOrder: -1,
    disabled: false,
    needsLocation: false
  },
  // {
  //   to: '/dashboard',
  //   icon: 'dashboard_icon',
  //   label: '_home_btn_dashboard',
  //   citizenOrder: 2,
  //   proOrder: 1,
  //   disabled: false
  //   needsLocation: true //
  // },
  {
    to: '/tabs/map',
    icon: 'map_icon',
    label: '_home_btn_map',
    citizenOrder: 4,
    proOrder: 0,
    disabled: false,
    needsLocation: true
  },
  {
    to: '/tabs/reports',
    icon: 'reports_icon',
    label: '_home_btn_reports',
    citizenOrder: 5,
    proOrder: 3,
    disabled: false,
    needsLocation: true
  },
  {
    to: '/tabs/emergencyCommunications',
    icon: 'emcomms_icon',
    label: '_home_btn_comms',
    citizenOrder: 2,
    proOrder: 2,
    disabled: false,
    needsLocation: true
  },
  {
    to: '/learner',
    icon: 'tips_icon',
    label: '_home_btn_tips',
    citizenOrder: 1,
    proOrder: -1,
    disabled: false,
    needsLocation: false
  },
  {
    to: '/tabs/social',
    icon: 'social_icon',
    label: '_home_btn_social',
    citizenOrder: 3,
    proOrder: 4,
    disabled: false,
    needsLocation: true
  },
  {
    to: '/settings',
    icon: 'settings_icon',
    label: '_home_btn_settings',
    citizenOrder: 6,
    proOrder: 5,
    disabled: false,
    needsLocation: false
  },
  {
    to: '/help',
    icon: 'help_icon',
    label: '_help',
    citizenOrder: 7,
    proOrder: 5,
    disabled: false,
    needsLocation: false
  }
];

function linksSorter(linksSortingKey) {
  return (a, b) => a[linksSortingKey] - b[linksSortingKey];
}

class Home extends Component {
  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };
  // PATCH TW that may not get updated after long time the app isn't used
  // it checks if liveTW.dateEnd > today, and if not, re-sets the TW based
  // on the current settings
  _checkTWConsistency = () => {
    const { liveTW, liveTWPreferences, setLiveTimeWindowExtent } = this.props;
    const now = moment();
    const tEnd = moment(liveTW.dateEnd);
    const isOld = now.diff(tEnd) > 0;
    if (isOld) {
      logWarning('Refreshing time window', tEnd.toISOString(), now.toISOString());
      const { quantity, unit } = liveTWPreferences;
      setLiveTimeWindowExtent(quantity, unit);
    } else {
      logMain('Time window check ok 👌');
    }
  };

  _updateSettings = async () => {
    if (!this._autoRef) {
      return;
    }
    try {
      logMain('Home::_updateSettings');
      //   if (
      //     !this.props.mapLayersUpdating &&
      //     Array.isArray(this.props.mapLayers.availableTaskIds) &&
      //     this.props.mapLayers.availableTaskIds.length === 0
      //   ) {
      // Try a getLayers and update settings
      if (this._autoRef) {
        await this.props.getLayers();
      }
      if (this._autoRef) {
        await this.props.loadSettings();
      }
      // await Promise.all[(this.props.getLayers(), this.props.loadSettings())];
      // }
    } catch (err) {
      logError(err);
    }
  };

  componentDidMount() {
    this._checkTWConsistency();
  }

  // componentDidUpdate(prevProps) {
  //   if (prevProps.liveAOI === null && this.props.liveAOI !== null) {
  //     this._updateSettings();
  //   }
  // }

  _setLiveAreaOfInterestFromUserPosition = async (position, contentsRadius) => {
    if (!this.props.loggedIn) {
      return;
    }
    const nextLiveAOI = await this.props.setLiveAreaOfInterestFromUserPosition(
      position,
      contentsRadius
    );
    this._updateSettings();
    return nextLiveAOI;
  };

  render() {
    const {
      loggedIn,
      dictionary,
      user,
      liveAOI,
      ireactFeaturesIsUpdating /* , setLiveAreaOfInterestFromUserPosition */
    } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" push={false} />;
    }
    const isValidUserData = user !== null;
    if (!isValidUserData) {
      return <div />;
    }
    const userType = user ? user.userType : null; //POSSIBLE BUG HERE?

    const userUpdateProps = {
      loggedIn,
      user,
      liveAOI,
      setLiveAreaOfInterestFromUserPosition: this._setLiveAreaOfInterestFromUserPosition
    };

    const linksSortingKey = userType === 'authority' ? 'proOrder' : 'citizenOrder';

    // TODO optimize some calls
    return (
      <FeatureSelectionContext.Consumer>
        {FeatureSelectionState => (
          <UserPositionUpdater
            {...userUpdateProps}
            selectedFeature={FeatureSelectionState.selectedFeature}
            // onLiveAOIUpdate={this.props.getLayers}
          >
            {(
              requestGeolocationUpdate,
              geolocationPosition,
              geolocationUpdating,
              requestPositionPermission
            ) => (
              <TopCenteredPage className="home" ref={this._setAutoRef}>
                <Header
                  key="home-header"
                  //   title="_home"
                  showLogo={true}
                  leftButtonType="drawer"
                  rightButton={
                    <HeaderControls className="header-controls">
                      {userType !== 'citizen' && !IS_IOS && <BTDeviceButton />}
                      {userType !== 'citizen' && (
                        <MissionsButton label={dictionary._drawer_label_mission} />
                      )}
                      <ReportsQueueIndicator
                        selectedFeature={FeatureSelectionState.selectedFeature}
                        selectFeature={FeatureSelectionState.selectFeature}
                      />
                    </HeaderControls>
                  }
                />

                <Content className="home-content">
                  <SquaredLinksContainer className="home-links">
                    {LINKS_CONFIGURATIONS.filter(
                      lc => lc[linksSortingKey] > -1 && lc.disabled === false
                    )
                      .sort(linksSorter(linksSortingKey))
                      .map(lc => {
                        const disabled = lc.disabled || (lc.needsLocation && liveAOI === null);
                        return (
                          <SquaredLink
                            key={lc.to}
                            to={disabled ? '/home' : lc.to}
                            replace={disabled}
                            disabled={disabled}
                            onClick={
                              disabled
                                ? e => {
                                    e.preventDefault();
                                    if (!ireactFeaturesIsUpdating) {
                                      requestPositionPermission();
                                    }
                                  }
                                : undefined
                            }
                            className={lc.icon === 'profile_icon' ? 'profile' : lc.icon}
                          >
                            <LinkIcon
                              isProfile={lc.icon === 'profile_icon'}
                              className={lc.icon === 'profile_icon' ? 'profile' : ''}
                              src={
                                lc.icon === 'profile_icon'
                                  ? getAsset(AVATAR_FILE_NAMES[user.userPicture])
                                  : getAsset(MAP_PICS[lc.icon])
                              }
                            />
                            <LinkText>{dictionary(lc.label)}</LinkText>
                          </SquaredLink>
                        );
                      })}
                  </SquaredLinksContainer>
                  <ActionButtonsContainer className="home-actions">
                    <div className="action-button">
                      <ReportRequestButton label={dictionary('_home_action_report_request')} />
                    </div>
                    <div className="action-button">
                      <NewReportButton
                        type="raised"
                        label={dictionary('_home_action_new_report')}
                      />
                    </div>
                  </ActionButtonsContainer>
                </Content>
              </TopCenteredPage>
            )}
          </UserPositionUpdater>
        )}
      </FeatureSelectionContext.Consumer>
    );
  }
}

export default enhance(Home);
