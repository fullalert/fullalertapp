import React, { Component } from 'react';
import { FontIcon, RaisedButton } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withEmergencyCommunicationsApp } from 'ioc-api-interface';
import { LinkedButton } from 'js/components/app/LinkedButton';

class ReportRequestButton extends Component {
  static defaultProps = {
    label: '' // only for type: raised
  };

  render() {
    const { label, emergencyCommunicationFeaturesStats } = this.props;
    const borderStyle = { borderRadius: '18px' };
    const disabled = emergencyCommunicationFeaturesStats
      ? emergencyCommunicationFeaturesStats.lastActiveReportRequestID === null
      : true;
    const link = disabled
      ? null
      : `/emcommdetails/${emergencyCommunicationFeaturesStats.lastActiveReportRequestID}`;
    return (
      <RaisedButton
        disabled={disabled}
        labelColor={this.props.muiTheme.palette.textColor}
        buttonStyle={borderStyle}
        backgroundColor={this.props.muiTheme.palette.reportRequestEmCommColor}
        label={label}
        labelStyle={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
        overlayStyle={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-evenly'
        }}
        containerElement={
          link ? <LinkedButton disabled={disabled} to={link} replace={false} /> : 'div'
        }
        icon={
          <FontIcon color={this.props.muiTheme.palette.textColor} className="material-icons">
            edit
          </FontIcon>
        }
      />
    );
  }
}

export default withEmergencyCommunicationsApp(muiThemeable()(ReportRequestButton));
