import React, { Component } from 'react';
import { List, Divider, ListItem, Avatar, FontIcon } from 'material-ui';
import { Link } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import { Header } from 'js/components/app/header2';
import { TopCenteredPage } from 'js/components/app/Pages';
import { LearnerHeader, LearnerBottomLink, LearnerContent } from './commons';
import { Route } from 'react-router-dom';

const menuLinks = [
  {
    label: '_tips',
    secondaryLabel: '_tips_li_secondary',
    link: '/learner/tips',
    iconClass: 'ireact-icons',
    iconText: '\ue916'
  },
  {
    label: '_quizzes',
    secondaryLabel: '_quizzes_li_secondary',
    link: '/learner/quizzes',
    iconClass: 'ireact-icons',
    iconText: '\ue917'
  }
];

// const LinkItemIcon = ({ text }) => <FontIcon className="material-icons">{text}</FontIcon>;
const LinkItemIcon = ({ iconClass, iconText }) => (
  <FontIcon className={iconClass}>{iconText}</FontIcon>
);

class LearnerComponent extends Component {
  _getNavLinkFor = el => <Link to={el.link} />;

  _getMenuLinks = (el, i) => (
    <div key={i}>
      <ListItem
        leftAvatar={
          <Avatar icon={<LinkItemIcon iconClass={el.iconClass} iconText={el.iconText} />} />
        }
        primaryText={
          <div className="app-drawer__body-label">{this.props.dictionary(el.label)}</div>
        }
        secondaryText={this.props.dictionary(el.secondaryLabel)}
        containerElement={this._getNavLinkFor(el)}
        rightIcon={<ChevronRight />}
      />
      <Divider />
    </div>
  );

  render() {
    const { dictionary, locale } = this.props;

    return (
      <TopCenteredPage className="learner page">
        <Header
          title="_learner"
          leftButtonType="back"
          onLeftHeaderButtonClick={() => {
            this.props.history.replace('/home');
          }}
          primary={true}
          rightButton={
            <Link to="/about" style={{ padding: 12 }}>
              <FontIcon className="ireact-icons">{'\ue918'}</FontIcon>
            </Link>
          }
        />
        <LearnerContent>
          <LearnerHeader dictionary={dictionary} text="_learner_header_text" />
          <List
            style={{ width: '100%', marginTop: '0px', marginBottom: 'auto' }}
            className="learner-menu"
          >
            {menuLinks.map(this._getMenuLinks)}
          </List>
          <div className="disclaimer">
            {locale === 'en' ? '' : dictionary('_learner_prov_english_only')}
          </div>
          <LearnerBottomLink dictionary={dictionary} label="_my_scores" />
        </LearnerContent>
      </TopCenteredPage>
    );
  }
}

const Learner = withDictionary(LearnerComponent);

const LearnerPageRoute = () => (
  <Route render={({ match, history, location }) => <Learner history={history} />} />
);

export default LearnerPageRoute;
