import React, { Component } from 'react';
import { FormComponent } from 'js/components/app/FormComponent';
import { RaisedButton, FlatButton } from 'material-ui';
import { CountrySelect } from './CountrySelect';
import { MobilePrefixSelect } from './MobilePrefixSelect';

import {
  emailRule,
  passwordRule,
  usernameRule,
  firstnameRule,
  lastnameRule,
  confirmRule,
  mobilePhoneRule
} from 'js/utils/fieldValidation';
// import { CountryPhoneSelection } from './CountryPhoneSelection';
// import { LinkedButton } from 'js/components/app/LinkedButton';
import styled from 'styled-components';
import { logError, logMain } from 'js/utils/log';

const StyledForm = styled.form.attrs(props => ({ className: 'reg-form' }))`
  width: 100%;
  height: calc(100% - 64px);
`;

const FormContentWrapper = styled.div.attrs(props => ({ className: 'reg-form-wrapper' }))`
  width: 100%;
  height: calc(100% - 64px);
  overflow-y: auto;
  overflow-x: hidden;
`;

const StyledFormAction = styled.div`
  width: 100%;
  height: 64px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  background-color: ${props => props.theme.palette.backgroundColor};
`;

const style = {
  button: {
    margin: 8
  }
};

const passwordConfirmRule = confirmRule('password', 'Password do not match');
const emailConfirmRule = confirmRule('emailAddress', 'Email addresses do not match');

const validateCustom = value => {
  let errStr = null;
  if (!value) {
    errStr = { text: '_form_field_not_empty' };
  }
  return errStr;
};

const defaultTextInputStyle = {
  marginLeft: 'calc(50% - 152px)'
};

// field names not used directly in registration process
// though useful for restoring the view
const ADDITIONAL_FIELDS = ['prefix', 'phoneDigits', 'fullCountry', 'fullPrefix'];

export class RegistrationForm extends Component {
  _formConfig = [
    {
      name: 'name',
      trim: 'left',
      inputType: 'text',
      label: '_reg_form_firstname',
      rules: [firstnameRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'surname',
      trim: 'left',
      inputType: 'text',
      label: '_reg_form_lastname',
      rules: [lastnameRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'userName',
      trim: true,
      inputType: 'text',
      label: '_reg_form_username',
      rules: [usernameRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'password',
      trim: true,
      inputType: 'password',
      label: '_reg_form_password',
      rules: [passwordRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'password-confirm',
      trim: true,
      inputType: 'password',
      label: '_reg_form_password_conf',
      rules: [passwordRule, passwordConfirmRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'emailAddress',
      trim: true,
      inputType: 'email',
      label: '_reg_form_email',
      rules: [emailRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'emailAddress-confirm',
      trim: true,
      inputType: 'email',
      label: '_reg_form_email_conf',
      rules: [emailRule, emailConfirmRule],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    },
    {
      name: 'prefix',
      inputType: 'custom',
      input: MobilePrefixSelect,
      rules: [validateCustom],
      defaultValue: '',
      wrapperStyle: { width: 80, height: 48, display: 'inline-block', ...defaultTextInputStyle }
    },
    {
      name: 'phoneDigits',
      trim: true,
      inputType: 'number',
      label: '_reg_form_mobile',
      rules: [mobilePhoneRule],
      defaultValue: '',
      wrapperStyle: { width: 160, display: 'inline-block', marginLeft: 8, verticalAlign: 'bottom' },
      style: { width: 160 },
      inputStyle: { width: 160 },
      underlineStyle: { width: 160 },
      actionStyle: { bottom: 0, right: -56 }
    },
    {
      name: 'country',
      inputType: 'custom',
      input: CountrySelect,
      rules: [validateCustom],
      defaultValue: '',
      wrapperStyle: defaultTextInputStyle
    }
  ];

  _formData2Object = formData => {
    console.log('_formData2Object', formData);
    const { prefix, phoneDigits, fullCountry, fullPrefix } = formData;
    const validNumber = prefix.length > 0 && phoneDigits.length > 0;

    return {
      name: formData.name,
      surname: formData.surname,
      userName: formData.userName,
      password: formData.password,
      emailAddress: formData.emailAddress,
      'emailAddress-confirm': formData['emailAddress-confirm'],
      'password-confirm': formData['password-confirm'],
      country: formData.country,
      phoneNumber: validNumber ? `+${prefix}${phoneDigits}` : '',
      // additional fields
      prefix,
      phoneDigits,
      fullCountry,
      fullPrefix
    };
  };

  // _extractRelevantInformationFromFormdata = formData => ({
  //   name: formData.name,
  //   surname: formData.surname,
  //   userName: formData.userName,
  //   password: formData.password,
  //   emailAddress: formData.emailAddress,
  //   phoneNumber:
  //     formData.geography.phone &&
  //     formData.geography.phone.phoneNumber &&
  //     formData.geography.phone.prefix
  //       ? `+${
  //           formData.geography.phone.prefix ? formData.geography.phone.prefix.callingCode : '00'
  //         }${formData.geography.phone.phoneNumber}`
  //       : '',
  //   country: formData.geography.country ? formData.geography.country.name : '',
  //   // province: formData.geography.province ? formData.geography.province.name : '',
  //   // city: formData.geography.city ? formData.geography.city.name : '',
  //   bypassCaptchaToken: 'bypass',
  //   captchaResponse: '',
  //   fullCountry: formData.geography.country, // whole country object
  //   phone: formData.geography.phone // whole phone object
  // });

  _checkMissingFields = state => {
    let missingFields = false;
    let data = null;
    try {
      data = this._formData2Object(state);
      missingFields = Object.entries(data).reduce((prev, next) => {
        if (!ADDITIONAL_FIELDS.includes(next[0])) {
          prev = prev || next[1].length === 0;
        }
        return prev;
      }, false);
    } catch (err) {
      logError(err);
    }
    return { missingFields, data };
  };

  handleRegisterButtonClick = formData => {
    const { missingFields, data } = this._checkMissingFields(formData);
    if (!missingFields) {
      // const registrationData = this._extractRelevantInformationFromFormdata(formData); // remove confirms?
      logMain('REG CLICK', formData, Object.values(data), data);
      this.props.onSubmit(data);
    } else {
      this.props.onError(new Error(this.props.dictionary('_reg_err_complete_all_msg')));
    }
  };

  handleAbortButtonClick = e => {
    this.props.onAbort(e);
  };

  _getFormConfiguration = () => {
    if (this.props.initialValue === null) {
      return this._formConfig;
    } else {
      return this._formConfig.map(regFormField => {
        const name = regFormField.name;
        const value = this.props.initialValue[name.replace(/-confirm$/, '')];
        if (value) {
          regFormField['defaultValue'] = value;
        }
        return regFormField;
      });
    }
  };

  render() {
    const { dictionary } = this.props;
    const config = this._getFormConfiguration();
    return (
      <FormComponent
        className="registration-form"
        config={config}
        dictionary={dictionary}
        onSubmit={this.handleRegisterButtonClick}
        formComponent={StyledForm}
        innerComponent={FormContentWrapper}
        actionComponent={StyledFormAction}
        actions={(reset, resetValidationState, state, hasValidationErrors) => {
          const { missingFields } = this._checkMissingFields(state);
          console.log('Missing Fields?', missingFields, 'Validation Errors?', hasValidationErrors);
          return [
            <FlatButton
              key="back"
              className="ui-button"
              disableTouchRipple={true}
              onClick={this.handleAbortButtonClick}
              label={dictionary._back}
            />,
            <RaisedButton
              key="register"
              type="submit"
              className="ui-button"
              primary={true}
              style={style.button}
              disabled={missingFields || hasValidationErrors}
              label={dictionary._next}
            />
          ];
        }}
      />
    );
  }
}

export default RegistrationForm;
