export { default as Registration } from './Registration';
export { TermsAndConditions } from './TermsAndConditions';
export { SMSValidation } from './SMSValidation';
export { EmailValidation } from './EmailValidation';
export { CloseButton } from './commons';
