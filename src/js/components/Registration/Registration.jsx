import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withLogin } from 'ioc-api-interface';
import { withMessages, withLoader } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { withPartialRegistration } from 'js/modules/partialRegistration';
import { logError } from 'js/utils/log';
import { compose } from 'redux';
import RegistrationForm from './RegistrationForm';
import { Header } from 'js/components/app';
import { PageContainer } from 'js/components/app/commons';
import nop from 'nop';

const enhance = compose(
  withDictionary,
  withLogin,
  withMessages,
  withLoader,
  withPartialRegistration
);

class Registration extends Component {
  componentDidCatch(error, info) {
    this.props.pushError(error);
  }

  _checkPendingRegistration = () => {
    if (this.props.hasPendingRegistration) {
      const destination =
        this.props.partialUser.isPhoneConfirmed === false ? '/smsvalidation' : '/emailvalidation';
      this.props.history.replace(destination);
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.hasPendingRegistration && !prevProps.hasPendingRegistration) {
      this._checkPendingRegistration();
    }
  }

  onRegistrationFormSubmit = async data => {
    this.props.loadingStart();
    try {
      const result = await this.props.saveOrUpdateUser(data);
      console.log('%cRegistration success', 'color: white; background: lime', result);
      if (result.isPhoneConfirmed) {
        // Just go to email validation
        this.props.history.push('/emailvalidation');
      } else {
        this.props.history.push('/smsvalidation');
      }
    } catch (err) {
      err.message = '_registration_failure';
      this.props.pushError(err);
    } finally {
      this.props.loadingStop();
    }
  };

  _abortRegistration = async () => {
    if (this.props.hasPendingRegistration) {
      this.props.loadingStart();
      try {
        await this.props.abortRegistration();
        console.log('Registration aborted successfully');
        this.props.loadingStop();
        this.props.history.replace('/');
      } catch (err) {
        this.props.loadingStop();
        logError('Error aborting registration', err);
      }
    }
  };

  onAbortButtonClick = e => {
    e.preventDefault();

    if (this.props.hasPendingRegistration) {
      const { dictionary } = this.props;
      const title = dictionary('_reg_abort_modal_title');
      const body = dictionary('_reg_abort_modal_body');
      this.props.pushModalMessage(title, body, {
        _cancel: nop,
        _yes: this._abortRegistration
      });
    } else {
      this.props.history.replace('/');
    }
  };

  render() {
    return (
      <PageContainer className="registration page">
        <Header
          title={'_signup'}
          leftButtonType="clear"
          onLeftHeaderButtonClick={this.onAbortButtonClick}
        />
        <RegistrationForm
          initialValue={this.props.partialUser}
          onSubmit={this.onRegistrationFormSubmit}
          onAbort={this.onAbortButtonClick}
          dictionary={this.props.dictionary}
          locale={this.props.locale}
          onError={this.props.pushError}
        />
      </PageContainer>
    );
  }
}

export default withRouter(enhance(Registration));
