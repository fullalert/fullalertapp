import React, { Component } from 'react';
import { Header } from 'js/components/app/header2';
import { RaisedButton, FlatButton, FontIcon } from 'material-ui';
import { withMessages, withLoader } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
// import { Attire } from 'react-attire';
import moment from 'moment';
import { PageContainer } from 'js/components/app/commons';
import { compose } from 'redux';
import nop from 'nop';
import { Route, Redirect } from 'react-router';
import { ValidationPageContent } from './commons';
import styled from 'styled-components';
import { logError, logMain } from 'js/utils/log';
import { withPartialRegistration } from 'js/modules/partialRegistration';
import { FormComponent } from 'js/components/app/FormComponent';
import { pinDigitsRule } from 'js/utils/fieldValidation';
const enhance = compose(
  withDictionary,
  withMessages,
  withLoader,
  withPartialRegistration
);

const StyledForm = styled.form.attrs(props => ({ className: 'sms-form' }))`
  width: 100%;
  flex-grow: 2;
  display: flex;
  flex-direction: column;
`;

const FormContentWrapper = styled.div.attrs(props => ({ className: 'sms-form-wrapper' }))`
  width: 100%;
  /* flex: 2; */
  /* height: calc(100% - 64px); */
  overflow-y: auto;
  overflow-x: hidden;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;

const StyledFormAction = styled.div`
  bottom: 0;
  left: 0;
  position: absolute;
  width: 100%;
  height: 64px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  background-color: ${props => props.theme.palette.backgroundColor};
`;

const buttonIconStyle = { width: 24 };

const MAX_WAIT_TIME = IS_PRODUCTION ? 60 : 30; // seconds

export class SMSValidationComponent extends Component {
  state = {
    timeElapsed: 0,
    validating: false,
    sending: false
  };

  _formConfig = [
    {
      name: 'pinCode',
      trim: true,
      inputType: 'number',
      label: '_sms_valid_enter_code',
      rules: [pinDigitsRule],
      defaultValue: '',
      // wrapperStyle: { width: 160, display: 'inline-block', marginLeft: 8, verticalAlign: 'bottom' },
      // style: { width: 160 },
      inputStyle: { textAlign: 'center', fontSize: '28px', letterSpacing: '8px' }
      // underlineStyle: { width: 160 },
      // actionStyle: { bottom: 0, right: -56 }
    }
  ];

  _incrementTime = () => {
    if (this._componentRef) {
      this.setState(prevState => {
        return { timeElapsed: prevState.timeElapsed + 1 };
      });
    } else {
      this._clearInterval();
    }
  };

  _componentRef = null;

  _setComponentRef = e => (this._componentRef = e);

  _interval = null;

  _clearInterval = () => {
    if (this._interval !== null) {
      clearInterval(this._interval);
      this._interval = null;
    }
  };

  _startInterval = () => {
    if (this._componentRef) {
      this._clearInterval();
      this._interval = setInterval(this._incrementTime, 1000);
    }
  };

  _startCounting = partialUserPhoneNumberLastUpdate => {
    if (partialUserPhoneNumberLastUpdate !== null && this._componentRef) {
      const now = moment();
      const ts = moment(partialUserPhoneNumberLastUpdate);
      const timeElapsed = Math.floor(moment.duration(now.diff(ts)).asSeconds());
      this.setState({ timeElapsed, lastTimeChecked: ts }, this._startInterval);
    } else {
      this._startInterval();
    }
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.partialUserPhoneNumberLastUpdate !== null &&
      this.props.partialUserPhoneNumberLastUpdate !== prevProps.partialUserPhoneNumberLastUpdate
    ) {
      // user pushed resend restart timer
      this._startCounting(this.props.partialUserPhoneNumberLastUpdate);
    }
  }

  componentDidMount() {
    const partialUserPhoneNumberLastUpdate = this.props.partialUserPhoneNumberLastUpdate;
    this._startCounting(partialUserPhoneNumberLastUpdate);
  }

  componentWillUnmount() {
    this._clearInterval();
  }

  _resendSMS = () => {
    if (this.props.hasPendingRegistration && this._componentRef) {
      this.setState({ sending: true }, async () => {
        try {
          const result = this.props.resendPhoneActivationSMS();
          logMain('click resend SMS', result);
          this._startInterval();
        } catch (err) {
          logError(err);
          err.message = '_resend_sms_failure';
          this.props.pushError(err);
        } finally {
          if (this._componentRef) {
            this.setState({ sending: false });
          }
        }
      });
    }
  };

  _smsSent = () => {
    this.props.pushMessage('_sms_confirmed_successfully', 'success');
  };

  _onEnterPin = formData => {
    logMain('PIN', formData);

    if (this._componentRef) {
      this.setState({ validating: true }, async () => {
        this.props.loadingStart();
        try {
          const result = await this.props.confirmPhoneNumber(formData.pinCode);
          this._clearInterval();
          if (this._componentRef) {
            logMain('confirmPhoneNumber', result);
            this.setState({ validating: false }, this._smsSent);
          } else {
            this._smsSent();
          }
        } catch (err) {
          logError(err);
          err.message = '_registration_failure';
          this.props.pushError(err);
          if (this._componentRef) {
            this.setState({ validating: false });
          }
        } finally {
          this.props.loadingStop();
        }
      });
    }
  };

  _currentDuration() {
    const duration = moment.duration(this.state.timeElapsed, 'seconds');
    const mins = duration.minutes() + '';
    const secs = duration.seconds() + '';
    return `${mins.padStart(2, 0)}:${secs.padStart(2, 0)}`;
  }

  _abortRegistration = async () => {
    if (this.props.hasPendingRegistration) {
      this.props.loadingStart();
      try {
        await this.props.abortRegistration();
        logMain('Registration aborted successfully');
      } catch (err) {
        logError('Error aborting registration', err);
      } finally {
        this.props.loadingStop();
      }
    }
  };

  onAbortButtonClick = e => {
    e.preventDefault();
    const { dictionary } = this.props;
    const title = dictionary('_reg_abort_modal_title');
    const body = dictionary('_reg_abort_modal_body');
    this.props.pushModalMessage(title, body, {
      _cancel: nop,
      _yes: this._abortRegistration
    });
  };

  onBackButtonClick = e => {
    e.preventDefault();
    this.props.history.replace('/registration');
  };

  render() {
    const config = this._formConfig;

    const { sending, validating, timeElapsed } = this.state;
    const mayBeExpired = timeElapsed > MAX_WAIT_TIME;
    const duration = this._currentDuration();
    const { dictionary, partialUser, hasPendingRegistration } = this.props;
    if (!hasPendingRegistration) {
      return <Redirect to="/" push={false} />;
    } else if (hasPendingRegistration && partialUser.isPhoneConfirmed === true) {
      return <Redirect to="/emailvalidation" push={false} />;
    } else {
      return (
        <PageContainer ref={this._setComponentRef} className="sms validation page">
          <Header
            title={'_sms_validation'}
            leftButtonType="back"
            onLeftHeaderButtonClick={this.onBackButtonClick}
            rightButtonType="close"
            onRightHeaderButtonClick={this.onAbortButtonClick}
          />
          <ValidationPageContent>
            <div className="validation-content-element">
              {partialUser && [
                <p key="pm">
                  {dictionary(
                    '_sms_validation_sent_message',
                    partialUser.name,
                    partialUser.phoneNumber
                  )}
                </p>,
                <small key="ps">
                  <i>{sending ? '' : dictionary('_sms_valid_time_elapsed', duration)}</i>
                </small>
              ]}
            </div>
            <FormComponent
              className="registration-form"
              config={config}
              dictionary={dictionary}
              onSubmit={this._onEnterPin}
              formComponent={StyledForm}
              innerComponent={FormContentWrapper}
              actionComponent={StyledFormAction}
              actions={(reset, resetValidationState, state, hasValidationErrors) => {
                const nextDisabled = hasValidationErrors || state.pinCode.length === 0;
                return [
                  <FlatButton
                    key="back"
                    className="ui-button"
                    disableTouchRipple={true}
                    onClick={this.onBackButtonClick}
                    label={dictionary._back}
                  />,
                  <RaisedButton
                    key="submitPin"
                    type="submit"
                    labelPosition="before"
                    disabled={nextDisabled}
                    label={dictionary._next}
                    fullWidth={false}
                    primary={true}
                  />
                ];
              }}
            />
            <div className="validation-content-element sms-resend" style={{ marginBottom: 64 }}>
              {!validating && (
                <p>
                  {mayBeExpired
                    ? dictionary('_sms_valid_ask_sms')
                    : sending
                      ? ' '
                      : dictionary('_sms_valid_wait')}
                </p>
              )}
              <div style={{ width: '100%', textAlign: 'center' }}>
                <RaisedButton
                  primary={true}
                  disabled={sending === true || !mayBeExpired}
                  labelPosition="before"
                  label={dictionary('_sms_valid_btn_resend')}
                  fullWidth={false}
                  onClick={this._resendSMS}
                  icon={
                    <FontIcon style={buttonIconStyle} className="material-icons">
                      sms
                    </FontIcon>
                  }
                />
              </div>
            </div>
          </ValidationPageContent>
        </PageContainer>
      );
    }
  }
}

export const SMSValidation = enhance(props => (
  <Route
    render={({ location, history }) => (
      <SMSValidationComponent location={location} history={history} {...props} />
    )}
  />
));

export default SMSValidation;
