import React, { Component } from 'react';
import { TopCenteredPage } from 'js/components/app/Pages';
import { Header } from 'js/components/app/header2';
import { /* ShareIcon, */ GoToMapIcon } from 'js/components/ReportsCommons';
import { FontIcon, LinearProgress } from 'material-ui';
import { CATEGORY_MAP, CONTENT_TYPES_LETTERS } from 'js/components/ReportsCommons';
import { getAsset, openExternalLink } from 'js/utils/getAssets';
import { DelayedRedirect } from 'js/components/app/DelayedRedirect';
import { localizeDate } from 'js/utils/localizeDate';
import {
  EmCommChip,
  EmCommLanguage,
  //EmCommLanguageChip,
  LevelBox,
  getContentTypesInOrder,
  DetailsLoader
} from 'js/components/EmergencyCommunicationsCommons';
import { withDictionary } from 'ioc-localization';
import { NewReportButton } from 'js/components/MapTabs';

const TITLE_ICON_STYLE = { fontSize: 24 };
const INLINE_ICON_STYLE = { fontSize: 13, lineHeight: '18px', marginRight: '4px' };

/**
 * Mini-dict for handling pronoun's gender depending on type on supported languages
 * e.g. it "Questo allarme", "Questa allerta"
 */
function _notFoundPronounGenderLocale(locale, type) {
  let val = '';
  let LOCMAP = {};
  switch (type) {
    case 'alert':
      LOCMAP = {
        de: '',
        en: '',
        es: 'a',
        fr: 'tte',
        fi: '',
        it: 'a'
      };
      val = LOCMAP[locale] || '';
      break;
    case 'warning':
      LOCMAP = {
        de: '',
        en: '',
        es: 'a',
        fr: 't',
        fi: '',
        it: 'o'
      };
      val = LOCMAP[locale] || '';
      break;
    case 'report_request':
      LOCMAP = {
        de: '',
        en: '',
        es: 'a',
        fr: 'tte',
        fi: '',
        it: 'a'
      };
      val = LOCMAP[locale] || '';
      break;
    default:
      break;
  }
  return val;
}

const MeasureItem = ({ measure, dictionary }) => (
  <div className="measure-item">
    <FontIcon style={INLINE_ICON_STYLE} className={`mdi mdi-${CATEGORY_MAP[measure.category]}`} />
    <span>{dictionary(measure.title)}</span>
  </div>
);

const ReportRequestContent = ({
  featureProperties,
  hasReceivers,
  dictionary,
  emcommDatesString,
  statusColor
}) => {
  const { contentType, measures, address } = featureProperties;
  const contentTypes = getContentTypesInOrder(contentType);
  const hasMeasures = Array.isArray(measures) && measures.length > 0;
  const singleMeasure = hasMeasures && measures.length === 1;

  return (
    <div
      className="emcomm-details-body report-request"
      style={{ maxHeight: `calc(100% - ${hasReceivers ? 96 : 48}px)` }}
    >
      <div className="detail-field emcomm__address">
        <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
          place
        </FontIcon>
        <span>{address}</span>
      </div>
      <div className="detail-field emcomm__dates">
        <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
          event
        </FontIcon>
        <span>{emcommDatesString}</span>
        <div className="emcomm-status" style={{ backgroundColor: statusColor }} />
      </div>
      {contentTypes.map((ct, i) => (
        <div
          className={`detail-field emcomm__content-type ${ct}${
            i === contentTypes.length - 1 ? ' last' : ''
          }`}
          key={i}
        >
          {ct !== 'measure' && (
            <FontIcon className="ireact-icons" style={{ ...INLINE_ICON_STYLE, marginRight: 8 }}>
              {CONTENT_TYPES_LETTERS[ct]}
            </FontIcon>
          )}
          {ct !== 'measure' && <span>{dictionary('_' + ct)}</span>}

          {ct === 'measure' && (
            <div className="header-line">
              <span className="measures-count">{measures.length}</span>
              <span className="measures-header">
                {dictionary(singleMeasure ? '_measure' : '_measures')}
              </span>
            </div>
          )}
          {ct === 'measure' && (
            <div className="measures-list">
              {measures.map((m, i) => (
                <MeasureItem key={i} measure={m} dictionary={dictionary} />
              ))}
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

function getEntryValueString(value) {
  return Array.isArray(value)
    ? value.map(v => getEntryValueString(v)).join(', ')
    : JSON.stringify(value);
}

const AoWExtData = ({ extensionData }) => {
  const dataEntries = Object.entries(extensionData);
  return (
    <div className="text-details-field emcomm-details-extdata">
      {dataEntries.map(entry => (
        <div key={entry[0]} className="emcomm-extdata-entry">
          <span className="emcomm-extdata-key">{entry[0]}:</span>
          <span className="emcomm-extdata-value">{getEntryValueString(entry[1])}</span>
        </div>
      ))}
    </div>
  );
};

const AoWContent = ({
  hasReceivers,
  severity,
  locale,
  dictionary,
  type,
  emcommDatesString,
  address,
  statusColor,
  otherFeatureProps
}) => {
  const {
    capStatus,
    capUrgency,
    capCertainty,
    lastModified,
    title,
    capResponseType,
    instruction,
    instructions,
    description,
    web,
    extensionData,
    resource,
    capIdentifier
  } = otherFeatureProps;
  const lastUpdate = localizeDate(lastModified, locale, 'L LT');
  const _instructions = instruction || instructions;
  return (
    <div
      className="emcomm-details-body alert-or-warning"
      style={{ maxHeight: `calc(100% - ${hasReceivers ? 96 : 48}px)` }}
    >
      <div className="detail-field emcomm-details-body__info">
        <div className="emcomm-card__emcdesc">
          <LevelBox severity={severity} />
          <div className="emcomm-card__emctype">
            <div className="line big">
              {dictionary(`_emcomm_${type}`).toLocaleUpperCase()}
              {capStatus !== 'actual' && '/'}
              {capStatus !== 'actual' && dictionary(`_emd_cap_stat_${capStatus}`)}
            </div>
            <div className="line">{`${dictionary('_emd_cap_sev')}: ${dictionary(
              `_emd_cap_sev_${severity}`
            )}`}</div>
            <div className="line">{`${dictionary('_emd_cap_urg')}: ${dictionary(
              `_emd_cap_urg_${capUrgency}`
            )}`}</div>
            <div className="line">{`${dictionary('_emd_cap_cer')}: ${dictionary(
              `_emd_cap_cer_${capCertainty}`
            )}`}</div>
          </div>
        </div>
      </div>
      <div className="detail-field emcomm-details-body__dates">
        <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
          event
        </FontIcon>
        <span style={{ fontSize: '0.75em' }}>{emcommDatesString}</span>
        <div className="emcomm-status" style={{ backgroundColor: statusColor }} />
      </div>
      <div className="detail-field emcomm-details-body__address">
        <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
          place
        </FontIcon>
        <span>{address}</span>
      </div>
      <div className="detail-field emcomm-details-text">
        <div className="text-details-field emcomm-last-update">
          <div className="emcomm-details-label">{dictionary('_emd_lastupdate')}</div>
          <div className="emcomm-details-value">{lastUpdate}</div>
        </div>
        <div className="text-details-field emcomm-details-title-long">
          <div className="emcomm-details-label">{dictionary('_emd_title')}</div>
          <div className="emcomm-details-value">{title}</div>
        </div>
        {capResponseType && (
          <div className="text-details-field emcomm-details-resp-type">
            <div className="emcomm-details-label">{dictionary('_emd_responsetype')}</div>
            <div className="emcomm-details-value">{capResponseType}</div>
          </div>
        )}
        {_instructions && (
          <div className="text-details-field emcomm-details-instructions">
            <div className="emcomm-details-label">{dictionary('_emd_instructions')}</div>
            <div className="emcomm-details-value">{_instructions}</div>
          </div>
        )}
        {description && (
          <div className="text-details-field emcomm-details-message">
            <div className="emcomm-details-label">{dictionary('_emd_message')}</div>
            <div className="emcomm-details-value">{description}</div>
          </div>
        )}
        {extensionData && <AoWExtData extensionData={extensionData} />}
        {web && (
          <div className="text-details-field emcomm-details-web">
            <span className="emcomm-details-link" onClick={() => openExternalLink(web)}>
              {web}
            </span>
          </div>
        )}
        {resource && (
          <div className="text-details-field emcomm-details-attachment">
            <FontIcon className="material-icons" style={INLINE_ICON_STYLE}>
              attachment
            </FontIcon>
            <span className="emcomm-details-link" onClick={() => openExternalLink(resource)}>
              {resource}
            </span>
          </div>
        )}
        {capIdentifier && (
          <div className="text-details-field emcomm-details-code">
            <div className="emcomm-details-label">{dictionary('_emd_code')}</div>
            <div className="emcomm-details-value">{capIdentifier}</div>
          </div>
        )}
      </div>
    </div>
  );
};

class EmergencyCommunicationDetailsContent extends Component {
  render() {
    const {
      itemType,
      showOnMap,
      featureProperties,
      // matchId,
      dictionary,
      locale,
      palette,
      objectType,
      loading
    } = this.props;
    const {
      end,
      start,
      address,
      warningLevel,
      type,
      receivers,
      isOngoing,
      organization,
      sourceOfInformation,
      ...otherFeatureProps
    } = featureProperties;
    const hasReceivers = Array.isArray(receivers) && receivers.length > 0;
    const receiversListString = hasReceivers
      ? receivers.map(r => r.name.toLowerCase()).includes('citizen')
        ? dictionary('_emd_recv_public')
        : dictionary('_emd_recv_professionals')
      : '';
    const severity = typeof warningLevel === 'string' ? warningLevel : 'unknown';
    const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
    const emcommDatesString = `${dateStartedString}${end ? ' - ' + dateFinishedString : ''}`;

    const statusColor = isOngoing
      ? palette.ongoingReportRequestColor
      : palette.closedReportRequestColor;

    const aowProps = {
      hasReceivers,
      severity,
      locale,
      dictionary,
      type,
      emcommDatesString,
      address,
      statusColor,
      otherFeatureProps
    };

    // console.log('EMC CONTENT: loading', itemType, loading);
    let typeLabel = objectType
      ? dictionary('_emcomm_' + objectType)
      : dictionary('_drawer_label_notifications'); //TODO ADD SINGULAR?
    if (locale !== 'de') {
      typeLabel = typeLabel.toLowerCase();
    }
    return (
      <TopCenteredPage className="emcomm-details-content">
        {loading === true && [
          <Header key="header" showLogo={false} leftButtonType="back" />,
          <DetailsLoader key="items-list-loader" />
        ]}
        {itemType === null &&
          loading === false && [
            <Header key="header" showLogo={false} leftButtonType="back" />,
            // <h2 key="msg">{dictionary('_no_emcomm_found_with_id', matchId)}</h2>
            <div style={{ padding: 16, marginTop: 8 }} key="msg">
              <h3>
                {dictionary(
                  '_no_emcomm_found',
                  _notFoundPronounGenderLocale(locale, objectType),
                  typeLabel
                )}
              </h3>
              <DelayedRedirect
                timeout={6000}
                to="/tabs/emergencyCommunications"
                OnWaitComponent={() => (
                  <LinearProgress style={{ marginTop: '30%' }} mode="indeterminate" />
                )}
              />
            </div>
          ]}
        {itemType === 'reportRequest' && loading === false && (
          <Header
            showLogo={false}
            leftButtonType="back"
            title={<div className="emcomm-details-title">{dictionary._emcomm_report_request}</div>}
            rightButton={
              <div className="emcomm-details-actions">
                <GoToMapIcon iconStyle={TITLE_ICON_STYLE} onClick={showOnMap} />
                {/* <ShareIcon iconStyle={TITLE_ICON_STYLE} /> */}
              </div>
            }
          />
        )}
        {itemType === 'reportRequest' && loading === false && (
          <ReportRequestContent
            featureProperties={featureProperties}
            hasReceivers={hasReceivers}
            dictionary={dictionary}
            emcommDatesString={emcommDatesString}
            statusColor={statusColor}
          />
        )}
        {itemType === 'emergencyCommunication' && loading === false && (
          <Header
            showLogo={false}
            leftButtonType="back"
            title={
              <div className="emcomm-details-title">
                <EmCommChip dictionary={dictionary} palette={palette} emComm={featureProperties} />
                {featureProperties.language && (
                  <EmCommLanguage language={featureProperties.language} />
                  // <EmCommLanguageChip
                  //   dictionary={dictionary}
                  //   palette={palette}
                  //   language={featureProperties.language}
                  // />
                )}
              </div>
            }
            rightButton={
              <div className="emcomm-details-actions">
                <GoToMapIcon iconStyle={TITLE_ICON_STYLE} onClick={showOnMap} />
                {/* <ShareIcon iconStyle={TITLE_ICON_STYLE} /> */}
              </div>
            }
          />
        )}
        {itemType === 'emergencyCommunication' && loading === false && <AoWContent {...aowProps} />}
        {itemType !== null && loading === false && hasReceivers && (
          <div className="detail-field emcomm__receivers">
            <span>
              <FontIcon className="material-icons">person</FontIcon>
              <span>{`${dictionary._emcomm_sent_to}: `}</span>
            </span>
            <span>{receiversListString}</span>
          </div>
        )}
        {itemType !== null && loading === false && (
          <div className="detail-field emcomm__organization">
            <span
              className="emcomm__organization-avatar"
              style={{ color: palette.authorityColor, backgroundImage: getAsset('logo', true) }}
            />
            <span
              className="role-color-dot role-authority"
              style={{ backgroundColor: palette.authorityColor }}
            />
            <span>{sourceOfInformation || organization || 'EMDAT'}</span>
          </div>
        )}
        {itemType === 'reportRequest' &&
          loading === false &&
          (isOngoing === true && <NewReportButton requestProperties={featureProperties} />)}
      </TopCenteredPage>
    );
  }
}

export default withDictionary(EmergencyCommunicationDetailsContent);
