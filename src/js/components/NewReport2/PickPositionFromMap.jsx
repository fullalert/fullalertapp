import './commons/styles.scss';
import React, { Component } from 'react';

import nop from 'nop';
import { Header } from 'js/components/app';

import { MapView } from 'js/components/MapView';
import { makeDefaultCircle, DEFAULT_POSITION_CIRCLE_PAINT } from 'js/modules/map';

import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';
import { withNewReport } from 'js/modules/newReport2';
import { withMessages } from 'js/modules/ui';
import { withMapPreferences } from 'js/modules/preferences';

import { point, featureCollection /* , feature */ } from '@turf/helpers';
import circle from '@turf/circle';
import bbox from '@turf/bbox';

import { DeleteReportButton } from 'js/components/ReportsCommons';

const enhance = compose(
  withNewReport,
  withDictionary,
  withMessages,
  withMapPreferences
);

const DEFAULT_REPORT_AREA_RADIUS = 10; //KM
const SELECTED_POSITION_COLOR = '#FF0077';
const SELECTED_POSITION_BORDER_COLOR = '#880055';

class PickPositionFromMapComponent extends Component {
  constructor(props) {
    super(props);
    this.mapView = React.createRef();
    this.state = {
      selectedPosition: null,
      center: [0, 0],
      zoom: 14
    };
  }

  _getMap = () => {
    return this.mapView && this.mapView.current ? this.mapView.current.getMap() : null;
  };

  componentDidMount() {
    if (this.props.hasDraft) {
      const draft = this.props.newReportDraft;
      const center = [draft.userPosition.longitude, draft.userPosition.latitude];
      this.setState({ center });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const map = this._getMap();
    const positionPointSource = map.getSource('selected-pos');
    if (positionPointSource) {
      const fc = featureCollection([]);
      if (prevState.selectedPosition === null && this.state.selectedPosition !== null) {
        // Add point
        fc.features.push(this.state.selectedPosition);
        positionPointSource.setData(fc);
        this._showConfirmPositionMessage();
      } else if (prevState.selectedPosition !== null && this.state.selectedPosition === null) {
        // Remove point
        positionPointSource.setData(fc);
        this.props.updateReportDraft({
          position: null
        });
      }
    }
  }

  _toggleSelectedPosition = pos => {
    if (this.state.selectedPosition) {
      this.setState({ selectedPosition: null });
    } else {
      this.setState({ selectedPosition: pos });
    }
  };

  computeReportAreaAndBounds = center => {
    // if (this.props.hasDraft) {
    //   const draft = this.props.newReportDraft;
    //   const center = [draft.userPosition.longitude, draft.userPosition.latitude];
    // turf v5: const circularArea = circle(center, DEFAULT_REPORT_AREA_RADIUS, { units: 'kilometres', steps: 30 });
    // turf v4.7.3
    const circularArea = circle(center, DEFAULT_REPORT_AREA_RADIUS, {
      steps: 30,
      units: 'kilometres'
    });
    const box = bbox(circularArea);
    const areaBounds = [[box[0], box[1]], [box[2], box[3]]];
    return {
      center,
      circularArea,
      areaBounds
    };
    // }
  };

  _nextPage = () => {
    const position = this.state.selectedPosition;
    console.log('NAV TO NP', position);
    this.props.updateReportDraft({
      position: {
        latitude: position.geometry.coordinates[1],
        longitude: position.geometry.coordinates[0]
      }
    });
    const { history, location } = this.props;
    history.push(`/newreport/hazardSelect${location.search}`);
  };

  _showConfirmPositionMessage() {
    const actions = {
      _cancel: () => {
        this.setState({ selectedPosition: null });
      },
      _next: this._nextPage
    };
    const message = '_confirm_pick_position';
    this.props.pushMessage(message, 'default', actions);
  }

  _onConfirmExit = () => {
    this.props.deleteReportDraft();
    this.props.history.goBack();
  };

  _onCancelButtonClick = () => {
    const title = '_confirm';
    const message = '_confirm_delete_report_draft';
    this.props.pushModalMessage(title, message, {
      _close: nop,
      _confirm: this._onConfirmExit
    });
  };

  _onMapClick = event => {
    const position = event.lngLat;
    const positionPoint = point([position.lng, position.lat]);
    this._toggleSelectedPosition(positionPoint);
  };

  _onMapLoad = map => {
    if (this.props.hasDraft) {
      const draft = this.props.newReportDraft;
      const center = [draft.userPosition.longitude, draft.userPosition.latitude];

      map.addSource('userposition', {
        type: 'geojson',
        data: point(center)
      });
      map.addSource('selected-pos', {
        type: 'geojson',
        data: featureCollection([])
      });
      map.addLayer({
        id: 'position-marker',
        type: 'circle',
        source: 'userposition',
        paint: DEFAULT_POSITION_CIRCLE_PAINT
      });
      map.addLayer({
        id: 'selected-position-marker',
        type: 'circle',
        source: 'selected-pos',
        layout: {
          visibility: 'visible'
        },
        paint: makeDefaultCircle(SELECTED_POSITION_COLOR, SELECTED_POSITION_BORDER_COLOR)
      });
      const { areaBounds } = this.computeReportAreaAndBounds(center);
      map.stop();
      map.setCenter(center);
      map.setMaxBounds(areaBounds);
    }
  };

  _updateMapSettings = mapSettings => {
    const { center, zoom } = mapSettings;
    this.setState({ center, zoom });
  };

  render() {
    if (!this.props.hasDraft) return <Redirect to="/" push={false} />;

    const mapSettings = {
      style: this.props.preferences.mapStyle,
      center: this.state.center,
      zoom: this.state.zoom,
      pitch: 0,
      bearing: 0,
      // maxBounds: areaBounds,
      minZoom: 12,
      maxZoom: 17 // TODO make it depend on mapStyle and/or layers
    };

    return (
      <div className="new-report pick-report-position page">
        <Header
          title={'_pick_report_position'}
          leftButtonType="back"
          rightButton={<DeleteReportButton onClick={this._onCancelButtonClick} />}
        />
        <MapView
          ref={this.mapView}
          mapSettings={mapSettings}
          onMapLoadEnd={this._onMapLoad}
          onMapClick={this._onMapClick}
          onSettingsUpdate={this._updateMapSettings}
        />
      </div>
    );
  }
}

const PickPositionFromMap = enhance(PickPositionFromMapComponent);

const PickPositionRoute = () => (
  <Route
    render={({ history, location }) => (
      <PickPositionFromMap history={history} location={location} />
    )}
  />
);

export default PickPositionRoute;
