import './commons/styles.scss';
import React, { Component } from 'react';

import nop from 'nop';
import { Header } from 'js/components/app';

import { compose } from 'redux';
import { Route, Redirect } from 'react-router-dom';
import { withDictionary } from 'ioc-localization';
import { withNewReport } from 'js/modules/newReport2';
import { withMessages } from 'js/modules/ui';
import { withMapPreferences } from 'js/modules/preferences';
import { withAppNewReportUIConfig } from 'ioc-api-interface';
import { List, ListItem, Divider } from 'material-ui';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { DeleteReportButton, MeasureCategoryIcon } from 'js/components/ReportsCommons';
import qs from 'qs';

const enhance = compose(
  withNewReport,
  withDictionary,
  withMessages,
  withMapPreferences,
  withAppNewReportUIConfig
);

const NEXT_STEP = '/newreport/contentValue/';

class CategorySelectComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoriesFromMeasures: []
    };
    //this._initState(props);
  }

  _initState(props) {
    const { newReportUiConfig, location } = props;
    const search = location.search.replace(/^\?/, '');
    const searchParams = qs.parse(search);
    const filters = Object.keys(searchParams);
    const hasFilters = filters.indexOf('measure') > -1;
    const allowedMeasureIds = hasFilters
      ? Array.isArray(searchParams.measureId)
        ? searchParams.measureId.map(id => parseInt(id, 10))
        : typeof searchParams.measureId === 'string'
          ? [parseInt(searchParams.measureId, 10)]
          : 'any'
      : 'any';
    const categoriesFromMeasures = Array.from(
      new Set(
        newReportUiConfig.measures
          .filter(m => (allowedMeasureIds === 'any' ? true : allowedMeasureIds.indexOf(m.id) > -1))
          .map(m => m.category)
      )
    );
    this.setState(
      {
        categoriesFromMeasures
      },
      () => {
        if (this.state.categoriesFromMeasures.length === 1) {
          // no hassle, go straight to this measure
          this._onCategoryItemClick(this.state.categoriesFromMeasures[0], true);
        }
      }
    );
  }

  _onConfirmExit = () => {
    this.props.deleteReportDraft();
  };

  _onCancelButtonClick = () => {
    const title = '_confirm';
    const message = '_confirm_delete_report_draft';
    this.props.pushModalMessage(title, message, {
      _close: nop,
      _confirm: this._onConfirmExit
    });
  };

  _onCategoryItemClick = async (category, replace = false) => {
    const contentType = this.props.match.params.contentType;
    const draft = this.props.newReportDraft;
    if (draft.category !== category) {
      const updatedReport = await this.props.updateReportDraft({
        category: draft.category === category ? null : category
      });
      if (updatedReport.category === category) {
        replace
          ? this.props.history.replace(`${NEXT_STEP}${contentType}${this.props.location.search}`)
          : this.props.history.push(`${NEXT_STEP}${contentType}${this.props.location.search}`);
      }
    } else {
      replace
        ? this.props.history.replace(`${NEXT_STEP}${contentType}${this.props.location.search}`)
        : this.props.history.push(`${NEXT_STEP}${contentType}${this.props.location.search}`);
    }
  };

  componentDidMount() {
    if (this.props.newReportUiConfig) {
      this._initState(this.props);
    } else {
      this.props.history.replace('/');
    }
    // const state = this._initState(this.props);
    // this.setState(state, () => {
    //   if (this.state.categoriesFromMeasures.length === 1) {
    //     // no hassle, go straight to this measure
    //     this._onCategoryItemClick(this.state.categoriesFromMeasures[0], true);
    //   }
    // });
  }

  render() {
    if (!this.props.hasDraft) return <Redirect to="/" push={false} />;

    const { dictionary, muiTheme } = this.props;
    const { categoriesFromMeasures } = this.state;
    const palette = muiTheme.palette;

    const draft = this.props.newReportDraft;

    return (
      <div className="new-report content-selection page">
        <Header
          title={'_select_report_category'}
          leftButtonType="back"
          rightButton={<DeleteReportButton onClick={this._onCancelButtonClick} />}
        />
        <div className="new-report__body_full">
          <List className="line new-report__categories-list" style={{ padding: 0 }}>
            {categoriesFromMeasures.map((cat, i) => {
              const selected = draft.category === cat;
              const onClick = () => this._onCategoryItemClick(cat);
              return [
                <ListItem
                  key={`cat-${i}`}
                  primaryText={dictionary(`_cat_${cat}`)}
                  leftIcon={
                    <MeasureCategoryIcon
                      color={selected ? palette.accent1Color : palette.textColor}
                      category={cat}
                    />
                  }
                  rightIcon={<ChevronRight />}
                  className={selected ? 'category-item' : 'category-item selected'}
                  style={selected ? { color: palette.accent1Color } : {}}
                  onClick={onClick}
                />,
                <Divider key={`div-${i}`} />
              ];
            })}
          </List>
        </div>
      </div>
    );
  }
}

const CategorySelect = enhance(muiThemeable()(CategorySelectComponent));

const CategorySelectRoute = () => (
  <Route
    render={({ history, location, match }) => (
      <CategorySelect history={history} location={location} match={match} />
    )}
  />
);

export default CategorySelectRoute;
