export { default as NewReportRedirectLock } from './NewReportRedirectLock';

export { default as NewReportCapture } from './NewReportCapture';

export { default as NewReport } from './NewReport';
export { default as HazardSelect } from './HazardSelect';
export { default as PickPositionFromMap } from './PickPositionFromMap';
export { default as ContentSelect } from './ContentSelect';
export { default as CategorySelect } from './CategorySelect';
export { default as ContentValue } from './ContentValue';
export { default as WebcamCapture } from './WebcamCapture';
// export { MeasuresSelect } from './MeasuresSelect';
// export { DamagesSelect } from './DamagesSelect';
// export { ResourcesSelect } from './ResourcesSelect';
// export { PeopleSelect } from './PeopleSelect';