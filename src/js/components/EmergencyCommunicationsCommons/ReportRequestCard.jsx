import React, { Component } from 'react';
import { localizeDate } from 'js/utils/localizeDate';
import {
  CategoryDotIcon,
  ContentTypeChip,
  // getContentTypesInOrder,
  RESOURCES_IN_ORDER
} from 'js/components/EmergencyCommunicationsCommons';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { GoToMapIcon /*  ShareIcon */ } from 'js/components/ReportsCommons';
import { CloseButton } from 'js/components/app/HeaderButton';

const themed = muiThemeable();

function getMeasureCategories(measures) {
  if (Array.isArray(measures)) {
    const categories = Array.from(new Set(measures.map(m => m.category)));
    return categories;
  } else {
    return [];
  }
}

class ReportRequestCard extends Component {
  static defaultProps = {
    showMapIcon: false,
    featureProperties: null,
    onCloseButtonClick: null
  };

  render() {
    const {
      featureProperties,
      // dictionary,
      onCloseButtonClick,
      locale,
      muiTheme,
      // onShareButtonClick,
      onMapButtonClick,
      showMapIcon
    } = this.props;
    const { end, start, isOngoing, address, measures, contentType } = featureProperties;
    const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
    const emcommDatesString = `${dateStartedString}${end ? ' - ' + dateFinishedString : ''}`;
    const palette = muiTheme.palette;
    const statusColor = isOngoing
      ? palette.ongoingReportRequestColor
      : palette.closedReportRequestColor;
    const categories = getMeasureCategories(measures);
    const contentTypes = contentType.split(', '); //getContentTypesInOrder(contentType);

    if (featureProperties) {
      return (
        <div className="emcomm-card reportrequest">
          <div className="emcomm-card__content">
            <div className="reportrequest-content">
              {RESOURCES_IN_ORDER.map((ct, i) => (
                <ContentTypeChip
                  key={'cont' + i}
                  contentType={ct}
                  highlighted={contentTypes.indexOf(ct) !== -1}
                  measureCount={measures.length}
                  palette={palette}
                />
              ))}
              {categories.map((c, i) => (
                <CategoryDotIcon key={'cat' + i} category={c} palette={palette} />
              ))}
            </div>
            <div className="emcomm-card__address">{address}</div>
            <div className="emcomm-card__dates">
              <span>{emcommDatesString}</span>
              <div className="reportrequest-status" style={{ backgroundColor: statusColor }} />
            </div>
          </div>
          <div className="emcomm-card__actions">
            {!showMapIcon && typeof onCloseButtonClick === 'function' && (
              <CloseButton
                onClick={onCloseButtonClick}
                style={{ padding: 0, width: 24, height: 24, marginTop: 0, marginBottom: 'auto' }}
              />
            )}
            {/* <ShareIcon tooltip="Share" onClick={onShareButtonClick} /> */}
            {showMapIcon && <GoToMapIcon tooltip="Show On Map" onClick={onMapButtonClick} />}
          </div>
        </div>
      );
    } else {
      return <div className="emcomm-card reportrequest" />;
    }
  }
}

export default themed(ReportRequestCard);
