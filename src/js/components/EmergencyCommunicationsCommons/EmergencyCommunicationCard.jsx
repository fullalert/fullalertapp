import React, { Component } from 'react';
import { GoToMapIcon /* ShareIcon */ } from 'js/components/ReportsCommons';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { localizeDate } from 'js/utils/localizeDate';
import {
  EmCommChip,
  LevelBox,
  EmCommLanguage /*  EmCommLanguageChip */
} from 'js/components/EmergencyCommunicationsCommons';
import { CloseButton } from 'js/components/app/HeaderButton';

const themed = muiThemeable();

class EmergencyCommunicationCard extends Component {
  static defaultProps = {
    showMapIcon: false,
    featureProperties: null,
    onCloseButtonClick: null
  };

  render() {
    const {
      featureProperties,
      onCloseButtonClick,
      dictionary,
      locale,
      muiTheme,
      // onShareButtonClick,
      onMapButtonClick,
      showMapIcon
    } = this.props;
    const {
      end,
      start,
      address,
      title,
      type,
      warningLevel,
      capStatus,
      organization,
      sourceOfInformation,
      isOngoing
    } = featureProperties;
    const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
    const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
    const emcommDatesString = `${dateStartedString}${end ? ' - ' + dateFinishedString : ''}`;
    const palette = muiTheme.palette;
    const severity = typeof warningLevel === 'string' ? warningLevel : 'unknown';

    const statusColor = isOngoing
      ? palette.ongoingReportRequestColor
      : palette.closedReportRequestColor;

    if (featureProperties) {
      return (
        <div className="emcomm-card reportrequest">
          <div className="emcomm-card__content">
            <div className="emcomm-card__info">
              <EmCommChip dictionary={dictionary} palette={palette} emComm={featureProperties} />
              {featureProperties.language && (
                <EmCommLanguage language={featureProperties.language} />
              )}
              {/* <EmCommLanguageChip
                dictionary={dictionary}
                palette={palette}
                language={featureProperties.language}
              /> */}
            </div>
            <div className="emcomm-card__emcdesc">
              <LevelBox severity={severity} size="18px" />
              {/* <div className="emcomm-card__emclevel" style={levelStyle} /> */}
              <span className="emcomm-card__emctype">
                {dictionary(`_emcomm_${type}`).toLocaleUpperCase()}
                {capStatus !== 'actual' && '/'}
                {capStatus !== 'actual' && dictionary(`_emd_cap_stat_${capStatus}`)}
              </span>
            </div>
            <div className="emcomm-card__address">{address || title}</div>
            <div className="emcomm-card__dates">
              <span style={{ fontSize: '0.9em' }}>{emcommDatesString}</span>
              <div className="emcomm-status" style={{ backgroundColor: statusColor }} />
            </div>
            <div className="emcomm-card__organization">
              <span>{sourceOfInformation || organization || 'EMDAT'}</span>
            </div>
          </div>
          <div className="emcomm-card__actions">
            {!showMapIcon && typeof onCloseButtonClick === 'function' && (
              <CloseButton
                onClick={onCloseButtonClick}
                style={{ padding: 0, width: 24, height: 24, marginTop: 0, marginBottom: 'auto' }}
              />
            )}
            {/* <ShareIcon tooltip="Share" onClick={onShareButtonClick} /> */}
            {showMapIcon && <GoToMapIcon tooltip="Show On Map" onClick={onMapButtonClick} />}
          </div>
        </div>
      );
    } else {
      return <div className="emcomm-card reportrequest" />;
    }
  }
}

export default themed(EmergencyCommunicationCard);
