import React, { Component } from 'react';
import { Dialog, FlatButton } from 'material-ui';
import isPojo from 'is-pojo';
import { compose } from 'redux';
import { Provider } from 'react-redux';

import { Route } from 'react-router-dom';
import { StateContextContext, theme } from 'js/startup/Root';
import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';
import { ThemeProvider } from 'styled-components';

const enhance = compose(
  withLogin,
  withDictionary,
  withMessages
);

class Modal extends Component {
  unlock = null;

  handleRequestClose = (callback = null) => {
    let isMessage = this.props.modal !== null;
    if (isMessage) {
      if (typeof callback === 'function') {
        callback();
      }
      this.props.popModalMessage();
      if (this.unlock !== null) {
        this.unlock();
        this.unlock = null;
      }
    }
  };

  //This component accepts only actions in the format { actionName:actionCallback }
  //This methods remap them to flat buttons
  validateActions = actionName => {
    if (!actionName) {
      return { actions: [], onClose: this.handleRequestClose };
    } else {
      if (typeof actionName === 'function') {
        // JUST A CALLBACK
        return { actions: [], onClose: () => this.handleRequestClose(actionName) };
      }
      // Cfg object
      else if (isPojo(actionName)) {
        const actionKeys = Object.keys(actionName);
        if (actionKeys.length > 1) {
          return {
            onClose: () => this.handleRequestClose(actionName['_close'] || null),
            actions: actionKeys
              .map(name => {
                const isValid = typeof name === 'string' && typeof actionName[name] === 'function';
                return isValid
                  ? {
                      name,
                      action: () => {
                        // actionName[name]();
                        this.handleRequestClose(actionName[name]);
                      }
                    }
                  : null;
              })
              .filter(a => a !== null)
              .map((a, index) => (
                <FlatButton
                  label={this.props.dictionary(a.name)}
                  primary={index > 0}
                  onClick={a.action}
                />
              ))
          };
        } else {
          //just one action
          const name = actionKeys[0];
          const isValid = typeof name === 'string' && typeof actionName[name] === 'function';
          if (isValid) {
            const action = actionName[name];
            if (name === '_close') {
              const closeButton = (
                <FlatButton
                  label={this.props.dictionary._close}
                  primary={false}
                  onClick={() => {
                    // actionName[name]();
                    this.handleRequestClose(actionName[name]);
                  }}
                />
              );
              return {
                actions: [closeButton],
                onClose: () => this.handleRequestClose(actionName['_close'] || null)
              };
            } else {
              const actionButton = (
                <FlatButton
                  label={this.props.dictionary(action.name)}
                  primary={true}
                  onClick={() => {
                    // actionName[name]();
                    this.handleRequestClose(action);
                  }}
                />
              );
              const closeButton = (
                <FlatButton
                  label={this.props.dictionary._close}
                  primary={false}
                  onClick={this.handleRequestClose}
                />
              );
              return { actions: [closeButton, actionButton], onClose: this.handleRequestClose };
            }
          } else {
            return {
              actions: [
                <FlatButton
                  label={this.props.dictionary._close}
                  primary={false}
                  onClick={this.handleRequestClose}
                />
              ],
              onClose: this.handleRequestClose
            };
          }
        }
      } else return { actions: [], onClose: this.handleRequestClose };
    }
  };

  render() {
    const isModalMessage = this.props.modal !== null && this.props.modal.type === 'modal';
    let title = ' ';
    let text = ' ';
    let _actions = [];
    let elementIsText = false;
    let elementIsFn = false;
    let titleIsText = false;
    let titleIsFn = false;
    let titleIsObj = false;
    let titleStyle = {};
    let _onClose = this.handleRequestClose;
    const dict = this.props.dictionary;
    if (isModalMessage) {
      const message = this.props.modal;
      elementIsText = typeof message.text === 'string';
      elementIsFn = typeof message.text === 'function';
      const args = [message.text, ...message.params];
      text = elementIsText ? dict(...args) : message.text;
      titleIsText = typeof message.title === 'string';
      titleIsFn = typeof message.title === 'function';
      titleIsObj = !titleIsText && !titleIsFn && isPojo(message.title);
      titleStyle = titleIsObj ? message.title.titleStyle : {};
      title = titleIsObj ? message.title.text : message.title;
      const actionName = message.actionName;
      let { actions, onClose } = this.validateActions(actionName);
      _actions = actions;
      _onClose = onClose;
      if (this.unlock === null) {
        this.unlock = this.props.history.block();
      }
    }

    return (
      <StateContextContext.Consumer>
        {({ store }) => {
          if (store && typeof store.getState !== 'function') {
            return null;
          }
          return (
            <Dialog
              titleStyle={titleStyle}
              title={titleIsText || titleIsObj ? dict(title) : titleIsFn ? title(_onClose) : ''}
              actions={_actions}
              modal={false}
              open={isModalMessage}
              onRequestClose={_onClose}
              repositionOnUpdate={true}
              bodyClassName="app-modal-body"
              bodyStyle={{
                minHeight: '30vh',
                // height: 'calc(70vh - 76px)',
                maxHeight: 'calc(70vh - 76px)'
              }}
              autoDetectWindowHeight={false}
            >
              <ThemeProvider theme={theme}>
                <Provider store={store}>
                  {isModalMessage && !elementIsFn && text}
                  {isModalMessage && elementIsFn && text(_onClose, dict)}
                </Provider>
              </ThemeProvider>
            </Dialog>
          );
        }}
      </StateContextContext.Consumer>
    );
  }
}
const EnhancedModal = enhance(Modal);

function EnhancedModalRoute(props) {
  return <Route render={routeProps => <EnhancedModal {...routeProps} {...props} />} />;
}

export default EnhancedModalRoute;
