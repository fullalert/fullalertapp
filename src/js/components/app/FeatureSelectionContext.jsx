import React from 'react';
const FeatureSelectionState = {
  featureDetailsLoading: 0,
  clickedPoint: null, // Array[lng, lat]
  // only id: number and itemType: string wrapped in properties: {} for GeoJSON compatibility
  // plus coordinates for displaying on maps and so on
  // hoveredFeature: null,
  selectedFeature: null,
  // Mission task Management
  // hoveredMissionTask: null,
  selectedMissionTask: null,
  // State updaters
  setClickedPoint: point => {},
  selectFeature: feature => {},
  selectMissionTask: taskFeature => {},
  deselectFeature: point => {},
  deselectMissionTask: () => {},
  setFeatureDetailsLoading: () => {}
};

export const FeatureSelectionContext = React.createContext(FeatureSelectionState);
