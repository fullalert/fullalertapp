import './snackbar.scss';
import React, { Component } from 'react';
import { Snackbar as MuiSnackbar } from 'material-ui';
import { grey900, yellow600, lightGreenA400, red900, white } from 'material-ui/styles/colors';
import { iReactTheme } from 'js/startup/iReactTheme';

import isPojo from 'is-pojo';
import { compose } from 'redux';

import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';
import { withMessages } from 'js/modules/ui';

const enhance = compose(
  withLogin,
  withDictionary,
  withMessages
);

const BAR_HEIGHT = 48; //px
const DEFAULT_HIDE_TIME = 4000;
const ACTIONS_WIDTH = 70; //px
const STYLES = {
  error: {
    color: white,
    backgroundColor: red900
  },
  default: {
    color: white,
    backgroundColor: grey900
  },
  success: {
    color: lightGreenA400,
    backgroundColor: grey900
  },
  warning: {
    color: yellow600,
    backgroundColor: grey900
  }
};

class Snackbar extends Component {
  handleRequestClose = () => {
    const isError = this.props.error !== null;
    const isMessage = this.props.message !== null;
    if (isError) {
      this.props.popError();
    }
    if (isMessage) {
      this.props.popMessage();
    }
  };

  shouldComponentUpdate(nextProps) {
    const error = this.props.error ? this.props.error.toString() : null;
    const nxError = nextProps.error ? nextProps.error.toString() : null;

    const message = JSON.stringify(this.props.message);
    const nxMessage = JSON.stringify(nextProps.message);

    const locale = this.props.locale;
    const nxLocale = nextProps.locale;

    return error !== nxError || message !== nxMessage || locale !== nxLocale;
  }

  actions = {
    _close: this.handleRequestClose
  };

  //This component accepts only actions in the format { actionName:actionCallback }
  validateAction = actionName => {
    if (!actionName) {
      return null;
    } else if (actionName === '_close') {
      return [
        {
          name: '_close',
          action: this.handleRequestClose
        }
      ];
    } else {
      if (isPojo(actionName)) {
        let actionsMap = Object.keys(actionName)
          .map(name => {
            let isValid = typeof name === 'string' && typeof actionName[name] === 'function';
            return isValid
              ? {
                  name,
                  action: () => {
                    actionName[name]();
                    this.handleRequestClose();
                  }
                }
              : null;
          })
          .filter(a => a !== null);
        return actionsMap.length > 0 ? actionsMap : null;
      } else return null;
    }
  };

  _getMessage = () => {
    const dict = this.props.dictionary;
    const isError = this.props.error !== null;
    const isMessage = this.props.message !== null && this.props.message.type !== 'modal';
    let text = '';
    let actionName = null;
    let type = '';

    if (isError) {
      const error = this.props.error;
      let key = error.message;
      type = 'error';
      const details = error.details || {};
      const status = details.status || 0;
      let serverMessage = '';

      //means there is no translation for this
      if (details.hasOwnProperty('serverDetails')) {
        const serverDetails = details.serverDetails;
        if (serverDetails.hasOwnProperty('message')) {
          serverMessage = serverDetails.message;
        } else if (serverDetails.hasOwnProperty('error')) {
          serverMessage = serverDetails.error.message || serverDetails.error.details;
        }
        if (this.props.dictionary(key) === key) {
          key =
            status >= 400 && status < 500
              ? '_client_error'
              : status >= 500
                ? '_api_error'
                : '_unknown_error';
        }
      }
      const args = [key, serverMessage];
      text = dict(...args);
      actionName = error.actionName;
    }
    if (isMessage) {
      const message = this.props.message;
      type = message.type;
      const args = [message.text, ...message.params];
      text = dict(...args);
      actionName = message.actionName;
    }
    const actionsMap = this.validateAction(actionName);
    return { text, actionsMap, type };
  };

  _getStyleForTextLength = (actionsMap, textLength) => {
    let style = {
      height: BAR_HEIGHT,
      lineHeight: `${BAR_HEIGHT}px`
    };
    let screenWidth = window.screen.width;
    let appBarClassName = 'app-snackbar';
    if (actionsMap) {
      switch (actionsMap.length) {
        case 2:
          appBarClassName = 'app-snackbar two-actions';
          screenWidth = window.screen.width - 2 * ACTIONS_WIDTH;
          break;
        case 1:
          appBarClassName = 'app-snackbar one-action';
          screenWidth = window.screen.width - ACTIONS_WIDTH;
          break;
        case 0:
        default:
          break;
      }
    }

    if (textLength / screenWidth > 0.13) {
      style = {
        height: BAR_HEIGHT,
        lineHeight: `${BAR_HEIGHT / 2}px`
      };
    }
    if (textLength / screenWidth > 0.26) {
      style = {
        height: BAR_HEIGHT + BAR_HEIGHT / 2,
        lineHeight: `${BAR_HEIGHT / 2}px`
      };
    }
    return { style, appBarClassName };
  };

  _getMultipleActionsComponent = (actionsMap, height) => {
    const _height = height || BAR_HEIGHT;
    if (!actionsMap) {
      return null;
    } else {
      const dict = this.props.dictionary;
      return (
        <div
          className="app-snackbar__actions"
          style={{ height: _height, lineHeight: `${_height}px` }}
        >
          {actionsMap.map((cfg, i) => (
            <div
              className={`snackbar-action ${cfg.name}`}
              id={cfg.name}
              onClick={cfg.action}
              style={{
                color: i > 0 ? iReactTheme.palette.primary1Color : iReactTheme.palette.textColor
              }} // TODO USE PALETTE primary and text
              key={cfg.name}
            >
              {dict(cfg.name)}
            </div>
          ))}
        </div>
      );
    }
  };

  render() {
    const { text, actionsMap, type } = this._getMessage();
    const notificationStyle = typeof type === 'string' ? STYLES[type] : type;
    let time = undefined;
    const { style, appBarClassName } = this._getStyleForTextLength(
      actionsMap,
      text ? text.length : 0
    );
    const actionLabel = this._getMultipleActionsComponent(actionsMap, style.height);
    if (!actionLabel) {
      time = DEFAULT_HIDE_TIME;
    }
    return (
      <MuiSnackbar
        open={text && text.length > 0 ? true : false}
        className={appBarClassName}
        bodyStyle={{ ...style, ...notificationStyle }}
        autoHideDuration={time}
        onRequestClose={this.handleRequestClose}
        message={
          <div style={notificationStyle} className="app-snackbar__message">
            {text}
          </div>
        }
        action={actionLabel}
      />
    );
  }
}

export default enhance(Snackbar);
