import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Redirect } from 'react-router-dom';
import { logMain } from 'js/utils/log';

function useTimeout(timeout) {
  const _timeout = useRef(null);
  const [expired, setExpired] = useState(false);
  const timeoutCallback = useCallback(() => {
    setExpired(true);
  }, [timeout]);

  function initTimeout() {
    _timeout.current = setTimeout(timeoutCallback, timeout);
    logMain('Initialized timeout of', timeout, 'ms');

    return () => {
      if (_timeout.current) {
        clearTimeout(_timeout.current);
        _timeout.current = null;
        logMain('Timeout cleared');
      }
    };
  }

  useEffect(initTimeout, [timeout]);
  // useEffect(initTimeout, []);
  return expired;
}

export function DelayedRedirect({
  timeout = 1000,
  OnWaitComponent = null,
  to = '/',
  push = true,
  ...redirectProps
}) {
  const expired = useTimeout(timeout);
  return expired ? (
    <Redirect to={to} push={push} {...redirectProps} />
  ) : OnWaitComponent ? (
    <OnWaitComponent />
  ) : null;
}
