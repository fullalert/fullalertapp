import React from 'react';
import { Avatar /* , FontIcon */ } from 'material-ui';
import { RoleColorDot } from 'js/components/ReportsCommons';
import { getAsset } from 'js/utils/getAssets';

const LOGO = getAsset('logo', false);

function is(x) {
  return !!x;
}

const Reporter = ({
  dictionary,
  requesterId,
  reporter,
  reporterId,
  organization,
  palette,
  requesterType
}) => {
  const proReporter = organization !== null;
  const reporterName =
    requesterType === 'citizen' ? null : proReporter ? reporter.employeeId : null;
  if (organization) {
    // const displayedUserName = requesterId === reporterId ? dictionary._me : employeeId;

    const displayedUserName = requesterId === reporterId ? dictionary._me : reporterName;
    const text = [organization, displayedUserName].filter(is).join(', ');

    return (
      <div className="reporter-info">
        <Avatar
          className="reporter-avatar"
          style={{ borderColor: palette.authorityColor, backgroundColor: palette.textColor }}
          size={30}
          src={LOGO}
        />
        <RoleColorDot userRole="authority" />
        <span className="reporter-name-and-scores">{text}</span>
      </div>
    );
  } else {
    const { userPicture, level /* nickname,  score */ } = reporter;
    // const displayedUserName = requesterId === reporterId ? dictionary._me : nickname;
    const displayedUserName = requesterId === reporterId ? dictionary._me : null;
    const text = [displayedUserName, `${level || 'Novice'}`].filter(is).join(', ');
    const src = getAsset(AVATAR_FILE_NAMES[userPicture]);

    return (
      <div className="reporter-info">
        <Avatar
          className="reporter-avatar"
          style={{ borderColor: palette.citizensColor, backgroundColor: palette.primary2Color }}
          size={30}
          src={src}
        />
        <RoleColorDot userRole="citizen" />
        <span className="reporter-name-and-scores">{text /* `${text}, ${score} pts` */}</span>
      </div>
    );
  }
};

export default Reporter;
