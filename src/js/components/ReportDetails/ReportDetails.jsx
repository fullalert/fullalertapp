import './styles.scss';
import React, { Component } from 'react';
// import { withMap } from 'js/modules/map';
import { withReports } from 'ioc-api-interface';
import { withReportFilters } from 'js/modules/reportFilters';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import { SORTERS } from 'js/modules/reportFilters';
// import { getUserType } from 'js/utils/userPermissions';
// import { filterWithBounds } from 'js/utils/filterWithBounds';
import { withRouter } from 'react-router';

import ReportDetailsInner from './ReportDetailsInner';

class ReportDetails extends Component {
  render() {
    const {
      reportFeaturesURL,
      // updateReports,
      // selectedFeature,
      // selectFeature,
      // deselectFeature,
      // dictionary,
      // reportFeaturesStats,
      reportFiltersDefinition,
      reportSorter,
      match,
      history,
      location
      // upVoteReport,
      // downVoteReport,
      // validateReport,
      // setInaccurateReport,
      // setInappropriateReport
    } = this.props;

    const routerProps = { match, history, location };

    return (
      <GeoJSONFeaturesProvider
        sourceURL={reportFeaturesURL}
        filters={reportFiltersDefinition}
        sorter={SORTERS[reportSorter]}
        itemType={'report'}
      >
        {(features, featuresUpdating, featuresUpdateError, filtering, sorting) => (
          <FeatureSelectionContext.Consumer>
            {FeatureSelectionState => (
              <ReportDetailsInner
                {...FeatureSelectionState}
                features={features}
                featuresUpdating={featuresUpdating}
                {...routerProps}
              />
            )}
          </FeatureSelectionContext.Consumer>
        )}
      </GeoJSONFeaturesProvider>
    );
  }
}

export default withRouter(withReports(withReportFilters(ReportDetails)));
