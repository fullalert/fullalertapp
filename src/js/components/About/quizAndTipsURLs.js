export const credits = [
  { name: 'IHSA', url: 'https://www.ihsa.ca/' },
  { name: 'Civil Protection Grrek', url: 'https://www.civilprotection.gr' },
  {
    name: 'Canadian Red Cross',
    url:
      'http://www.redcross.ca/how-we-help/emergencies-and-disasters-in-canada/types-of-emergencies/floods'
  },
  {
    name: 'Victoria State Government',
    url:
      'https://www.betterhealth.vic.gov.au/health/HealthyLiving/After-a-flood-returning-home-safely#'
  },
  { name: 'UNISDR', url: 'http://www.unisdr.org/we/inform/terminology' },
  { name: 'NASA', url: 'https://www.nasa.gov/centers/langley/news/factsheets/Aerosols.html' },
  { name: 'NASA', url: 'https://climate.nasa.gov/evidence/' },
  {
    name: 'PHYS',
    url: 'https://phys.org/news/2017-07-effective-individual-tackle-climate-discussed.html'
  },
  {
    name: 'PRINCETON',
    url:
      'https://www.princeton.edu/news/2013/11/24/even-if-emissions-stop-carbon-dioxide-could-warm-earth-centuries'
  },
  {
    name: 'WHATSYOURIMPACT',
    url: 'https://whatsyourimpact.org/greenhouse-gases/carbon-dioxide-emissions'
  },
  { name: 'Canadian Red Cross', url: 'https://www.getprepared.gc.ca/cnt/hzd/lndslds-drng-en.aspx' },
  {
    name: 'NGWA, National Ground Water Association',
    url: 'http://www.ngwa.org/Fundamentals/teachers/Pages/information-on-earth-water.aspx'
  },
  { name: 'NSIDC', url: 'https://nsidc.org/cryosphere/glaciers/life-glacier.html' },
  {
    name: 'yaleclimateconnections',
    url:
      'https://www.yaleclimateconnections.org/2014/11/loss-of-land-ice-not-sea-ice-more-sea-level-rise/'
  },
  { name: 'noaa', url: 'https://oceanservice.noaa.gov/education/kits/tides/tides02_cause.html' },
  {
    name: 'livescience',
    url: 'https://www.livescience.com/7916-pollution-travels-globe-study-confirms.html'
  },
  { name: 'sciencedaily', url: 'https://www.sciencedaily.com/releases/2015/07/150703135248.htm' },
  { name: 'NSIDC', url: 'http://nsidc.org/cryosphere/sotc/sea_ice.html' },
  {
    name: 'CDC, Centers for Disease Control and Prevention',
    url: 'https://www.cdc.gov/disasters/index.html'
  },
  {
    name: 'CDC, Person with special needs',
    url: 'https://www.cdc.gov/disasters/earthquakes/disabilities.html'
  },
  { name: 'Ready', url: 'https://www.ready.gov/wildfires' }
];
