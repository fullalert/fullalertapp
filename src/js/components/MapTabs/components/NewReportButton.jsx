import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { FloatingActionButton, FontIcon, RaisedButton } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { withAppNewReportUIConfig } from 'ioc-api-interface';
import { logSevereWarning } from 'js/utils/log';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { withDevicePermissions } from 'js/modules/devicePermissions';
import STYLES from './styles';
import { compose } from 'redux';
import { withMessages } from 'js/modules/ui';
import {
  UserPermissionPrimer,
  UserGeolocationTurnedOff
} from 'js/components/app/UserPermissionPrimer';
import nop from 'nop';
import { isLocationEnabled } from 'js/utils/geolocation_check';

const enhance = compose(
  withAppNewReportUIConfig,
  withDevicePermissions,
  withMessages
);

class NewReportButton extends Component {
  static defaultProps = {
    offsetTop: 0,
    newReportUiConfig: {},
    requestProperties: null,
    type: 'floating', // or raised
    label: '' // only for type: raised
  };

  _tryUpdateUIConfig = async props => {
    try {
      await props.getAppNewReportUiConfig();
    } catch (err) {
      logSevereWarning('Cannot update UI config', err);
      appInsights.trackException(err, 'Home::_updateAreaIfNeeded', {}, {}, SeverityLevel.Error);
    }
  };

  componentDidMount() {
    const canRender = Object.keys(this.props.newReportUiConfig).length > 0;
    if (!canRender) {
      this._tryUpdateUIConfig(this.props);
    } else {
      console.log('UI config is ok, skipping update');
    }
  }

  _getReportRequestFilters = () => {
    if (this.props.requestProperties) {
      const { measures, contentType } = this.props.requestProperties;
      const contentTypes = contentType.split(', ');
      const nonMeasureFilters = contentTypes.map(ct => `${ct}=true`);
      const measureFilters = Array.isArray(measures) ? measures.map(m => `measureId=${m.id}`) : [];
      const qs = [...nonMeasureFilters, ...measureFilters].join('&');
      return `?${qs}`;
    } else {
      return '';
    }
  };

  _gotoNewReport = () => {
    const url = `/startCapture${this._getReportRequestFilters()}`;
    console.log('NRU', url);
    this.props.history.push(url);
  };

  _requestPermission = async permission => {
    try {
      await this.props.requestDevicePermission(permission);
      const permissions = await this.props.checkDevicePermissionGranted(['location', 'camera']);
      const [locationIsGranted, cameraIsGranted] = permissions;
      if (locationIsGranted && cameraIsGranted) {
        this._gotoNewReport();
      }
    } catch (err) {
      this.props.pushError(err);
    }
  };

  _askUserPermissions = async () => {
    try {
      const permissions = await this.props.checkDevicePermissionGranted([
        'location',
        'camera',
        'gallery'
      ]);
      const [locationIsGranted, cameraIsGranted] = permissions;
      if (!locationIsGranted) {
        const locationDenied = this.props.locationPermission === 'DENIED';
        this.props.pushModalMessage(
          '_geol_permission_primer_title',
          (onClose, dictionary) => (
            <UserPermissionPrimer type="location" denied={locationDenied} dictionary={dictionary} />
          ),
          locationDenied
            ? { _close: null }
            : {
                _close: nop,
                _next: () => this._requestPermission('location')
              }
        );
      } else if (!cameraIsGranted) {
        const cameraDenied = this.props.cameraPermission === 'DENIED';
        this.props.pushModalMessage(
          '_camera_permission_primer_title',
          (onClose, dictionary) => (
            <UserPermissionPrimer type="camera" denied={cameraDenied} dictionary={dictionary} />
          ),
          cameraDenied
            ? {
                _close: null
              }
            : {
                _close: nop,
                _next: () => this._requestPermission(['camera', 'gallery'])
              }
        );
      }
    } catch (err) {}
  };

  onClick = async () => {
    const { locationIsGranted, cameraIsGranted } = this.props;
    if (locationIsGranted && cameraIsGranted) {
      const locationIsEnabled = await isLocationEnabled();
      if (locationIsEnabled) {
        this._gotoNewReport();
      } else {
        this.props.pushModalMessage(
          '_geol_turned_off_title',
          (onClose, dictionary) => <UserGeolocationTurnedOff dictionary={dictionary} />,
          { _close: null }
        );
      }
    } else {
      this._askUserPermissions();
    }
  };

  render() {
    // check if the configuration is available
    const { type, label, offsetTop, newReportUiConfig } = this.props;
    const canRender = Object.keys(newReportUiConfig).length > 0;
    const bottom = STYLES.newReportButton.bottom + offsetTop;
    const textColorStyle = { color: this.props.muiTheme.palette.textColor };
    if (canRender) {
      if (type === 'raised') {
        const borderStyle = { borderRadius: '18px' };
        return (
          <RaisedButton
            label={label}
            backgroundColor={this.props.muiTheme.palette.accent1Color}
            // secondary={true}
            onClick={this.onClick}
            style={textColorStyle}
            buttonStyle={borderStyle}
            labelStyle={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
            overlayStyle={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-evenly'
            }}
            labelColor={this.props.muiTheme.palette.textColor}
            icon={
              <FontIcon color={this.props.muiTheme.palette.textColor} className="material-icons">
                photo_camera
              </FontIcon>
            }
          />
        );
      } else {
        return (
          <div
            className="floating-controls"
            style={{ ...STYLES.controls, ...STYLES.newReportButton, bottom, ...this.props.style }}
          >
            <FloatingActionButton
              mini={true}
              secondary={true}
              iconStyle={textColorStyle}
              onClick={this.onClick}
            >
              <FontIcon className="material-icons">photo_camera</FontIcon>
            </FloatingActionButton>
          </div>
        );
      }
    } else return null;
  }
}

export default withRouter(enhance(muiThemeable()(NewReportButton)));
