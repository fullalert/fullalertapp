import React, { Component } from 'react';
import { Tabs, Tab, FontIcon } from 'material-ui';
import { NavLink } from 'react-router-dom';
import muiThemeable from 'material-ui/styles/muiThemeable';
import styled from 'styled-components';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import { EmergencyCommunications } from './EmergencyCommunications';
import { Map } from './Map';
import { Reports } from './Reports';
import { Social } from './Social';

const STYLES = {
  customWidth: {
    width: 150
  },
  button: {
    margin: 12
  },
  default: {
    margin: 8
  },
  badgeText: {
    top: -7,
    position: 'relative',
    padding: 4
  },
  full: {
    width: '100%',
    height: '100%'
  },
  tabTemplate: {
    width: '100%',
    height: 'calc(100% - 48px)'
  },
  tabLink: { alignSelf: 'stretch' }
};

const themed = muiThemeable();

const TabContainer = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

class TabsNavLink extends Component {
  render() {
    const { replace, style, activeClassName, className, to, children } = this.props;
    const props = { replace, style, activeClassName, className, to };
    return <NavLink {...props}>{children}</NavLink>;
  }
}

class HomeTabs extends Component {
  render() {
    const {
      activeTab,
      muiTheme,
      liveAOI,
      requestGeolocationUpdate,
      geolocationPosition,
      geolocationUpdating
    } = this.props;
    const palette = muiTheme.palette;
    const tabStyle = { color: palette.textColor };

    const geolocationProps = {
      requestGeolocationUpdate,
      geolocationPosition,
      geolocationUpdating
    };

    return (
      <FeatureSelectionContext.Consumer>
        {FeatureSelectionState => (
          <Tabs
            style={{ ...STYLES.full, height: 'calc(100% - 64px)' }}
            className="home-tabs"
            contentContainerClassName="home-tab"
            contentContainerStyle={STYLES.tabTemplate}
            tabTemplateStyle={STYLES.full}
            tabItemContainerStyle={{
              color: palette.textColor,
              backgroundColor: palette.tabsColor,
              display: 'block'
            }}
            value={activeTab}
          >
            {/* Emergency Communications (Alert, Warnings, Report Requests) */}
            <Tab
              className="emergencyCommunications-tab"
              value="emergencyCommunications"
              containerElement={
                <TabsNavLink
                  replace={true}
                  style={tabStyle}
                  activeClassName="active"
                  className="tab-link"
                  to="/tabs/emergencyCommunications"
                />
              }
              icon={
                <span>
                  <FontIcon
                    style={{
                      padding: 4,
                      borderRadius: '100%',
                      backgroundColor: this.props.muiTheme.palette.tabsColor //this._isAnyEmCommSelectedTabColor()
                    }}
                    className="material-icons"
                    color={palette.textColor}
                  >
                    notifications
                  </FontIcon>
                </span>
              }
              style={{ color: palette.textColor }}
            >
              {activeTab === 'emergencyCommunications' && (
                <TabContainer>
                  <EmergencyCommunications {...FeatureSelectionState} />
                </TabContainer>
              )}
            </Tab>
            {/* Map */}
            <Tab
              value="map"
              className="map-tab"
              containerElement={
                <TabsNavLink
                  replace={true}
                  style={tabStyle}
                  activeClassName="active"
                  className="tab-link"
                  to="/tabs/map"
                />
              }
              icon={
                <span>
                  <FontIcon className="material-icons" color={palette.textColor}>
                    room
                  </FontIcon>
                </span>
              }
              style={{ color: palette.textColor }}
            >
              {activeTab === 'map' && (
                <TabContainer {...FeatureSelectionState}>
                  <Map
                    {...FeatureSelectionState}
                    {...geolocationProps}
                    maxDataBounds={liveAOI ? liveAOI.bounds : null}
                  />
                </TabContainer>
              )}
            </Tab>
            {/* Reports */}
            <Tab
              value="reports"
              className="reports-tab"
              containerElement={
                <TabsNavLink
                  replace={true}
                  style={tabStyle}
                  activeClassName="active"
                  className="tab-link"
                  to="/tabs/reports"
                />
              }
              icon={
                <span>
                  <FontIcon className="material-icons" color={palette.textColor}>
                    view_list
                  </FontIcon>
                </span>
              }
              buttonStyle={{ color: palette.textColor }}
            >
              {activeTab === 'reports' && (
                <TabContainer>
                  <Reports {...FeatureSelectionState} />
                </TabContainer>
              )}
            </Tab>
            {/* Social */}
            <Tab
              value="social"
              className="social-tab"
              containerElement={
                <TabsNavLink
                  replace={true}
                  style={tabStyle}
                  activeClassName="active"
                  className="tab-link"
                  to="/tabs/social"
                />
              }
              icon={
                <span>
                  <FontIcon
                    className="materialdesignicons mdi mdi-twitter"
                    color={palette.textColor}
                  />
                </span>
              }
              buttonStyle={{ color: palette.textColor }}
            >
              {activeTab === 'social' && (
                <TabContainer>
                  <Social {...FeatureSelectionState} />
                </TabContainer>
              )}
            </Tab>
          </Tabs>
        )}
      </FeatureSelectionContext.Consumer>
    );
  }
}

export default themed(HomeTabs);
