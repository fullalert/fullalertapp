import React, { Component } from 'react';
import { compose } from 'redux';
import { withTweetsFilters, availableSorters } from 'js/modules/tweetsFilters';
import { withAppSocial } from 'ioc-api-interface';
import { TweetsTimespanSelect, TweetsLanguageSelect } from 'js/components/Social';
import { ListHeader } from '../commons';
import SorterSelect from '../SorterSelect';
import RefreshButton from '../RefreshButton';

const enhance = compose(
  withTweetsFilters,
  withAppSocial
);

class TweetsListHeader extends Component {
  render() {
    const { text, refreshItems, tweetsSorter, setTweetsSorter, loading } = this.props;

    return (
      <ListHeader className="tweets">
        <RefreshButton tooltip={'_refresh'} refreshItems={refreshItems} loading={loading} />
        <div className="tweets list-header__item text">{text}</div>
        <TweetsLanguageSelect />
        <TweetsTimespanSelect />
        <SorterSelect
          availableSorters={availableSorters}
          selectedSorter={tweetsSorter}
          setSorter={setTweetsSorter}
        />
      </ListHeader>
    );
  }
}

export default enhance(TweetsListHeader);
