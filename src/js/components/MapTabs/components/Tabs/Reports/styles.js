export const STYLES = {
    avatar: {
        borderRadius: 0,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    inner: {
        padding: '4px',
        height: '100%',
        width: '100%',
        display: 'flex'
    },
    rIcon: {
        margin: '0 8px'
    },
    smallIcon: {
        fontSize: 16
    },
    smallIconButton: {
        width: 32,
        height: 32,
        padding: 4,
    },
    primaryItem: {
        height: 48
    },
    line: {
        height: 24,
        lineHeight: '24px',
        fontSize: '16px',
        display: 'inline-block'
    },
    dateLine: {
        height: 24,
        lineHeight: '24px',
        fontSize: '16px',
        margin: '0 8px'
    },
    separator: {
        width: '100%',
        border: '1px solid gray'
    }
};