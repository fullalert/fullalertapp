import React, { Component } from 'react';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { /*FloatingActionButton,*/ Paper, FontIcon } from 'material-ui';
import { Motion, spring } from 'react-motion';
import { withCompass } from 'ioc-device-orientation';
import { /*withMapRotation*/ withMap } from 'js/modules/map';
import STYLES from '../../styles';

const COMPASS_STYLE = {
  height: 40,
  width: 40,
  boxSizing: 'border-box',
  borderRadius: '50%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderStyle: 'dotted',
  borderWidth: '2px'
};
// string rotation
// funcs: enableMapCompassRotation, disableMapCompassRotation, toggleMapRotation
class Compass extends Component {
  componentDidMount() {
    this.props.watchHeadingStart();
  }

  componentWillUnmount() {
    this.props.watchHeadingStop();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.heading.normalizedValue !== nextProps.heading.normalizedValue;
  }

  render() {
    const palette = this.props.muiTheme.palette;
    const headingValue = this.props.heading.normalizedValue;
    // const headingValue = this.props.rotation === 'compass' ? 0 : normalizeRotation.call(this, this.props.heading[HEADING_KEY]);
    // console.log('COMPASS', this.props.rotation, headingValue);
    return (
      <div
        className="floating-controls"
        style={{ ...STYLES.controls, ...STYLES.compass, ...this.props.style }}
      >
        {
          <Motion defaultStyle={{ rotate: 0 }} style={{ rotate: spring(headingValue) }}>
            {({ rotate }) => (
              <Paper
                style={{
                  ...COMPASS_STYLE,
                  borderColor: palette.textColor,
                  backgroundColor: palette.primary2Color
                }}
                zDepth={2}
                circle={true}
                onClick={this.props.onClick}
              >
                <FontIcon
                  color={palette.textColor}
                  style={{ transform: `rotate3d(0,0,-1,${rotate}deg)` }}
                  className="material-icons"
                >
                  navigation
                </FontIcon>
              </Paper>
            )}
          </Motion>
        }
      </div>
    );
  }
}

export default withCompass(/*withMapRotation*/ withMap(muiThemeable()(Compass)));
