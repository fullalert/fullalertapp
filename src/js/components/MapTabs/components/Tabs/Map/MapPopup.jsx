import React, { Component } from 'react';
import { compose } from 'redux';
import nop from 'nop';
import { withDictionary } from 'ioc-localization';
import { withLogin } from 'ioc-api-interface';

import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { areFeaturesEqual } from 'js/utils/ireactFeatureComparison';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import { SmallReportCard } from 'js/components/ReportsCommons';
import {
  EmergencyCommunicationCard,
  ReportRequestCard
} from 'js/components/EmergencyCommunicationsCommons';
import { MissionCard, MissionTaskCard } from 'js/components/MissionCard';
import { AgentLocationCard } from 'js/components/AgentLocationCard';
import { TweetCard } from 'js/components/Social';

const enhance = compose(
  withDictionary,
  withLogin
);

const POPUP_HEIGHT_TWITTER = 130;
const POPUP_HEIGHT_EMCOMM = 120;
const POPUP_HEIGHT = 100; //px

const providerStyle = {
  position: 'relative',
  height: '100%',
  width: '100%',
  maxHeight: '100%',
  maxWidth: '100%'
};

const linkStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  height: '100%',
  width: '100%',
  maxHeight: '100%',
  maxWidth: '100%'
};

function findByIDAndItemType(id, itemType) {
  return f => f.properties.id === id && f.properties.itemType === itemType;
}

const PopupWrapper = styled.div`
  background: transparent;
  height: ${props =>
    props.hasFeature
      ? props.featureType === 'tweet'
        ? POPUP_HEIGHT_TWITTER
        : props.featureType === 'emergencyCommunication'
        ? POPUP_HEIGHT_EMCOMM
        : POPUP_HEIGHT
      : props.hasClickedPoint
      ? 40
      : 0}px;
  opacity: ${props => (props.hasFeature || props.hasClickedPoint ? 1 : 0)};
  transition: height, opacity ease 0.5s;
  width: 100%;
  position: absolute;
  top: 40px;
  z-index: 999;
  overflow: hidden;
`;

const PopupContainer = styled.div`
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.canvasColor};
  width: calc(100% - 16px);
  max-width: 400px;
  height: 100%;
  max-height: 100%;
  overflow: hidden;
  user-select: none;
`;

class MapPopupInner extends Component {
  static defaultProps = {
    clickedPoint: null,
    selectedFeature: null,
    selectedMissionTask: null,
    features: [],
    missionTasks: []
  };

  state = {
    feature: null
  };

  _clear = () => {
    this.setState({ feature: null });
  };

  _setSelectedFeature(id, itemType) {
    if (
      this.state.feature === null ||
      (this.state.feature.properties.id !== id ||
        this.state.feature.properties.itemType !== itemType)
    ) {
      const feature =
        itemType === 'missionTask'
          ? this.props.selectedMissionTask
          : this.props.features.find(findByIDAndItemType(id, itemType));
      if (feature) {
        this.setState({ feature });
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { features, selectedFeature, selectedMissionTask } = this.props;

    if (features.length > 0) {
      if (
        !areFeaturesEqual(prevProps.selectedFeature, selectedFeature) ||
        !areFeaturesEqual(prevProps.selectedMissionTask, selectedMissionTask)
      ) {
        if (selectedFeature !== null) {
          const feature = selectedMissionTask === null ? selectedFeature : selectedMissionTask;
          this._setSelectedFeature(feature.properties.id, feature.properties.itemType);
        } else {
          if (this.state.feature !== null) {
            this._clear();
          }
        }
      }
    }
  }

  componentDidMount() {
    const { features, selectedFeature, selectedMissionTask } = this.props;
    if (features.length > 0) {
      if (selectedFeature !== null) {
        const feature = selectedMissionTask === null ? selectedFeature : selectedMissionTask;
        this._setSelectedFeature(feature.properties.id, feature.properties.itemType);
      }
    }
  }

  _getLinkAndCardElement(feature, user, dictionary, deselectItem, missionTasks = []) {
    const onCloseButtonClick = e => {
      e.preventDefault();
      e.stopPropagation();
      deselectItem();
    };

    const { id, itemType, missionId } = feature.properties;
    let link = '';
    let CardElement = null;
    switch (itemType) {
      case 'report':
        link = `/reportdetails/${id}?expanded`;
        CardElement = (
          <SmallReportCard
            requesterType={user.userType}
            requesterId={user.id}
            dictionary={dictionary}
            reportProperties={feature.properties}
            coordinates={feature.geometry.coordinates}
            showMapIcon={false}
            onMapButtonClick={null}
            onCloseButtonClick={onCloseButtonClick}
          />
        );
        break;
      case 'emergencyCommunication':
        link = `/emcommdetails/${id}`;
        CardElement = (
          <EmergencyCommunicationCard
            dictionary={dictionary}
            featureProperties={feature.properties}
            showMapIcon={false}
            onMapButtonClick={null}
            onCloseButtonClick={onCloseButtonClick}
          />
        );
        break;
      case 'reportRequest':
        link = `/emcommdetails/${id}`;
        CardElement = (
          <ReportRequestCard
            dictionary={dictionary}
            featureProperties={feature.properties}
            showMapIcon={false}
            onMapButtonClick={null}
            onCloseButtonClick={onCloseButtonClick}
          />
        );
        break;
      case 'mission':
        link = `/missiondetails/${id}`;
        CardElement = (
          <MissionCard
            feature={feature}
            missionTasks={missionTasks}
            showMapIcon={false}
            onMapButtonClick={null}
            onCloseButtonClick={onCloseButtonClick}
          />
        );
        break;
      case 'missionTask':
        link = `/missiondetails/${missionId}/${id}`;
        CardElement = (
          <MissionTaskCard
            currentUserId={user.id}
            feature={feature}
            showMapIcon={false}
            onMapButtonClick={null}
            onCloseButtonClick={onCloseButtonClick}
          />
        );
        break;
      case 'agentLocation':
        link = '#';
        CardElement = (
          <AgentLocationCard
            dictionary={dictionary}
            agentLocationFeature={feature}
            currentUserId={user.id}
          />
        );
        break;
      case 'tweet':
        link = `/social/${id}`;
        CardElement = <TweetCard dictionary={dictionary} feature={feature} showMapIcon={false} />;
        break;
      default:
        CardElement = <div>{`ID: ${id}, Type: ${itemType}`}</div>;
        break;
    }
    return { link, CardElement };
  }

  render() {
    if (this.state.feature) {
      const { dictionary, user, deselectFeature, deselectMissionTask, missionTasks } = this.props;
      const deselectItem =
        this.state.feature.properties.itemType === 'missionTask'
          ? deselectMissionTask
          : deselectFeature;
      const { link, CardElement } = this._getLinkAndCardElement(
        this.state.feature,
        user,
        dictionary,
        deselectItem,
        missionTasks
      );
      return (
        <Link to={link} style={linkStyle}>
          <PopupContainer className="popup-container">{CardElement}</PopupContainer>
        </Link>
      );
    } else if (Array.isArray(this.props.clickedPoint)) {
      return (
        <div style={linkStyle}>
          <PopupContainer className="popup-container">
            <div style={{ padding: 8 }}>
              Selected: {this.props.clickedPoint[1].toFixed(5)},{' '}
              {this.props.clickedPoint[0].toFixed(5)}
            </div>
          </PopupContainer>
        </div>
      );
    } else {
      return null;
    }
  }
}

class MapPopup extends Component {
  static defaultProps = {
    allFeaturesDataURL: null,
    missionTaskFeaturesURL: null,
    clickedPoint: null,
    selectedFeature: null,
    selectedMissionTask: null,
    deselectFeature: nop,
    deselectMissionTask: nop
  };

  render() {
    const {
      allFeaturesDataURL,
      missionTaskFeaturesURL,
      selectedMissionTask,
      selectedFeature,
      clickedPoint,
      dictionary,
      user,
      deselectFeature,
      deselectMissionTask
    } = this.props;
    const hasClickedPoint = Array.isArray(clickedPoint);
    const hasFeature = allFeaturesDataURL
      ? selectedMissionTask !== null || selectedFeature !== null
      : false;

    const featureType = hasFeature
      ? selectedMissionTask === null
        ? selectedFeature.properties.itemType
        : 'missionTask'
      : null;

    const innerProps = {
      selectedMissionTask,
      selectedFeature,
      clickedPoint,
      dictionary,
      user,
      deselectFeature,
      deselectMissionTask
    };

    return (
      <PopupWrapper
        featureType={featureType}
        hasFeature={hasFeature}
        hasClickedPoint={hasClickedPoint}
        className="popup-wrapper"
      >
        <GeoJSONFeaturesProvider
          className="all-features-provider"
          sourceURL={allFeaturesDataURL}
          style={providerStyle}
        >
          {(features, featuresUpdating, error) => (
            <GeoJSONFeaturesProvider
              className="mission-task-provider"
              sourceURL={missionTaskFeaturesURL}
              style={providerStyle}
            >
              {(missionTaskFeatures, missionTaskfeaturesUpdating, missionTaskLoaderror) => {
                if ((features.length && selectedFeature) || hasClickedPoint) {
                  return (
                    <MapPopupInner
                      features={features}
                      missionTasks={missionTaskFeatures}
                      {...innerProps}
                    />
                  );
                } else return null;
              }}
            </GeoJSONFeaturesProvider>
          )}
        </GeoJSONFeaturesProvider>
      </PopupWrapper>
    );
  }
}

export default enhance(MapPopup);
