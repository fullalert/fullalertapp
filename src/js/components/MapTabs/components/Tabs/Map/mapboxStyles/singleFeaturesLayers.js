import { getMissionTaskFilterDefinition } from '../mapViewSettings';
import { darken } from 'material-ui/utils/colorManipulator';
import {
  REPORT_COLOR,
  ALERTS_OR_WARNINGS_CLUSTERED_COLOR,
  REPORT_REQUEST_COLOR,
  MISSION_COLOR,
  MISSION_TASK_COLOR,
  AGENTLOCATION_COLOR,
  MAPREQUEST_COLOR,
  TWEET_COLOR
} from './featureColors';
// itemType vs unicode for hazard icons
import HAZARD_ICONS_CODES from 'js/components/EmergencyCommunicationsCommons/ireact_pinpoint_emcomm_hazard_map';

const arrayFlatten = (p, n) => {
  p = [...p, ...n];
  return p;
};

const flattenedHazardIcons = Object.entries(HAZARD_ICONS_CODES).reduce(arrayFlatten, []);

// itemType vs unicode for feature icons
export const FEATURE_ICONS = {
  reportRequest: '\ue909',
  mapRequest: '\ue90d',
  mission: '\ue908',
  tweet: '\ue90b'
};

const AGENT_ICONS = {
  agentLocation: '\ue901',
  agentLocationWearable: '\ue90c'
};

const flattenItemTypesIcons = Object.entries(FEATURE_ICONS).reduce(arrayFlatten, []);

// Defines the icon-text-field value based on hazard
export const hazardExpr = ['match', ['string', ['get', 'hazard']], ...flattenedHazardIcons, ''];

// Defines the icon-text-field value based on itemType and hazard
export const iconTextFieldExpr = [
  'match',
  ['string', ['get', 'itemType']],
  ...flattenItemTypesIcons,
  'agentLocation',
  [
    'case',
    ['==', ['to-boolean', ['get', 'wearableInfo']], true],
    AGENT_ICONS.agentLocationWearable,
    AGENT_ICONS.agentLocation
  ],
  'emergencyCommunication',
  hazardExpr,
  'report',
  hazardExpr,
  ''
];

const reportValidationBadgeExpr = [
  'match',
  ['string', ['get', 'status']],
  'validated',
  'pinshape_validated_small',
  'inaccurate',
  'pinshape_rejected',
  'inappropriate',
  'pinshape_rejected',
  'submitted',
  '',
  ''
];

const emergencyCommunicationBadgeExpr = [
  'match',
  ['string', ['get', 'type']],
  'warning',
  'pinshape_warning_small',
  ''
];

export const iconImageExpr = [
  'match',
  ['string', ['get', 'itemType']],
  'report',
  reportValidationBadgeExpr,
  'emergencyCommunication',
  emergencyCommunicationBadgeExpr, // MERDA QUI!!!
  ''
];

export const FEATURE_COLORS = {
  report: REPORT_COLOR,
  emergencyCommunication: ALERTS_OR_WARNINGS_CLUSTERED_COLOR,
  mission: MISSION_COLOR,
  missionTask: MISSION_TASK_COLOR,
  reportRequest: REPORT_REQUEST_COLOR,
  mapRequest: MAPREQUEST_COLOR,
  agentLocation: AGENTLOCATION_COLOR,
  tweet: TWEET_COLOR
};

export const FEATURE_BORDER_COLORS = Object.keys(FEATURE_COLORS).reduce((values, colorKey) => {
  values[colorKey] = darken(FEATURE_COLORS[colorKey], 0.15);
  return values;
}, {});

export function getByItemType(colorMap) {
  return [
    'match',
    ['get', 'itemType'],
    'report',
    colorMap.report,
    'reportRequest',
    colorMap.reportRequest,
    'mission',
    colorMap.mission,
    'agentLocation',
    colorMap.agentLocation,
    'emergencyCommunication',
    colorMap.emergencyCommunication,
    'mapRequest',
    colorMap.mapRequest,
    'tweet',
    colorMap.tweet,
    '#666'
  ];
}

export const DEFAULT_ICON_PAINT = {
  'text-color': '#FFFFFF',
  'text-halo-width': 1,
  'text-halo-color': getByItemType(FEATURE_BORDER_COLORS)
};

export const DEFAULT_ICON_LAYOUT = {
  'text-font': ['I-REACT_Pinpoints_v4'],
  'text-field': iconTextFieldExpr,
  'text-size': ['interpolate', ['linear'], ['zoom'], 0, 2, 3, 5, 5, 9, 20, 24],
  'text-offset': [0, -0.1],
  // 'text-offset': [-0.5, -0.6],
  'text-allow-overlap': true,
  'icon-image': iconImageExpr,
  'icon-size': ['interpolate', ['linear'], ['zoom'], 0, 0, 4, 0, 7, 0.25, 24, 0.75], //8, 0.3, 24, 0.75],
  // 'icon-offset': [10, -40],
  'icon-offset': [20, -30],
  'icon-allow-overlap': true
};

export const NOR_CLUSTER_NOR_LEG = ['all', ['!has', 'cluster'], ['!has', 'isLeg']];

export function addSingleFeatureLayers(
  map,
  featuresSrcName,
  missionTasksSrc,
  mapMinZoom,
  mapMaxZoom
) {
  // Single feature pinpoints
  // Color depends on category
  map.addLayer({
    id: 'single-features',
    type: 'symbol',
    minzoom: mapMinZoom,
    // maxzoom: mapMaxZoom,
    source: featuresSrcName,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': ['interpolate', ['linear'], ['zoom'], 0, 2, 4, 7, 8, 14, 26, 36],
      // 'text-offset': [-0.5, -0.5],
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 2,
      'text-color': getByItemType(FEATURE_COLORS),
      'text-halo-color': getByItemType(FEATURE_BORDER_COLORS)
    },
    filter: NOR_CLUSTER_NOR_LEG
  });

  // Mission Tasks pinpoint (visible only if mission is selected)
  map.addLayer({
    id: 'single-missiontasks-features',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: missionTasksSrc,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': ['interpolate', ['linear'], ['zoom'], 0, 2, 4, 7, 7, 12, 24, 32],
      'text-allow-overlap': true
      // 'text-offset': [-0.5, -0.5]
    },
    paint: {
      'text-halo-width': 2,
      'text-color': FEATURE_COLORS.missionTask,
      'text-halo-color': FEATURE_BORDER_COLORS.missionTask
    },
    filter: getMissionTaskFilterDefinition()
  });

  // Single feature icons
  map.addLayer({
    id: 'single-feature-icons',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: featuresSrcName,
    filter: NOR_CLUSTER_NOR_LEG,
    layout: DEFAULT_ICON_LAYOUT,
    paint: DEFAULT_ICON_PAINT
  });

  // Mission Tasks icons (visible only if mission is selected)
  map.addLayer({
    id: 'single-missiontasks-feature-icons',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: missionTasksSrc,
    filter: getMissionTaskFilterDefinition(),
    layout: {
      ...DEFAULT_ICON_LAYOUT,
      'text-field': '\ue900'
    },
    paint: DEFAULT_ICON_PAINT
  });

  return Promise.resolve();
}
