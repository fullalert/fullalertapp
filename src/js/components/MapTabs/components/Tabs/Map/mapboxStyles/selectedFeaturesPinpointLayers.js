import {
  getByItemType,
  FEATURE_BORDER_COLORS,
  FEATURE_COLORS,
  NOR_CLUSTER_NOR_LEG,
  DEFAULT_ICON_PAINT,
  iconTextFieldExpr,
  iconImageExpr
} from './singleFeaturesLayers';
// import { EmptyFeatureCollection } from '../mapViewSettings';

export async function addSelectedMissionTaskLayers(map, missionTasksSrc, mapMinZoom) {
  const filter = ['all', ['==', 'itemType', 'missionTask'], ['==', 'id', -1]];
  const selectedMissionTaskHaloCircle = {
    id: 'selected-missiontask-halo-circle',
    type: 'circle',
    minzoom: mapMinZoom,
    source: missionTasksSrc,
    paint: {
      'circle-stroke-width': 2,
      'circle-blur': 0.15,
      'circle-opacity': 0.25,
      'circle-pitch-scale': 'map',
      'circle-stroke-color': FEATURE_BORDER_COLORS.missionTask,
      'circle-color': FEATURE_COLORS.missionTask,
      'circle-radius': {
        base: 1.5,
        stops: [[0, 16], [12, 26], [20, 104]]
      }
    },
    filter
  };

  const selectedMissionTaskPinpoint = {
    id: 'selected-missiontask-pin',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: missionTasksSrc,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': 36,
      // 'text-offset': [-0.4, -0.4],
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 3,
      'text-color': FEATURE_COLORS.missionTask,
      'text-halo-color': FEATURE_BORDER_COLORS.missionTask
    },
    filter
  };

  const selectedMissionTaskPinpointIcon = {
    id: 'selected-missiontask-pin-icon',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: missionTasksSrc,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-size': 36,
      'text-offset': [0, -0.1],
      // 'text-offset': [-0.4, -0.5],
      'text-field': '\ue900',
      'text-allow-overlap': true
    },
    paint: DEFAULT_ICON_PAINT,
    filter
  };

  map.addLayer(selectedMissionTaskHaloCircle);
  map.addLayer(selectedMissionTaskPinpoint);
  map.addLayer(selectedMissionTaskPinpointIcon);
  return Promise.resolve();
}

export async function addSelectedFeatureLayers(
  map,
  featuresSrcName,
  /* featureDataURL, */ mapMinZoom
) {
  // map.addSource(featuresSrcName, {
  //   type: 'geojson',
  //   data: featureDataURL ? featureDataURL : EmptyFeatureCollection,
  //   tolerance: 0,
  //   cluster: false
  // });

  const filter = ['all', NOR_CLUSTER_NOR_LEG, ['==', 'id', -1], ['==', 'itemType', 'undefined']];

  const selectedFeatureHaloCircle = {
    id: 'selected-feature-halo-circle',
    type: 'circle',
    minzoom: mapMinZoom,
    source: featuresSrcName,
    paint: {
      'circle-stroke-width': 2,
      'circle-blur': 0.15,
      'circle-opacity': 0.25,
      'circle-pitch-scale': 'map',
      'circle-stroke-color': getByItemType(FEATURE_BORDER_COLORS),
      'circle-color': getByItemType(FEATURE_COLORS),
      'circle-radius': {
        base: 1.5,
        stops: [[0, 16], [12, 26], [20, 104]]
      }
    },
    filter
  };

  const selectedFeaturePinpoint = {
    id: 'selected-feature-pin',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: featuresSrcName,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-field': '\ue90a',
      'text-size': 36,
      // 'text-offset': [-0.4, -0.4],
      'text-allow-overlap': true
    },
    paint: {
      'text-halo-width': 3,
      'text-color': getByItemType(FEATURE_COLORS),
      'text-halo-color': getByItemType(FEATURE_BORDER_COLORS)
    },
    filter
  };

  const selectedFeaturePinpointIcon = {
    id: 'selected-feature-pin-icon',
    type: 'symbol',
    minzoom: mapMinZoom,
    source: featuresSrcName,
    layout: {
      'text-font': ['I-REACT_Pinpoints_v4'],
      'text-size': 28,
      'text-offset': [0, -0.1],
      // 'text-offset': [-0.4, -0.5],
      'text-field': iconTextFieldExpr,
      'text-allow-overlap': true,
      'icon-image': iconImageExpr,
      'icon-size': 1,
      // 'icon-offset': [10, -30],
      'icon-offset': [18, -28]
    },
    paint: DEFAULT_ICON_PAINT,
    filter
  };

  map.addLayer(selectedFeatureHaloCircle);
  map.addLayer(selectedFeaturePinpoint);
  map.addLayer(selectedFeaturePinpointIcon);
  return Promise.resolve();
}

export async function updateSelectedFeatureLayers(map, selectedFeature) {
  if (map) {
    const filter = selectedFeature
      ? [
          'all',
          NOR_CLUSTER_NOR_LEG,
          ['==', 'id', selectedFeature.properties.id],
          ['==', 'itemType', selectedFeature.properties.itemType]
        ]
      : ['all', NOR_CLUSTER_NOR_LEG, ['==', 'id', -1], ['==', 'itemType', 'undefined']];
    map.setFilter('selected-feature-halo-circle', filter);
    map.setFilter('selected-feature-pin', filter);
    map.setFilter('selected-feature-pin-icon', filter);
  }
  return Promise.resolve();
}

export async function updateSelectedMissionTaskLayers(map, selectedMissionTask) {
  if (map) {
    const filter = selectedMissionTask
      ? ['all', ['==', 'itemType', 'missionTask'], ['==', 'id', selectedMissionTask.properties.id]]
      : ['all', ['==', 'itemType', 'missionTask'], ['==', 'id', -1]];
    map.setFilter('selected-missiontask-halo-circle', filter);
    map.setFilter('selected-missiontask-pin', filter);
    map.setFilter('selected-missiontask-pin-icon', filter);
  }
  return Promise.resolve();
}
