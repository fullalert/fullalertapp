import React, { Component } from 'react';
import { compose } from 'redux';
import { withIREACTFeatures, withMissions, withLogin } from 'ioc-api-interface';

import { CircularProgress } from 'material-ui';
import { MapView } from 'js/components/MapView';
import { withMap, withMapZoom } from 'js/modules/map';
import { /* withLoader */ withMessages } from 'js/modules/ui';
import { appInfo } from 'js/utils/getAppInfo';

import { withMapPreferences } from 'js/modules/preferences';
import MapPopup from './MapPopup';
import debounce from 'lodash.debounce';
import { toBBox } from 'js/utils/toBBox';
import { logMain, logSevereWarning, logWarning } from 'js/utils/log';
import { areGeolocationsEqual } from 'js/utils/updateRemotePosition';
import { compareFilterDefinitions } from 'js/utils/getActiveFiltersDefinitions';
import {
  areFeaturesEqual,
  arePointsEqual,
  isIREACTPointFeature,
  areBoundsEqual
} from 'js/utils/ireactFeatureComparison';
import { fetchFeatureAOI, flyToPolygonFeature } from './featuresInitialization/fetchFeatureAOI';
import { WorkerWrapper } from './clusterWorker/WorkerWrapper';
import {
  addFeatureLayers,
  // addEmergencyEventLayers,
  addGeolocationPositionFeaturesLayers,
  updateUserPositionDataOnMap,
  updateMapDataBounds,
  addClickedPositionFeaturesLayers,
  updateClickedPoint,
  addSelectedFeatureLayers,
  addSelectedMissionTaskLayers,
  updateSelectedFeatureLayers,
  updateSelectedMissionTaskLayers
} from './mapboxStyles/addFeatureLayers';
import {
  MIN_ZOOM,
  MAX_ZOOM,
  IREACT_FEATURES_SRC_NAME,
  IREACT_FEATURES_AOI_SRC_NAME,
  IREACT_MISSION_TASKS_SRC_NAME,
  // IREACT_EMERGENCY_EVENTS_SRC_NAME,
  boundsHeight,
  boundsWidth,
  //drawControlsOptions,
  EmptyFeatureCollection,
  getMissionTaskFilterDefinition
} from './mapViewSettings';
import { NewReportButton } from 'js/components/MapTabs';
import LegendButton from './LegendButton';
import { TimeSlider } from './TimeSlider';
import axios from 'axios';
import { getFeatureInfoUrl } from 'js/utils/layerUtils';
import Compass from './Compass';
import PositionButton from './PositionButton';

const MAP_HEIGHT_COEFF = 0.1; // for zoom offset on selected feat

const IREACT_ALL_FEATURES_SRC_NAME = `${IREACT_FEATURES_SRC_NAME}_all_features`;

const enhance = compose(
  withMap,
  withMapZoom,
  withMessages,
  withMapPreferences,
  withIREACTFeatures,
  withMissions,
  withMessages,
  withLogin
);

class MapInner extends Component {
  constructor(props) {
    super(props);
    this.mapView = React.createRef();
    this.state = {
      //mirrors of the ones in Home
      // hoveredFeature: null,
      selectedFeature: null,

      workerReady: false,
      featureLayersAdded: false,
      featureUpdating: false, // set true to hide popup when loading AOI
      // map initial style (base and stuff) loaded
      initialStyleLoaded: false,
      mapLoaded: false,
      // all point features URL (well, except events)
      allFeaturesDataURL: null,
      // clustered point features URL (well, except events)
      clusteredFeaturesDataURL: null,
      // Time Slider Index
      timeSliderIndex: 0,
      timeSliderLabel: '',
      geoserverService: this._getGeoserverServiceConfigFromProps(props.user)
    };
  }

  _getGeoserverServiceConfigFromProps(user) {
    return user && Array.isArray(user.services)
      ? user.services.find(s => s.serviceName === 'Geoserver') || null
      : null;
  }

  _getMap = () => {
    return this.mapView && this.mapView.current ? this.mapView.current.getMap() : null;
  };

  _transformRequest = xhrCfg => {
    const cfg = { ...xhrCfg };
    // if (this.state.geoserverService) {
    //   const baseUrl = this.state.geoserverService.baseUrl;
    //   if (cfg.url.includes(baseUrl)) {
    //     cfg.headers['apikey'] = this.state.geoserverService.key;
    //   } else {
    //     logSevereWarning(cfg.url, this.state.geoserverService, baseUrl);
    //   }
    // }
    return cfg;
  };

  workerWrapper = null;

  _requestClusterUpdate = debounce(
    () => {
      const map = this._getMap();
      if (!map) {
        return;
      }
      const zoom = map.getZoom();
      const bbox = toBBox(map.getBounds());
      const center = map.getCenter().toArray();
      this.workerWrapper.send('getClusters', { bbox, zoom });
      return {
        zoom,
        bbox,
        center
      };
    },
    200,
    { leading: true }
  );

  _onWorkerInitOrUpdate = result => {
    logMain('Feature Inited Or Updated', result);
    this.setState(prevState => {
      if (!prevState.workerReady && result.ready) {
        return { allFeaturesDataURL: result.allFeaturesUrl, workerReady: result.ready };
      } else if (prevState.allFeaturesDataURL !== result.allFeaturesUrl) {
        return { allFeaturesDataURL: result.allFeaturesUrl };
      } else return {};
    });
  };

  _onWorkerGetClusters = result => {
    logMain('Worker getClusters', result);
    if (!result.clusteredFeaturesUrl) {
      return;
    }
    const map = this._getMap();
    if (!map) {
      return;
    }
    const featuresSrc = map.getSource(IREACT_FEATURES_SRC_NAME);
    // Update cluster features URL
    if (featuresSrc) {
      featuresSrc.setData(result.clusteredFeaturesUrl);
    } else {
      console.warn(`Main: cannot find source ${IREACT_FEATURES_SRC_NAME}`);
    }
  };

  _zoomToPoint = (map, center, zoom, offset = [0, 0]) => {
    if (map && center) {
      map.stop().flyTo({ center, zoom, curve: 1, offset });
    }
  };

  _onWorkerZoomToFeature = ({ center, zoom, feature }) => {
    logMain('Worker zoom to feature', center, zoom, feature);
    const map = this._getMap();
    if (!map) {
      return;
    }
    const mapHeightInPixel = map.getContainer().clientHeight;
    const offset = [0, mapHeightInPixel * MAP_HEIGHT_COEFF];
    if (feature && isIREACTPointFeature(feature.properties.itemType)) {
      if (map.isMoving()) {
        map.stop();
      }
      this._zoomToPoint(map, center, zoom, offset);
    } else {
      if (feature == null && Array.isArray(center)) {
        this._zoomToPoint(map, center, zoom);
      }
    }
  };

  _onWorkerFindFeature = result => {
    logMain('Worker Find Feature', result);
    const { /* interactionType, */ feature /* , parentFeature */ } = result;
    this.setState({ selectedFeature: feature });
    // if (interactionType === 'hover') {
    //   let _feature =
    //     parentFeature === null
    //       ? feature
    //       : {
    //           properties: feature.properties,
    //           geometry: parentFeature.geometry,
    //           type: feature.type
    //         };

    //   this.setState({ hoveredFeature: _feature });
    // } else if (interactionType === 'select') {
    //   this.setState({ selectedFeature: feature });
    // }
  };

  _onWorkerGetChildren = result => {
    logMain('Worker Children', result);
  };

  _clearFeature = () => {
    this.setState({ selectedFeature: null });
    // if (interactionType === 'hover') {
    //   this.setState({ hoveredFeature: null });
    // } else if (interactionType === 'select') {
    //   this.setState({ selectedFeature: null });
    // }
  };

  _updatePolygonSrc = polygonData => {
    const map = this._getMap();
    if (!map) {
      return;
    }
    const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
    if (polygonsSRC) {
      polygonsSRC.setData(polygonData);
    }
  };

  _fetchFeatureAOI = (id, itemType) => {
    if (!isIREACTPointFeature(itemType)) {
      this.setState({ featureUpdating: true }, async () => {
        this.props.onAOILoading();
        try {
          const featureAOI = await fetchFeatureAOI(id, itemType);
          if (this.state.selectedFeature && this.state.selectedFeature.properties.id === id) {
            this._updatePolygonSrc(featureAOI);
          }
          this.props.onAOILoaded();
        } catch (err) {
          this.props.onAOILoaded();
          this.props.onError(err);
        }
      });
    } else {
      this._clearFeatureAOI();
      return;
    }
  };

  _clearFeatureAOI = () => {
    const map = this._getMap();
    const polygonsSRC = map.getSource(IREACT_FEATURES_AOI_SRC_NAME);
    if (polygonsSRC) {
      polygonsSRC.setData(EmptyFeatureCollection);
    }
  };

  _requestFeaturePosition = (feature, zoom, bbox, interactionType) => {
    const { id, itemType } = feature.properties;
    this.workerWrapper.send('findFeature', {
      interactionType: interactionType,
      featureId: id,
      itemType,
      zoom,
      bbox
    });
  };

  _updateMissionTaskFeaturesURLOnMap = url => {
    const map = this._getMap();
    if (map) {
      const featureTasksSrc = map.getSource(IREACT_MISSION_TASKS_SRC_NAME);
      if (featureTasksSrc) {
        featureTasksSrc.setData(url ? url : EmptyFeatureCollection);
      }
    }
  };

  _updateMissionMapFilter = id => {
    const map = this._getMap();
    if (!map) {
      return;
    }
    map.setFilter('single-missiontasks-features', getMissionTaskFilterDefinition(id));
    map.setFilter('single-missiontasks-feature-icons', getMissionTaskFilterDefinition(id));
  };

  _zoomToSelectedTaskIfNeeded = async (selectedMissionTask, selectedFeature) => {
    await updateSelectedMissionTaskLayers(this._getMap(), selectedMissionTask);
    if (selectedMissionTask !== null) {
      const center = selectedMissionTask.geometry
        ? selectedMissionTask.geometry.coordinates
        : selectedMissionTask.coordinates;
      this._zoomToPoint(center, 16);
    } else {
      if (selectedFeature && selectedFeature.properties.itemType === 'mission') {
        this._fetchFeatureAOI(selectedFeature.properties.id, selectedFeature.properties.itemType);
      }
    }
  };

  _zoomToSelectedFeatureIfNeeded = async selectedFeature => {
    await updateSelectedFeatureLayers(this._getMap(), selectedFeature);

    if (selectedFeature !== null) {
      if (isIREACTPointFeature(selectedFeature.properties.itemType)) {
        this._clearFeatureAOI();
        this.workerWrapper.send('zoomToFeature', {
          featureId: selectedFeature.properties.id,
          itemType: selectedFeature.properties.itemType,
          spiderBodyId: selectedFeature.properties.spiderBodyId || null
        });
      } else {
        // Fetch AOI
        this._fetchFeatureAOI(selectedFeature.properties.id, selectedFeature.properties.itemType);
        if (selectedFeature.properties.itemType === 'mission') {
          this._updateMissionMapFilter(selectedFeature.properties.id);
        } else {
          this._updateMissionMapFilter(-1);
        }
      }
    } else {
      this._clearFeature('select');
      this._updateMissionMapFilter(-1);
      // Clear AOI
      this._clearFeatureAOI();
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.featureLayersAdded === false &&
      this.state.featureLayersAdded === true &&
      this.props.ireactFeaturesIsUpdating === false
    ) {
      this.workerWrapper.send('init', {
        filterDefinitions: this.props.allFeatureFiltersDefinitions
      });
    }

    if (prevState.workerReady === false && this.state.workerReady === true) {
      this._requestClusterUpdate();
    }
    // TODO find some more accurate predicate property since this is flagged even if diff update returned 0 features
    if (
      prevProps.ireactFeaturesLastUpdated !== this.props.ireactFeaturesLastUpdated ||
      /* (prevProps.ireactFeaturesIsUpdating === true &&
        this.props.ireactFeaturesIsUpdating === false) */ !compareFilterDefinitions(
        // client-side filters changed
        prevProps.allFeatureFiltersDefinitions,
        this.props.allFeatureFiltersDefinitions
      )
    ) {
      if (this.state.featureLayersAdded) {
        //update features on map
        this.workerWrapper.send('update', {
          filterDefinitions: this.props.allFeatureFiltersDefinitions
        });
      }
    }
    if (!areFeaturesEqual(prevState.selectedFeature, this.state.selectedFeature)) {
      this._zoomToSelectedFeatureIfNeeded(this.state.selectedFeature);
    }
    if (!areFeaturesEqual(prevProps.selectedMissionTask, this.props.selectedMissionTask)) {
      this._zoomToSelectedTaskIfNeeded(this.props.selectedMissionTask, this.state.selectedFeature);
    }
    if (prevState.allFeaturesDataURL !== this.state.allFeaturesDataURL) {
      const map = this._getMap();
      if (map) {
        const allFeaturesSrc = map.getSource(IREACT_ALL_FEATURES_SRC_NAME);
        if (allFeaturesSrc && this.state.allFeaturesDataURL) {
          allFeaturesSrc.setData(this.state.allFeaturesDataURL);
        }
      }
    }

    if (!areFeaturesEqual(prevProps.selectedFeature, this.props.selectedFeature)) {
      if (this.props.selectedFeature !== null) {
        this._requestFeaturePosition(
          this.props.selectedFeature,
          this.props.zoom,
          this.props.bounds,
          'select'
        );
      } else {
        this._clearFeature('select');
        this._updateMissionMapFilter(-1);
        // Clear AOI
        this._clearFeatureAOI();
      }
    }

    if (prevProps.missionTaskFeaturesURL !== this.props.missionTaskFeaturesURL) {
      this._updateMissionTaskFeaturesURLOnMap(this.props.missionTaskFeaturesURL);
    }

    if (!arePointsEqual(prevProps.clickedPoint, this.props.clickedPoint)) {
      const map = this._getMap();
      if (map) {
        updateClickedPoint(map, this.props.clickedPoint);
      }
    }
    if (!areGeolocationsEqual(prevProps.geolocationPosition, this.props.geolocationPosition)) {
      const map = this._getMap();
      if (map) {
        updateUserPositionDataOnMap(
          map,
          prevProps.geolocationPosition,
          this.props.geolocationPosition
        );
        // if (this.props.geolocationPosition) {
        //   const { longitude, latitude } = this.props.geolocationPosition.coords;
        //   const center = [longitude, latitude];
        //   this._zoomToPoint(map, center, 16);
        // }
      }
    }
    if (!areBoundsEqual(prevProps.mapDataBounds, this.props.mapDataBounds)) {
      const map = this._getMap();
      if (map) {
        updateMapDataBounds(map, this.props.mapDataBounds);
      }
    }

    if (
      prevProps.activeSettingName !== this.props.activeSettingName &&
      this.props.activeSettingName === null
    ) {
      this.setState({
        timeSliderIndex: 0,
        timeSliderLabel: ''
      });
    }

    if (
      (prevProps.user === null && this.props.user !== null) ||
      (prevProps.user !== null &&
        this.props.user !== null &&
        prevProps.user.username !== this.props.user.username)
    ) {
      this.setState({
        geoserverService: this._getGeoserverServiceConfigFromProps(this.props.user)
      });
    } else if (prevProps.user !== null && this.props.user === null) {
      this.setState({
        geoserverService: null
      });
    }
  }

  componentDidMount() {
    this.workerWrapper = new WorkerWrapper({
      initOrUpdate: this._onWorkerInitOrUpdate,
      getClusters: this._onWorkerGetClusters,
      zoomToFeature: this._onWorkerZoomToFeature,
      getChildren: this._onWorkerGetChildren,
      findFeature: this._onWorkerFindFeature
    });
  }

  componentWillUnmount = () => {
    this.workerWrapper.terminate();
  };

  _deselectAnyContent = (clickedPoint = null) => {
    logMain('Will have to deselect anything');
    const p = clickedPoint; //this.props.selectedEvent || this.props.activeEvent ? null : clickedPoint;
    this.props.deselectFeature(p);
    // this.props.deselectEvent();
    this._clearFeatureAOI();
    // this.props.deselectTask();
  };

  _onMapStyleLoad = async () => {
    logMain('Map style loaded');
    if (this.state.initialStyleLoaded === false) {
      this.setState({ initialStyleLoaded: true });
    } else {
      const map = this._getMap();
      if (!map) {
        return;
      }
      await addGeolocationPositionFeaturesLayers(
        map,
        // MIN_ZOOM,
        // MAX_ZOOM,
        this.props.geolocationPosition,
        this.props.mapDataBounds
      );
      await addFeatureLayers(
        map,
        IREACT_FEATURES_SRC_NAME,
        IREACT_FEATURES_AOI_SRC_NAME,
        MIN_ZOOM,
        MAX_ZOOM,
        IREACT_MISSION_TASKS_SRC_NAME,
        this.props.missionTaskFeaturesURL
      );
      // await addEmergencyEventLayers(map, IREACT_EMERGENCY_EVENTS_SRC_NAME, MAX_ZOOM);
      await addSelectedFeatureLayers(
        map,
        IREACT_FEATURES_SRC_NAME,
        // IREACT_ALL_FEATURES_SRC_NAME,
        // this.state.allFeaturesDataURL,
        MIN_ZOOM
      );
      await addSelectedMissionTaskLayers(map, IREACT_MISSION_TASKS_SRC_NAME, MIN_ZOOM);
      await addClickedPositionFeaturesLayers(map, MIN_ZOOM, MAX_ZOOM);
      map.on('sourcedata', e => {
        const m = e.target;

        if (!m.isMoving()) {
          if (e.sourceId === IREACT_FEATURES_SRC_NAME) {
            if (this.props.selectedFeature !== null) {
              this._requestFeaturePosition(
                this.props.selectedFeature,
                this.props.zoom,
                this.props.bounds,
                'select'
              );
            }
          } else if (e.sourceId === IREACT_FEATURES_AOI_SRC_NAME) {
            if (e.source.data.features.length > 0) {
              const feature = e.source.data.features[0];
              const _onMoveEnd = () => {
                map.off('moveend', _onMoveEnd);
                this.setState({ featureUpdating: false });
              };
              map.on('moveend', _onMoveEnd);
              flyToPolygonFeature(map, feature);
            }
          }
        }
      });
      this.setState({ featureLayersAdded: true });
      if (this.state.workerReady) {
        this._requestClusterUpdate();
      } else if (!this.state.workerReady && this.state.featureLayersAdded) {
        //update features on map
        this.workerWrapper.send('update', {
          filterDefinitions: this.props.allFeatureFiltersDefinitions
        });
      }
    }
  };

  // _onMapLoad = () => {
  //   const map = this._getMap();
  //   logMain('Map Loaded', map.loaded());
  //   this.setState({ mapLoaded: map.loaded() });
  // };

  _onMapLoadEnd = async map => {
    logMain('Map load end');
    await this._onMapStyleLoad();
    this.setState({ mapLoaded: true });
    // attachCursorEvents(
    //   map,
    //   'single-features',
    //   this._onMouseEnterFeature,
    //   this.props.mouseLeaveFeature
    // );
    // attachCursorEvents(
    //   map,
    //   'single-missiontasks-features',
    //   this._onMouseEnterFeature,
    //   this.props.mouseLeaveMissionTask
    // );
    // this._addMapboxGlDraw(map);
  };

  _onMapMoveEnd = (map, mapSettings) => {
    logMain('Map move end', mapSettings);
    if (this.state.workerReady) {
      this._requestClusterUpdate();
    }
  };

  _findAndSelectFromCollection = async (id, itemType, collectionUrl) => {
    if (collectionUrl) {
      try {
        const response = await axios.get(collectionUrl);
        const features =
          response.data.type === 'FeatureCollection' ? response.data.features : response.data;
        const feature = features.find(
          f => f.properties.id === id && f.properties.itemType === itemType
        );
        if (feature) {
          this.props.selectFeature(feature);
        }
      } catch (err) {
        logSevereWarning('_findAndSelectFromCollection failure', err);
      }
    }
  };

  _onTimeSliderIndexUpdated = (index, label) => {
    this.setState({
      timeSliderIndex: index,
      timeSliderLabel: label
    });
  };

  _getFeatureInfoInner = async (pixelPoint, coordinates, map) => {
    const { defaultGeoServerURL, activeLayersSet } = this.props;
    if (activeLayersSet.length > 0) {
      const layer = activeLayersSet[this.state.timeSliderIndex];
      if (!layer) {
        return;
      }
      try {
        const canvas = map.getCanvas();
        const bounds = map.getBounds();

        const result = await getFeatureInfoUrl(
          pixelPoint,
          canvas.clientWidth,
          canvas.clientHeight,
          bounds,
          appInfo.backendEnvironment === 'dev'
            ? defaultGeoServerURL
            : layer.url || defaultGeoServerURL,
          layer.name,
          '1.1.0'
          // {
          //   apikey: this.state.geoserverService.key
          // }
        );
        // logMain('GetFeatureInfo response', result.data); // TODO pass this to a popup - get logic from FE
        this.props.onGetFeatureInfo({
          coordinates,
          features: result.data,
          label: this.state.timeSliderLabel
        });
      } catch (err) {
        logWarning(err);
      }
    }
  };

  _getFeatureInfo = e => {
    const map = e.target;
    const pixelPoint = e.point;
    const coordinates = e.lngLat.toArray();
    logMain('Context Menu');
    this._getFeatureInfoInner(pixelPoint, coordinates, map);
  };

  _onMapClick = e => {
    const map = e.target;
    const pixelPoint = e.point;
    const bounds = [
      [pixelPoint.x - boundsWidth / 2, pixelPoint.y - boundsHeight / 2],
      [pixelPoint.x + boundsWidth / 2, pixelPoint.y + boundsHeight / 2]
    ];
    const features = map.queryRenderedFeatures(bounds, {
      layers: ['clusters', 'single-features', 'single-missiontasks-features']
    });
    const aoiFeatures = map.queryRenderedFeatures(bounds, { layers: ['feature-polygon'] });

    const pointFeatures = features.filter(f => f.geometry.type === 'Point');
    if (pointFeatures.length === 0) {
      if (aoiFeatures.length === 0) {
        this._deselectAnyContent(e.lngLat.toArray());
      } else if (aoiFeatures[0].properties.itemType === 'mission') {
        this.props.deselectMissionTask();
      }
      // this.props.setClickedPoint(e.lngLat.toArray());
    } else if (pointFeatures[0].properties.cluster === true) {
      this.props.setClickedPoint(null);
      // Cluster Feature Click
      logMain('Cluster feature click', pointFeatures[0]);
      // Will make cluster expand
      this.workerWrapper.send('zoomToFeature', {
        featureId: pointFeatures[0].properties.cluster_id,
        itemType: 'cluster'
      });
    } else {
      this.props.setClickedPoint(null);
      const properties = pointFeatures[0].properties;
      logMain('Single Feature click', properties);
      if (properties.itemType === 'missionTask') {
        const missionTask = {
          properties: { ...pointFeatures[0].properties },
          geometry: pointFeatures[0].geometry
        };
        if (!Array.isArray(missionTask.properties.taskUsers)) {
          try {
            missionTask.properties.taskUsers = JSON.parse(missionTask.properties.taskUsers);
          } catch (er) {}
        }
        if (!Array.isArray(missionTask.properties.tools)) {
          try {
            missionTask.properties.tools = JSON.parse(missionTask.properties.tools);
          } catch (er) {}
        }
        this.props.selectMissionTask(missionTask);
      } else {
        if (properties.itemType === 'mission') {
          this.props.deselectMissionTask();
        }
        const { id, itemType } = pointFeatures[0].properties;
        this._findAndSelectFromCollection(id, itemType, this.state.allFeaturesDataURL);
        // this.props.selectFeature(pointFeatures[0]);
      }
    }
  };

  _onPositionButtonClick = async () => {
    const nextPosition = await this.props.requestGeolocationUpdate(true);
    const map = this._getMap();
    if (nextPosition.coords && map) {
      const { latitude, longitude } = nextPosition.coords;
      this._zoomToPoint(map, [longitude, latitude], 16);
    }
  };

  _onCompassClick = () => {
    const map = this._getMap();
    if (map) {
      map.easeTo({ bearing: 0, pitch: 0 });
    }
    // this.props.updateMapSettings({ bearing: 0, pitch: 0 });
  };

  render() {
    const {
      preferences,
      center,
      zoom,
      pitch,
      bearing,
      bounds,
      // mapDataBounds,
      updateMapSettings,
      selectedFeature,
      selectedMissionTask,
      deselectFeature,
      deselectMissionTask,
      missionTaskFeaturesURL,
      activeSettingName,
      activeSetting,
      activeLayersSet,
      removeAllLayers,
      mapHeight,
      defaultGeoServerURL,
      clickedPoint,
      buttonOffsetTop,
      // requestGeolocationUpdate,
      geolocationUpdating
    } = this.props;

    const mapSettings = {
      style: preferences.mapStyle,
      center: center,
      zoom: zoom,
      pitch: pitch,
      bearing: bearing,
      bounds: bounds,
      // maxBounds: mapDataBounds ? mapDataBounds : undefined,
      minZoom: MIN_ZOOM,
      maxZoom: MAX_ZOOM // TODO make it depend on mapStyle and/or layers
    };

    const map = this._getMap();
    const Slider =
      map && this.state.mapLoaded === true ? (
        <TimeSlider
          key="ts"
          id={activeSettingName}
          map={map}
          layers={activeLayersSet}
          activeSetting={activeSetting}
          activeSettingName={activeSettingName}
          removeAllLayers={removeAllLayers}
          defaultGeoServerURL={defaultGeoServerURL}
          onLayerUpdated={() => logMain('On Layer Updated?')}
          onIndexUpdated={this._onTimeSliderIndexUpdated}
        />
      ) : (
        <div key="ts" />
      );

    const requestProperties =
      this.state.selectedFeature &&
      this.state.selectedFeature.properties.itemType === 'reportRequest'
        ? this.state.selectedFeature.properties
        : null;

    // TODO use the condition on time slider
    return [
      <MapView
        key="mapview"
        style={{ height: mapHeight }}
        ref={this.mapView}
        mapSettings={mapSettings}
        onSettingsUpdate={updateMapSettings}
        onStyleLoad={this._onMapStyleLoad}
        onMapLoadEnd={this._onMapLoadEnd}
        onMapMoveEnd={this._onMapMoveEnd}
        onMapContextMenu={this._getFeatureInfo}
        onTransformRequest={this._transformRequest}
        onMapClick={this._onMapClick} // use touch start and touch end and determine length
        mapLoadingMessage={<CircularProgress />}
      />,
      activeSettingName ? <LegendButton key="legend-btn" offsetTop={buttonOffsetTop} /> : null,
      <Compass key="compass" onClick={this._onCompassClick} />,
      <PositionButton
        key="position-btn"
        onClick={this._onPositionButtonClick}
        geolocationUpdating={geolocationUpdating}
        offsetTop={buttonOffsetTop}
      />,
      <NewReportButton
        key="newreport-btn"
        offsetTop={buttonOffsetTop}
        requestProperties={requestProperties}
      />,
      Slider,
      <MapPopup
        key="map-popup"
        className="map-popup"
        clickedPoint={clickedPoint}
        selectedFeature={selectedFeature}
        selectedMissionTask={selectedMissionTask}
        allFeaturesDataURL={this.state.allFeaturesDataURL}
        missionTaskFeaturesURL={missionTaskFeaturesURL}
        deselectFeature={deselectFeature}
        deselectMissionTask={deselectMissionTask}
      />
    ];
  }
}

export default enhance(MapInner);
