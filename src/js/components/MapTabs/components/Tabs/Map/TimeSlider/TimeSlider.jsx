import React, { Component } from 'react';
import nop from 'nop';
import Close from 'material-ui/svg-icons/navigation/close';
import { IconButton, Slider, FontIcon } from 'material-ui';
import { LocalizedDate } from './LocalizeDate';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import { withMessages } from 'js/modules/ui';
import { iReactTheme } from 'js/startup/iReactTheme';
import { logError, logMain } from 'js/utils/log';
import { tileJSONIfy } from 'js/utils/layerUtils';
import { /* Legend, */ Metadata } from 'js/components/LayersLegend';
import {
  TSContainer,
  TSContainerInner,
  // TSMapRequestIndicator,
  TSHazardAndType,
  TSControls,
  TSLayerControls,
  TSDatePlusContainer,
  TSSliderContainer,
  // // TSThicks,
  TSLayerName,
  TSQuantity,
  TSActions
} from './commons';
import { LngLatBounds } from 'mapbox-gl';
import { appInfo } from 'js/utils/getAppInfo';

import wellknown from 'wellknown';
import bbox from '@turf/bbox';
import centroid from '@turf/centroid';
import intersect from '@turf/intersect';
import booleanWithin from '@turf/boolean-within';
import bboxPolygon from '@turf/bbox-polygon';
import { feature } from '@turf/helpers';
import styled from 'styled-components';
import { updateRasterLayersBBox } from '../mapboxStyles/addFeatureLayers';

function toGeoJSONFeature() {
  return bboxPolygon([this.getWest(), this.getSouth(), this.getEast(), this.getNorth()]);
}

LngLatBounds.prototype.toGeoJSONFeature = toGeoJSONFeature;

function parseLayerAOI(layerId, layerAOIWktString, mapCurrentBounds, center, mapMaxBounds = null) {
  let shouldUpdate = false;
  let zoomBounds = mapCurrentBounds;
  let layerAOIcenter = null;
  let layerAOIbounds = null;
  let layerAOIGeomPolygonFeature = null;
  const currentBoundsPolygonFeature = mapCurrentBounds.toGeoJSONFeature();

  try {
    const layerAOIGeom = wellknown.parse(layerAOIWktString);
    layerAOIcenter = centroid(layerAOIGeom);
    layerAOIbounds = bbox(layerAOIGeom);
    layerAOIGeomPolygonFeature = feature(layerAOIGeom, { layerId });
    // True if currentBoundsPolygonFeature is completely inside layerAOIGeomPolygonFeature
    const isAOIBiggerThanCurrentBounds = booleanWithin(
      currentBoundsPolygonFeature,
      layerAOIGeomPolygonFeature
    );
    const currentBoundsInters = intersect(currentBoundsPolygonFeature, layerAOIGeomPolygonFeature);
    const isAOIExternalToCurrentBounds = currentBoundsInters === null;

    if (!isAOIBiggerThanCurrentBounds || isAOIExternalToCurrentBounds) {
      if (mapMaxBounds) {
        const maxBoundPolygonFeature = mapMaxBounds.toGeoJSONFeature();
        const maxBoundsInters = intersect(maxBoundPolygonFeature, layerAOIGeomPolygonFeature);
        const isAOIBiggerThanMaxBounds = booleanWithin(
          maxBoundPolygonFeature,
          layerAOIGeomPolygonFeature
        );

        logMain('isAOIBiggerThanMaxBounds', isAOIBiggerThanMaxBounds, 'inters', maxBoundsInters);
        if (isAOIBiggerThanMaxBounds) {
          zoomBounds = mapCurrentBounds;
          shouldUpdate = false;
        } else if (maxBoundsInters) {
          const intersBbox = bbox(maxBoundsInters);
          zoomBounds = LngLatBounds.convert([
            [intersBbox[0], intersBbox[1]],
            [intersBbox[2], intersBbox[3]]
          ]);
          shouldUpdate = true;
        } else {
          zoomBounds = LngLatBounds.convert([
            [layerAOIbounds[0], layerAOIbounds[1]],
            [layerAOIbounds[2], layerAOIbounds[3]]
          ]);
          shouldUpdate = true;
        }
      } else {
        zoomBounds = LngLatBounds.convert([
          [layerAOIbounds[0], layerAOIbounds[1]],
          [layerAOIbounds[2], layerAOIbounds[3]]
        ]);
        shouldUpdate = false;
      }
    } else {
      shouldUpdate = false;
    }
  } catch (err) {
    logError(err);
  }

  return {
    bounds: layerAOIbounds,
    center: layerAOIcenter,
    zoomBounds,
    shouldUpdate,
    layerBoundsFeature: layerAOIGeomPolygonFeature
  };
}

const STYLES = {
  date: {
    color: iReactTheme.palette.canvasColor,
    backgroundColor: iReactTheme.palette.primary2Color,
    fontSize: 12,
    lineHeight: '16px',
    height: 16,
    textAlign: 'center',
    width: '80%'
  },
  icon: {
    boxSizing: 'border-box',
    padding: 2,
    width: 24,
    height: '100%'
  },
  legendIconButton: {
    boxSizing: 'border-box',
    padding: 0,
    width: 18,
    height: 18,
    lineHeight: '18px',
    fontSize: 16,
    margin: 'auto',
    display: 'block'
  },
  legendIcon: {
    boxSizing: 'border-box',
    padding: 0,
    width: 18,
    height: 18,
    lineHeight: '18px',
    fontSize: 16,
    margin: 'auto',
    display: 'block'
  },
  layerType: {
    fontSize: '0.725em'
  },
  num: {
    width: 'calc(100% - 8px)',
    textAlign: 'center',
    boxSizing: 'border-box'
  },
  slider: {
    zIndex: 10,
    width: '80%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sliderBar: {
    margin: 0
  }
};

const LAYER_MODE = USE_WMTS ? 'wmts' : 'wms';
const SCHEME = USE_WMTS ? 'xyz' : 'tms';
const UPDATE_INTERVAL = 10000; //10"

const ModalContenWrapper = styled.div`
  width: 100%;
  height: 90vw;
  overflow-x: hidden;
  overflow-y: auto;
`;

class TimeSlider extends Component {
  static defaultProps = {
    defaultGeoServerURL: null,
    layers: [],
    activeSettingName: [],
    removeAllLayers: nop,
    onIndexUpdated: nop
  };

  _interval = null;

  _autoRef = null;

  _setAutoRef = e => {
    this._autoRef = e;
  };

  state = {
    playing: false, // controls only the autoplay
    playingIndex: 0 // controls the layer displayed
  };

  _startInterval = () => {
    if (this._interval === null) {
      this._interval = setInterval(this._next, UPDATE_INTERVAL);
    }
  };

  _stopInterval = () => {
    if (this._interval !== null) {
      clearInterval(this._interval);
      this._interval = null;
    }
  };

  // Implement next in a loop like logic
  _next = () => {
    if (this._autoRef) {
      const max = this.props.layers.length - 1;
      this.setState(prevState => {
        const nextIndex = prevState.playingIndex + 1;
        return { playingIndex: nextIndex > max ? 0 : nextIndex };
      });
    }
  };

  // Implement next in a loop like logic
  _previous = () => {
    if (this._autoRef) {
      const min = 0;
      this.setState(prevState => {
        const nextIndex = prevState.playingIndex - 1;
        return { playingIndex: nextIndex < min ? this.props.layers.length - 1 : nextIndex };
      });
    }
  };

  _copernicusName = () => {
    if (
      this.props.layers[this.state.playingIndex] &&
      this.props.layers[this.state.playingIndex].extensionData &&
      this.props.layers[this.state.playingIndex].extensionData.attributes
    ) {
      const attributes = this.props.layers[this.state.playingIndex].extensionData.attributes;
      logMain('_copernicusName', attributes);
      return `[${attributes.Activation_Id}]: ${attributes.Map_Type}`;
    }
    return this.props.activeSetting ? this.props.activeSetting.name : '';
  };

  _onSliderDragStart = e => {
    e.stopPropagation();
  };

  _onSliderChange = (event, value) => {
    // TODO implement a transform/reverse value for handling date/time?
    // this.setState({ slider: transform(value) });
    this.setState({ playingIndex: value });
  };

  _play = () => {
    this.setState({ playing: true });
    this._next();
    logMain(`TimeSlider-${this.props.id} ▶️ Play`);
    this._startInterval();
  };

  _pause = () => {
    this.setState({ playing: false });
    logMain(`TimeSlider-${this.props.id} ⏹️ Paused`);
    this._stopInterval();
  };

  _togglePlay = () => {
    this.state.playing ? this._pause() : this._play();
  };

  _close = () => {
    this._pause();
    this._removeLayerFromMap(this.props.id);
    if (typeof this.props.removeAllLayers === 'function') {
      this.props.removeAllLayers();
    }
  };

  // _showLegend = () => {
  //   this.props.pushModalMessage(
  //     '_layers_legend',
  //     <ModalContenWrapper>
  //       <Legend />
  //     </ModalContenWrapper>,
  //     {
  //       _close: null
  //     }
  //   );
  // };

  _showLayerMetadata = metadataId => {
    this.props.pushModalMessage(
      `Metadata for metadataId=${metadataId}`,
      <ModalContenWrapper>
        <Metadata metadataId={metadataId} />
      </ModalContenWrapper>,
      {
        _close: null
      }
    );
  };

  _updateLayerOnMap = layer => {
    // TODO update map datasource for this layer
    //update layer source on this.props.map
    logMain(`TimeSlider-${this.props.id} layer updated`, layer);
    this._addOrUpdateLayerToMap(layer, this.props.id);
    this.props.onIndexUpdated(this.state.playingIndex, this._copernicusName());
  };

  _makeSourceName = name => name.toLocaleLowerCase().replace(' ', '_');

  _removeLayerFromMap = name => {
    const map = this.props.map;
    if (map && map.getStyle()) {
      updateRasterLayersBBox(map, name, null);

      const layer = map.getLayer(name);
      if (layer) {
        // Add a source
        map.removeLayer(name).removeSource(name);
        this.props.onLayerUpdated();
      }
    }
  };

  _addOrUpdateLayerToMap = (layer, layerGroupId) => {
    const map = this.props.map;
    if (map && layer) {
      const name = layerGroupId;
      const mapboxLayer = map.getLayer(name);
      const geoServerURL = GEOSERVER_URL
        ? GEOSERVER_URL
        : appInfo.backendEnvironment === 'dev'
        ? this.props.defaultGeoServerURL
        : layer.url || this.props.defaultGeoServerURL;
      const boundsData = parseLayerAOI(
        name,
        layer.areaOfInterest,
        map.getBounds(),
        map.getCenter(),
        map.getMaxBounds()
      );

      updateRasterLayersBBox(map, name, boundsData.layerBoundsFeature);

      const layerConfig = tileJSONIfy(
        layer.name,
        geoServerURL.replace(/\/$/, ''),
        LAYER_MODE,
        SCHEME,
        boundsData.bounds,
        boundsData.center,
        map.getMinZoom(),
        map.getMaxZoom()
      );
      if (mapboxLayer) {
        // Add a source - only GeoJSON source has setData()
        map.removeLayer(name).removeSource(name);
      }
      map.addLayer(
        {
          id: name,
          type: 'raster',
          source: {
            type: 'raster',
            ...layerConfig,
            tileSize: 256
          }
        },
        'clusters'
      ); // add before the clusters
      if (boundsData.shouldUpdate && boundsData.zoomBounds && !map.isMoving()) {
        map.fitBounds(boundsData.zoomBounds, { maxZoom: map.getMaxZoom() });
      }
      this.props.onLayerUpdated();
    }
  };

  componentWillUnmount() {
    this._stopInterval();
    if (typeof this.props.removeWidget === 'function') {
      this.props.removeWidget(this.props.id);
    }
    this._removeLayerFromMap(this.props.id);
  }

  componentDidMount() {
    // if (!this.props.defaultGeoServerURL) {
    //   throw new Error('Missing defaultGeoServerURL!');
    // }
    const layer = this.props.layers[this.state.playingIndex];
    if (layer) {
      this._updateLayerOnMap(layer);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.id !== this.props.id && this.props.id !== null) {
      //hope it's enough
      this._pause();
      this._removeLayerFromMap(prevProps.id);
      this.setState({ playingIndex: 0 });
      const layer = this.props.layers[0];
      if (layer) {
        this._updateLayerOnMap(layer);
      }
    } else if (prevState.playingIndex !== this.state.playingIndex) {
      const layer = this.props.layers[this.state.playingIndex];
      if (layer) {
        this._updateLayerOnMap(layer);
      }
    }
  }

  componentDidCatch(error) {
    logError(`TimeSlider-${this.props.id} error`, error);
    if (IS_PRODUCTION) {
      // log to app insight
      appInsights.trackException(
        error,
        'TimeSlider::componentDidCatch',
        this.props,
        {},
        SeverityLevel.Error
      );
    }
  }

  render() {
    const { activeSetting, /* removeAllLayers, style, */ layers } = this.props;
    const { playingIndex, playing } = this.state;
    const numLayers = layers.length;

    const layer = layers[playingIndex];

    const name = this._copernicusName();

    if (!layer || !activeSetting) {
      return null;
    } else {
      const {
        /* lastItem, */ style,
        /* layers, */ hazard,
        code,
        layerType /* isCopernicus */
      } = activeSetting;

      const { leadTime, timeSpan, metadataId, timespan } = layer;
      const playStatus = playing ? 'pause' : 'play_arrow'; // circle and circle filled available
      const disabled = numLayers === 1;

      return (
        <TSContainer ref={this._setAutoRef} className="time-slider" lastItem={false} style={style}>
          <TSContainerInner>
            <TSHazardAndType>
              <span className="hazard" style={STYLES.layerType}>
                {hazard}
              </span>
              <span className="group" style={STYLES.layerType}>
                {layerType}
              </span>
              <span className="group" style={STYLES.layerType}>
                {code}
              </span>
            </TSHazardAndType>
            <TSControls>
              <TSLayerName>{name}</TSLayerName>
              <TSDatePlusContainer style={{ padding: '0 4px 0 8px' }}>
                <LocalizedDate
                  date={leadTime}
                  timespan={timespan || timeSpan}
                  style={STYLES.date}
                />
                <IconButton
                  style={STYLES.legendIconButton}
                  iconStyle={STYLES.legendIcon}
                  onClick={this._showLayerMetadata.bind(this, metadataId)}
                  // tooltip="Show metadata"
                  // tooltipPosition="top-center"
                >
                  <FontIcon style={STYLES.legendIcon} className="material-icons">
                    receipt
                  </FontIcon>
                </IconButton>
                {/* <IconButton
                  style={STYLES.legendIcon}
                  iconStyle={STYLES.legendIcon}
                  onClick={this._showLegend}
                  // tooltip="Show Legend"
                  // tooltipPosition="top-center"
                >
                  <FontIcon style={STYLES.legendIcon} className="material-icons">
                    list
                  </FontIcon>
                </IconButton> */}
              </TSDatePlusContainer>
              <TSLayerControls>
                <IconButton
                  style={STYLES.icon}
                  iconStyle={STYLES.icon}
                  onClick={this._togglePlay}
                  disabled={disabled}
                  iconClassName="material-icons"
                  // tooltip="play"
                  // tooltipPosition="top-center"
                >
                  {playStatus}
                </IconButton>
                <TSSliderContainer className="slider-container">
                  {/* <TSThicks thickPositions={this._getThicksPositions()} /> */}
                  <Slider
                    style={STYLES.slider}
                    disabled={disabled}
                    onDragStart={this._onSliderDragStart}
                    sliderStyle={STYLES.sliderBar}
                    min={0}
                    max={numLayers === 1 ? 1 : numLayers - 1}
                    onChange={this._onSliderChange}
                    // TODO implement a transform/reverse value for handling date/time?
                    step={1}
                    value={playingIndex}
                  />
                </TSSliderContainer>
                <IconButton
                  style={STYLES.icon}
                  onClick={this._previous}
                  disabled={disabled}
                  // tooltip="Previous"
                  // tooltipPosition="top-center"
                >
                  <FontIcon style={STYLES.icon} className="material-icons">
                    skip_previous
                  </FontIcon>
                </IconButton>
                <IconButton
                  style={STYLES.icon}
                  onClick={this._next}
                  disabled={disabled}
                  // tooltip="Next"
                  // tooltipPosition="top-center"
                >
                  <FontIcon style={STYLES.icon} className="material-icons">
                    skip_next
                  </FontIcon>
                </IconButton>
              </TSLayerControls>
            </TSControls>
            <TSActions>
              <IconButton style={{ padding: 0 }} tooltip="Clear" onClick={this._close}>
                <Close />
              </IconButton>
              <TSQuantity className="num-layers">
                <span style={{ ...STYLES.num, borderBottom: '1px solid' }}>{playingIndex + 1}</span>
                <span style={STYLES.num}>{numLayers}</span>
              </TSQuantity>
            </TSActions>
            {/*             <small>{name}</small>
            <IconButton tooltip="Clear" onClick={removeAllLayers}>
              <Close />
            </IconButton> */}
          </TSContainerInner>
        </TSContainer>
      );
    }
  }
}

export default withMessages(TimeSlider); //withLayersAndSettings(TimeSlider);
