import { lineString } from '@turf/helpers';
import destination from '@turf/destination';

// DEFAULTS
const MAX_ITEMS = 8; // max item per spiral circle
const LD = 0.01; //0.001; // line radius in km
const LDU = 'kilometers'; // line radius distance

const DEFAULT_PARAMS = {
  maxItemsPerCircle: MAX_ITEMS,
  lineDistance: LD,
  distanceStep: LD * 0.5,
  lineDistanceUnit: LDU
};

export class Spiderifier {
  constructor(params = DEFAULT_PARAMS) {
    const { maxItemsPerCircle, lineDistance, distanceStep, lineDistanceUnit } = params;

    this._maxItemsPerCircle = maxItemsPerCircle;
    this._lineDistance = lineDistance;
    this._distanceStep = distanceStep;
    this._distanceOptions = { units: lineDistanceUnit };

    this.destination = destination;
    this.lineString = lineString;
  }

  _getDistance = (centerCoords, distance, alpha) => {
    return this.destination(centerCoords, distance, alpha, this._distanceOptions);
  };

  /**
   *
   * @param {GeoJSONPointGeometry} clusterCenter - coming from supercluster
   * @param {Array<GeoJSONPointFeature>} features - children of the cluster
   */
  _getSpiderifiedFeatures = (clusterCenter, features) => {
    const alpha = 360 / Math.min(this._maxItemsPerCircle, features.length); // degrees
    let spider = [clusterCenter];
    const clusterId = clusterCenter.properties.cluster_id; // comes from supercluster
    const nPoints = features.length;
    for (let i = 0; i < nPoints; i++) {
      const loopNumber = Math.floor(i / this._maxItemsPerCircle);
      const _alphaOffset = alpha > 0 && loopNumber > 0 ? alpha / (loopNumber + 1) : 0;
      const _alpha = (i * alpha + _alphaOffset) % 360;

      const _distance = this._lineDistance + loopNumber * this._distanceStep;
      const point = this._getDistance(clusterCenter.geometry, _distance, _alpha);
      point.properties = features[i].properties;
      const line = this.lineString(
        [clusterCenter.geometry.coordinates, point.geometry.coordinates],
        {
          leg_id: `leg-${clusterId}-${i}`,
          isLeg: true
        }
      );
      spider = [...spider, point, line];
    }
    return spider;
  };

  /**
   *
   * @param {Array<GeoJSONPointFeature>} clusters - clusters, coming from supercluster
   * @param {Array<Array<GeoJSONPointFeature>>} clusterChildren - children of the cluster
   * @return Array<GeoJSONPointFeature>
   */
  getSpiderifiedFeatures = (clusters, clusterChildren) => {
    if (Array.isArray(clusters)) {
      return clusters
        .map(
          (cluster, i) =>
            cluster.properties.cluster === true
              ? this._getSpiderifiedFeatures(cluster, clusterChildren[i])
              : [cluster]
        )
        .reduce((prev, next) => [...prev, ...next], []);
    } else if (clusters.type === 'Feature') {
      return this._getSpiderifiedFeatures(clusters, clusterChildren);
    } else {
      return [];
    }
  };
}
