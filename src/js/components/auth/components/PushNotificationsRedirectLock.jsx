import React from 'react';
import { withPushNotifications } from 'js/modules/pushNotifications';
import { Redirect } from 'react-router-dom';
import { withRouter } from 'react-router';
import { withMessagesOnly } from 'js/modules/ui';

const DEFAULT_NOTIFICATIONS_PAGE = '/tabs/emergencyCommunications';

function getRedirectPathnameForNotification(notification) {
  if (!notification) {
    return null;
  }
  let pathname = DEFAULT_NOTIFICATIONS_PAGE;
  const type = notification.type
    ? notification.type
    : notification.additionalData.customProp.Type || notification.additionalData.customProp.type;
  switch (type) {
    case 'event':
    case 'EmergencyEvent':
    case 'emergencyEvent':
      pathname = '/tabs/events';
      break;
    case 'gamification':
      pathname = '/achievements/community/leaderboard';
      break;
    case 'ReportRequest':
    case 'reportRequest':
    case 'Alert':
    case 'alert':
    case 'Warning':
    case 'warning':
    default:
      break;
  }
  console.log(
    'getRedirectPathnameForNotification:Notification',
    type,
    '-->',
    pathname,
    notification
  );
  return pathname;
}

const PushNotificationsRedirectLock = Component => {
  class Lock extends React.Component {
    _clearNotifications(props) {
      if (props.hasPushNotifications) {
        const notification = props.pushNotifications[0];
        props.setPushNotificationRead(notification);
        const isForeground = notification.additionalData.foreground === true;
        if (isForeground) {
          props.pushMessage(notification.message);
        }
      }
    }

    componentDidMount() {
      this._clearNotifications(this.props);
    }

    componentDidUpdate() {
      this._clearNotifications(this.props);
    }

    render() {
      const {
        pushNotifications,
        pushNotificationsReady,
        hasPushNotifications,
        pushNotificationsAvailableForPlatform,
        pushNotificationsPermissionsEnabled,
        pushNotificationsError,
        // actions
        initializePushNotifications,
        checkUserPermissionsForPushNotifications,
        unsubscribePushNotifications,
        setPushNotificationRead,
        clearPushNotificationsError,
        // ui
        pushMessage,
        // router
        match,
        location,
        history,
        ...rest
      } = this.props;

      if (hasPushNotifications) {
        const notification = pushNotifications[0];
        const isForeground = notification.additionalData.foreground === true;
        if (isForeground) {
          // pushMessage(notification.message);
          return <Component {...rest} />;
        } else {
          const notificationRedirectPathname = getRedirectPathnameForNotification(notification);

          if (location.pathname === notificationRedirectPathname) {
            return <Component {...rest} />;
          } else {
            return <Redirect to={notificationRedirectPathname} push={false} />;
          }
        }
      } else {
        return <Component {...rest} />;
      }
    }
  }
  return withRouter(withPushNotifications(withMessagesOnly(Lock)));
};

export default PushNotificationsRedirectLock;
