import React, { Component } from 'react';
import { withMissions, withLogin } from 'ioc-api-interface';
import { Header, Content, HeaderButton } from 'js/components/app';
import { withRouter } from 'react-router';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { FlatButton } from 'material-ui';
import { withMessages } from 'js/modules/ui';
import { withDictionary } from 'ioc-localization';
import { localizeDate } from 'js/utils/localizeDate';
import { compose } from 'redux';
import {
  DetailsIcon,
  MissionColorDot,
  MDMissionTitle,
  MDTitleAddressLine,
  ChipTookOverBy,
  ChipNoneTookOver,
  MissionUpdater
} from 'js/components/MissionCard';
import {
  TDInfo,
  TDDescription,
  TDContents,
  TDOrganization,
  TDLine,
  TDFooter,
  TDContentLine
} from './components';
import moment from 'moment';
import { openMapAppLink } from 'js/utils/getAssets';

const POS_PRECISION = 5;

const GoToMap = HeaderButton('place', '_map');
const enhance = compose(
  withDictionary,
  withLogin
);

const assigneeBadgeStyle = { margin: 0, height: 18, lineHeight: '16px' };

function isMine(taskProperties, userId) {
  const { taskUsers } = taskProperties;
  return taskUsers.findIndex(tu => tu.user.id === userId) > -1;
}

const MissionTaskActions = withMessages(
  withMissions(
    ({
      userId,
      taskProperties,
      history,
      dictionary,
      accomplishTask,
      takeChargeOfTask,
      releaseTaskOfMission,
      pushMessage,
      pushError
    }) => {
      const { isCompleted, isTookOver, temporalStatus, id } = taskProperties;
      if (isCompleted || temporalStatus === 'completed') {
        return [
          <div key={0} style={{ textTransform: 'uppercase' }}>
            {dictionary._missiontask_accomplished}
          </div>,
          <DetailsIcon key={1} iconText="check_circle" style={{ fontSize: 32 }} />
        ];
      } else {
        const downButtonLabel = isTookOver ? '_missiontask_dismiss' : '_missiontask_decline';
        const downButtonHandler = isTookOver
          ? releaseTaskOfMission.bind(this, id)
          : () => history.goBack();
        const upButtonLabel = isTookOver ? '_missiontask_accomplish' : '_missiontask_accept';
        const upButtonHandler = async () => {
          try {
            const handler = isTookOver ? accomplishTask : takeChargeOfTask;

            const message = isTookOver ? 'Task Accomplished!' : 'Task taken in charge';
            const status = isTookOver ? 'success' : 'default';

            await handler(id);
            pushMessage(message, status);
          } catch (err) {
            console.error('Error', err);
            if (
              err.details &&
              err.details.status === 400 &&
              err.details.serverDetails &&
              err.details.serverDetails.error
            ) {
              const ssError = err.details.serverDetails.error;
              pushError(ssError.details);
            } else {
              pushError(dictionary(err.message));
            }
          }
        };
        if (isTookOver) {
          return [
            <FlatButton
              key={0}
              disabled={isTookOver && !isMine(taskProperties, userId)}
              label={dictionary[downButtonLabel]}
              onClick={downButtonHandler}
            />,
            <FlatButton
              key={1}
              disabled={isTookOver && !isMine(taskProperties, userId)}
              label={dictionary[upButtonLabel]}
              onClick={upButtonHandler}
            />
          ];
        } else {
          return [
            <div key={0} />,
            // <FlatButton key={0}
            //     disabled={isTookOver && !isMine(taskProperties, userId)}
            //     label={dictionary[downButtonLabel]}
            //     onClick={downButtonHandler}
            // />,
            <FlatButton
              key={1}
              disabled={isTookOver && !isMine(taskProperties, userId)}
              label={dictionary[upButtonLabel]}
              onClick={upButtonHandler}
            />
          ];
        }
      }
    }
  )
);

class MissionTaskDetails extends Component {
  componentDidMount() {
    const taskId = parseInt(this.props.match.params.taskId, 10);
    if (this.props.selectedTaskId !== taskId) {
      this.props.selectTask(taskId);
    }
  }

  _showThisTaskOnMap = e => {
    e.stopPropagation();
    e.preventDefault();
    this.props.history.replace('/tabs/map');
  };

  render() {
    const {
      dictionary,
      locale,
      selectedTask,
      muiTheme,
      history,
      user,
      featuresUpdating,
      missionTasksUpdating
    } = this.props;
    console.log(featuresUpdating, missionTasksUpdating);
    if (selectedTask) {
      // const NA = dictionary._not_available; // fallback value
      const {
        /* id, */ address,
        description,
        start,
        end,
        temporalStatus,
        isCompleted,
        isTookOver,
        taskUsers,
        numberOfNecessaryPeople,
        tools,
        organization,
        organizationLogo
      } = selectedTask.properties;
      const coordinates = selectedTask ? selectedTask.geometry.coordinates : null;

      const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
      const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
      const taskStartDateString = `${dictionary('_event_date_started', dateStartedString)}`;
      const taskEndDateString = `${
        end ? '' + dictionary('_event_date_ended', dateFinishedString) : ''
      }`;
      // const missionDateString = `${dictionary('_event_date_started', dateStartedString)}${end ? ' - ' + dictionary('_event_date_ended', dateFinishedString) : ''}`;
      const toolsString = tools.length > 0 ? tools.map(t => t.name).join(', ') : '-';
      const assignee =
        isTookOver || isCompleted
          ? taskUsers
              .map(tu => (tu.user.id === user.id ? dictionary._me : tu.user.userName))
              .join(', ')
          : null;

      return (
        <div className="mission-details-page md page">
          <Header
            title={
              <MDMissionTitle className="header-title">
                <div className="taskdetails-title">
                  <span>{dictionary._missiontask_details}</span>
                  <span>
                    {Array.isArray(coordinates) && (
                      <span onClick={openMapAppLink(coordinates)}>
                        {coordinates[1].toFixed(POS_PRECISION)},{' '}
                        {coordinates[0].toFixed(POS_PRECISION)}
                      </span>
                    )}
                  </span>
                </div>
                <MDTitleAddressLine>{address}</MDTitleAddressLine>
              </MDMissionTitle>
            }
            leftButtonType="back"
            rightButton={<GoToMap onClick={this._showThisTaskOnMap} />}
          />
          <Content className="md-content">
            <TDInfo className="md-info">
              <TDLine>
                <DetailsIcon iconText="event" />
                <span>{taskStartDateString}</span>
                <MissionColorDot temporalStatus={temporalStatus} />
              </TDLine>
              {end && (
                <TDLine>
                  <DetailsIcon iconText="event" />
                  <span>{taskEndDateString}</span>
                </TDLine>
              )}
              <TDLine>
                <DetailsIcon iconText="label_outline" />
                <span>
                  {`${dictionary._missiontask_acceptedBy}:`}
                  &nbsp;
                </span>
                {assignee ? (
                  <ChipTookOverBy style={assigneeBadgeStyle}>{assignee}</ChipTookOverBy>
                ) : (
                  <ChipNoneTookOver style={assigneeBadgeStyle}>
                    {dictionary._nobody}
                  </ChipNoneTookOver>
                )}
              </TDLine>
            </TDInfo>
            <TDDescription className="md-description">{description}</TDDescription>
            <TDContents className="md-contents">
              <TDContentLine
                iconText="access_time"
                label={dictionary._missiontask_remaining_time}
                valueString={moment(end).fromNow()}
              />
              <TDContentLine
                iconText="person"
                label={dictionary._missiontask_agents_needed}
                valueString={numberOfNecessaryPeople}
              />
              <TDContentLine
                iconText="build"
                label={dictionary._missiontask_tools}
                valueString={toolsString}
                bottomBorder={false}
              />
            </TDContents>
            <TDOrganization
              className="md-organization"
              organizationName={organization || 'Organization'}
              avatarImageURL={organizationLogo}
            />
            <TDFooter
              className="md-footer"
              style={
                isCompleted
                  ? { backgroundColor: muiTheme.palette.validatedColor, justifyContent: 'flex-end' }
                  : {}
              }
            >
              <MissionTaskActions
                userId={user.id}
                history={history}
                dictionary={dictionary}
                taskProperties={selectedTask.properties}
              />
            </TDFooter>
          </Content>
          <MissionUpdater />
        </div>
      );
    } else {
      return (
        <div className="missiontask-details-page page">
          <Header title={<div>{dictionary._missiontask_details}</div>} leftButtonType="back" />
          <div>No selected mission - go back and select one</div>
          <MissionUpdater />
        </div>
      );
    }
  }
}

export default muiThemeable()(withRouter(enhance(MissionTaskDetails)));
