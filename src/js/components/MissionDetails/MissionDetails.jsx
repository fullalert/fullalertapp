import React, { Component } from 'react';
import { Header, Content, HeaderButton } from 'js/components/app';
import { withRouter } from 'react-router';
import { withDictionary } from 'ioc-localization';
import { withMissions, withLogin } from 'ioc-api-interface';
import { compose } from 'redux';
import {
  HEADER_COLOR,
  DetailsIcon,
  getCompletedTaskIdsFilterFn,
  MissionColorDot,
  MDMissionTitle,
  MDTitleAddressLine,
  MDDescriptionContainer,
  MDStatusContainer,
  MDStatusLine,
  MDProgressLine,
  MDProgressContainer,
  MDListContainer,
  MDListItemContainer,
  MissionTaskCard,
  MissionUpdater
} from 'js/components/MissionCard';
import muiThemeable from 'material-ui/styles/muiThemeable';
import { LinearProgress, DropDownMenu, MenuItem } from 'material-ui';
import { localizeDate } from 'js/utils/localizeDate';
import { SORTERS } from 'js/modules/missionsFilters';

const GoToMap = HeaderButton('place', '_map');

const enhance = compose(
  withDictionary,
  withMissions,
  withLogin
);

const MDListItem = ({
  missionTask,
  currentUserId,
  selectedTaskId,
  onMapButtonClick,
  toggleSelection
}) => (
  <MDListItemContainer
    className="task-item"
    onClick={e => {
      e.preventDefault();
      e.stopPropagation();
      toggleSelection(missionTask.properties.id);
    }}
  >
    <MissionTaskCard
      missionTask={missionTask}
      showMapIcon={true}
      isSelected={selectedTaskId === missionTask.properties.id}
      currentUserId={currentUserId}
      onMapButtonClick={onMapButtonClick.bind(null, missionTask.properties.id)}
    />
  </MDListItemContainer>
);

const { start_asc, start_desc, end_asc, end_desc } = SORTERS;

// Tasks don't have last modified
const TASK_SORTERS = {
  start_asc,
  start_desc,
  end_asc,
  end_desc
};

const availableSorters = Object.keys(TASK_SORTERS);

const DEFAULT_TASK_SORTERS = 'start_asc';

const SorterMenu = ({ dictionary, sorter, onSorterChange }) => (
  <DropDownMenu
    className="md-sorter-menu"
    iconStyle={{ top: -8, right: 8, height: 16, lineHeight: '16px', padding: 2 }}
    labelStyle={{
      width: '100%',
      height: 16,
      lineHeight: '16px',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      fontSize: 13
    }}
    menuStyle={{ maxWidth: '100%', overflow: 'hidden' }}
    style={{ width: '50%', height: 16, lineHeight: '16px', marginLeft: 'auto', fontSize: 13 }}
    autoWidth={false}
    onChange={onSorterChange}
    value={sorter}
    menuItemStyle={{ padding: 4, maxWidth: 'calc(100% - 8px)', border: 0 }}
  >
    {availableSorters.map((sorter, i) => (
      <MenuItem key={i} value={sorter} primaryText={dictionary(`_${sorter}`)} />
    ))}
  </DropDownMenu>
);

class MissionDetails extends Component {
  state = { sorter: DEFAULT_TASK_SORTERS };
  _sorterChange = (event, index, sorter) => this.setState({ sorter });

  componentDidMount() {
    const missionId = parseInt(this.props.match.params.id, 10);
    if (this.props.selectedMissionId !== missionId) {
      this.props.selectMission(missionId);
    }
  }

  _showTaskOnMap = (taskId, e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.selectTask(taskId);
    this.props.history.replace('/tabs/map');
  };

  _toggleSelection = taskId => {
    if (taskId === this.props.selectedTaskId) {
      this.props.deselectTask();
    } else {
      this.props.selectTask(taskId);
      const missionId = this.props.selectedMissionId;
      this.props.history.push(`/missiondetails/${missionId}/${taskId}`);
    }
  };

  _showThisMissionOnMap = e => {
    e.stopPropagation();
    e.preventDefault();
    this.props.history.replace('/tabs/map');
  };

  render() {
    const {
      dictionary,
      locale,
      missionTasks,
      selectedMission,
      selectedTaskId,
      muiTheme,
      user
    } = this.props;
    const currentSorter = TASK_SORTERS[this.state.sorter];

    if (selectedMission) {
      const { palette } = muiTheme;
      const {
        address,
        title,
        start,
        end,
        taskIds,
        temporalStatus,
        missionAssigneeLabel
      } = selectedMission.properties;
      const completedTasks = missionTasks.filter(getCompletedTaskIdsFilterFn(taskIds));
      const numOfCompletedTasks = completedTasks.length;
      const totalMissionTasks = taskIds.length;
      const percentCompleted = (numOfCompletedTasks / totalMissionTasks) * 100;
      const toBeProcessed = totalMissionTasks - numOfCompletedTasks;
      const dateStartedString = start ? localizeDate(start, locale, 'L LT') : '';
      const dateFinishedString = end ? localizeDate(end, locale, 'L LT') : '';
      const missionStartDateString = dictionary('_event_date_started', dateStartedString);
      const missionEndDateString = end ? dictionary('_event_date_ended', dateFinishedString) : '';
      // const dateStartedString = start ? localizeDate(start, locale, 'll') : '';
      // const dateFinishedString = end ? localizeDate(end, locale, 'll') : '';
      // const missionDateString = `${dictionary('_event_date_started', dateStartedString)}${end ? ' - ' + dictionary('_event_date_ended', dateFinishedString) : ''}`;
      return (
        <div className="mission-details-page md page">
          <Header
            style={{ backgroundColor: HEADER_COLOR }}
            title={
              <MDMissionTitle className="header-title">
                <div>{dictionary._mission_details}</div>
                <MDTitleAddressLine>{address}</MDTitleAddressLine>
              </MDMissionTitle>
            }
            leftButtonType="back"
            rightButton={<GoToMap onClick={this._showThisMissionOnMap} />}
          />
          <Content className="md-content">
            <MDDescriptionContainer>{title}</MDDescriptionContainer>
            <MDStatusContainer>
              <MDStatusLine>
                <DetailsIcon iconText="event" />
                <span>{missionStartDateString}</span>
                <MissionColorDot temporalStatus={temporalStatus} />
              </MDStatusLine>
              {end && (
                <MDStatusLine>
                  <DetailsIcon iconText="event" />
                  <span>{missionEndDateString}</span>
                </MDStatusLine>
              )}
              <MDStatusLine>
                <DetailsIcon iconText="people" />
                <span>{missionAssigneeLabel}</span>
              </MDStatusLine>
            </MDStatusContainer>
            <MDProgressContainer>
              <MDProgressLine>
                <span>{`${taskIds.length} TASKS`}</span>
                <span style={{ fontSize: 11 }}>
                  {dictionary('_mission_details_tasks_list_items', toBeProcessed)}
                </span>
              </MDProgressLine>
              <LinearProgress
                className="progress-bar"
                color={palette.missionProgressBarColor}
                style={{ margin: 0, width: '90%' }}
                mode="determinate"
                value={percentCompleted}
              />
              <SorterMenu
                dictionary={dictionary}
                onSorterChange={this._sorterChange}
                sorter={this.state.sorter}
              />
            </MDProgressContainer>
            <MDListContainer>
              {missionTasks
                .filter(t => taskIds.indexOf(t.properties.id) > -1)
                .sort(currentSorter)
                .map((t, i) => (
                  <MDListItem
                    key={i}
                    missionTask={t}
                    currentUserId={user.id}
                    selectedTaskId={selectedTaskId}
                    onMapButtonClick={this._showTaskOnMap}
                    toggleSelection={this._toggleSelection}
                  />
                ))}
            </MDListContainer>
          </Content>
          <MissionUpdater />
        </div>
      );
    } else {
      return (
        <div className="mission-details-page page">
          <Header title={<div>{dictionary._mission_details}</div>} leftButtonType="back" />
          <div>No selected mission - go back and select one</div>
          <MissionUpdater />
        </div>
      );
    }
  }
}

export default muiThemeable()(withRouter(enhance(MissionDetails)));
