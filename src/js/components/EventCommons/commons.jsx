import React /* , { Component } */ from 'react';
import { Chip, Avatar, IconButton } from 'material-ui';

import { HAZARD_TYPES_LETTERS } from 'js/components/ReportsCommons';

export function getDisplayName(event, fallback = '') {
  // const dispName = event.displayName ? event.displayName.length > 40 ? event.displayName.substring(0, 40) + '...' : event.displayName : fallback;
  // const eventCode = event.eventCode || 'Unknown-Code';
  // return dispName.length > 0 ? `${eventCode}: ${dispName}` : eventCode.replace(/-$/, '');
  const eventCode = event.eventCode || 'Unknown-Code';
  return eventCode.replace(/-$/, '');
}

export function getAddress(event) {
  if (event.address) {
    return event.address;
  } else {
    const isValid = e => typeof e === 'string' && e.length;
    const locations = Array.isArray(event.locations)
      ? event.locations.filter(isValid).join(', ')
      : null;
    const provinces = Array.isArray(event.provinces)
      ? event.provinces.filter(isValid).join(', ')
      : null;
    const countryNames = Array.isArray(event.countryNames)
      ? event.countryNames
          .filter(isValid)
          .map((cn, i) => `${cn} (${event.countryCodes[i]})`)
          .join(', ')
      : null;
    return `${locations ? locations + ' - ' : ''}${provinces ? provinces + '  - ' : ''}${
      countryNames ? countryNames : ''
    }`;
  }
}

export const LocateButton = ({ onClick, color }) => (
  <IconButton onClick={onClick} iconStyle={{ color }}>
    <i className="material-icons md-18 md-light eventCardIcon">location_on</i>
  </IconButton>
);

export const EventChip = ({ event, palette, dictionary }) => (
  <Chip className="hazard-chip" style={{ backgroundColor: palette.backgroundColor }}>
    <Avatar
      className="ireact-pinpoint-icons"
      size={24}
      style={{ height: 24, width: 24 }}
      color={event.isOngoing ? palette.textColor : palette.canvasColor}
      backgroundColor={event.isOngoing ? palette.ongoingEventsColor : palette.closedEventsColor}
    >
      {HAZARD_TYPES_LETTERS[event.hazard]}
    </Avatar>
    <div className="hazard-label">
      <span>{dictionary['_' + event.hazard]}</span>
    </div>
  </Chip>
);
