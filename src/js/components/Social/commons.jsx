import styled from 'styled-components';
import {
  TWITTER_BLUE,
  TWEET_POSITIVE_TAG_COLOR,
  TWEET_NEGATIVE_TAG_COLOR
} from 'js/startup/iReactTheme';

export const Effect = styled.div`
  background-color: ${props => props.theme.palette.tweet};
`;

export function linkifyTweet(text) {
  // parse urls
  text = text.replace(/[A-Za-z]+:\/\/[A-Za-z0-9_-]+\.[A-Za-z0-9_:%&~?/.=-]+/g, url => {
    return `<span class="tagged" contenteditable="false" data-url="${url}">${url}</span>`;
  });
  // parse usernames
  text = text.replace(/[@]+[A-Za-z0-9_-]+/g, u => {
    const username = u.replace('@', '');
    return `<span class="tagged" contenteditable="false" data-url="https://twitter.com/${username}">${u}</span>`;
  });
  // parse hashtags
  text = text.replace(/[#]+[A-Za-z0-9_-]+/g, t => {
    const tag = t.replace('#', '%23');
    return `<span class="tagged" contenteditable="false" data-url="https://twitter.com/search?q=${tag}">${t}</span>`;
  });
  return { __html: text };
}

export const TweetCardContainer = styled.div.attrs(props => ({ className: 'tweet-card' }))`
  padding: 8px;
  box-sizing: border-box;
  width: 100%;
  height: 130px;
  display: flex;
`;

export const TweetCardContent = styled.div.attrs(props => ({ className: 'tweet-card-content' }))`
  width: calc(100% - 24px);
  height: 100%;

  a,
  span.tagged {
    color: ${TWITTER_BLUE};
  }
`;

export const TweetCardActions = styled.div.attrs(props => ({ className: 'tweet-card-actions' }))`
  width: 24px;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: space-between;
  justify-content: space-around;
`;

const infoHeight = 16; //px
const lines = 3;
const contentPadding = 8;
const lineHeight = Math.round((130 - 2 * infoHeight - 32) / lines);

export const TweetCardText = styled.div.attrs(props => ({ className: 'tweet-card-text' }))`
  width: 100%;
  height: calc(100% - ${2 * infoHeight + 2 * contentPadding}px);
  line-clamp: ${lines};
  -webkit-line-clamp: ${lines};
  overflow: hidden;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  font-size: ${Math.round(0.75 * lineHeight)}px;
  line-height: ${lineHeight}px;
  font-weight: 200;
  margin: ${contentPadding}px 0;
  box-sizing: border-box;
`;

export const TweetCardInfo = styled.div.attrs(props => ({ className: 'tweet-card-info' }))`
  width: 100%;
  height: ${props => (props.large === true ? 2 * infoHeight : infoHeight)}px;
  font-size: ${0.8 * infoHeight}px;
  line-height: ${props => (props.large === true ? 2 * infoHeight : infoHeight)}px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const TweetCardProfile = styled.div.attrs(props => ({ className: 'tweet-card-profile' }))`
  width: ${props => (props.size ? props.size : infoHeight)}px;
  height: ${props => (props.size ? props.size : infoHeight)}px;
  margin: ${props => (props.margin ? props.margin : '0px')};
  background-image: url(${props => props.src});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  border: 1px solid ${props => props.theme.palette.tweet};
  border-radius: 100%;
  box-sizing: border-box;
`;

export const SocialDetailsCard = styled.div.attrs(props => ({ className: 'social-details-card' }))`
  width: 100%;
  min-height: 30%;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  padding: 0 16px;
  box-sizing: border-box;
  overflow: hidden;

  a,
  span.tagged {
    color: ${TWITTER_BLUE};
  }
`;

export const SocialDetailsCardContent = styled.div.attrs(props => ({
  className: 'social-details-card-text'
}))`
  width: calc(100% - 48px);
  height: 100%;
  padding: 8px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

export const SocialDetailsCardText = styled.div.attrs(props => ({
  className: 'social-details-card-text'
}))`
  padding: 8px 0px;
  line-height: ${lineHeight}px;
  font-weight: 200;
`;

export const SocialDetailsBottom = styled.div.attrs(props => ({
  className: 'social-details-bottom'
}))`
  width: 100%;
  /* max-height: 70%; */
  flex-grow: 1;
  background-color: ${props => props.theme.palette.canvasColor};
  display: flex;
  flex-direction: column;
  overflow: hidden;
  position: relative;
`;

const collapsedPanelHeight = '28px';

export const SocialVotingControlsPanel = styled.div.attrs(props => ({
  className: 'social-voting-controls-panel'
}))`
  position: absolute;
  bottom: 0px;
  width: 100%;
  /* height: 100%; */
  height: ${props =>
    props.expanded ? (props.canEdit === true ? '50%' : 'calc(50% - 56px)') : collapsedPanelHeight};
  min-height: ${props =>
    props.expanded ? (props.canEdit === true ? '50%' : 'calc(50% - 56px)') : collapsedPanelHeight};
  transition: height 0.3s ease-in-out;
  background-color: rgba(0, 0, 0, 0.8);
  overflow: hidden;

  &.expanded {
    backdrop-filter: blur(7px);
    .details-inner-heading {
      border-bottom: 1px solid rgba(255, 255, 255, 0.2);
    }

    .controls-inner {
      width: 100%;
      height: auto;
    }
  }

  .details-inner-heading {
    display: flex;
    flex-direction: row;
    width: 100;
    height: ${collapsedPanelHeight}px;
    align-items: center;
    justify-content: flex-start;
    box-sizing: border-box;
    border-bottom: 1px solid transparent;
    text-transform: uppercase;
  }

  .ccbox-select > div > div:nth-of-type(2) {
    /* patch the + icon position */
    margin: auto !important;
  }
`;

export const TweetCategories = styled.div.attrs(props => ({
  className: 'tweet-validation-categories'
}))`
  width: 100%;
  min-height: ${props => (props.canEdit ? 'calc(100% - 56px - 28px)' : 'calc(100% - 28px)')};
  height: ${props => (props.canEdit ? 'auto' : 'calc(100% - 28px)')};
  overflow: hidden;
  display: flex;
  flex-direction: column;
`;

export const TweetInfoLine = styled.div.attrs(props => ({
  className: 'tweet-validation-info-line'
}))`
  width: 100%;
  min-height: 32px;
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  box-sizing: border-box;
  padding: 8px;
  overflow: hidden;
  display: flex;
  flex-grow: ${props => props.weigth || 1};
`;

export const TweetInfoLineLabel = styled.div.attrs(props => ({
  className: 'tweet-validation-info-line-label'
}))`
  width: 25%;
  padding: 0 8px;
  box-sizing: border-box;
`;

export const TweetInfoValues = styled.div.attrs(props => ({
  className: 'tweet-validation-info-line-values'
}))`
  width: 75%;
  display: flex;
  flex-wrap: wrap-reverse;
  justify-content: flex-end;
`;

export const TweetValidationControls = styled.div.attrs(props => ({
  className: 'tweet-validation-controls'
}))`
  width: 100%;
  height: 56px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

export const SOCIAL_HAZARD_VALUES = [
  'drought',
  'earthquake',
  'extreme_weather_conditions',
  'floods',
  'landslide',
  'snow',
  'storms',
  'wildfires'
];

export const SOCIAL_SENTIMENT_VALUES = ['panic', 'none'];

export const SOCIAL_INFORMATION_TYPE_VALUES = [
  'affected_individuals',
  'caution_advice',
  'donations',
  'emotional_support',
  'infrastructures',
  'volunteering'
];

export const HAZARD_SOCIAL_ICONS = {
  floods: '\ue912',
  fire: '\ue911',
  extreme_weather_conditions: '\ue910',
  landslide: '\ue913',
  earthquake: '\ue90f',
  drought: '\ue90e',
  storms: '\ue915',
  snow: '\ue914',
  wildfires: '\ue911'
};

export const INFO_TYPE_ICONS = {
  affected_individuals: 'emoticon-dead',
  caution_advice: 'sign-caution',
  donations: 'square-inc-cash',
  emotional_support: 'heart-half-full',
  infrastructures: 'bridge',
  volunteering: 'hand'
};

export const TAGS_COLOR_MAP = {
  panic: TWEET_NEGATIVE_TAG_COLOR,
  no_panic: TWEET_POSITIVE_TAG_COLOR,
  informative: TWEET_POSITIVE_TAG_COLOR,
  uninformative: TWEET_NEGATIVE_TAG_COLOR
};

export const OPPOSITE_TAGS = {
  panic: 'no_panic',
  no_panic: 'panic',
  informative: 'uninformative',
  uninformative: 'informative'
};

// Formula used by MUI
const clientHeight =
  window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
export const EditTagsDialogBody = styled.div.attrs(props => ({
  className: 'edit-tags-dialog-body'
}))`
  width: 100%;
  height: ${clientHeight - 2 * 64 - 76 - 52}px;
  display: flex;
  flex-direction: column;
`;

export const EditTagsDialogLine = styled.div.attrs(props => ({
  className: 'edit-tags-dialog-line'
}))`
  width: 100%;
  height: 25%;
  display: flex;
  flex-direction: column;
`;
