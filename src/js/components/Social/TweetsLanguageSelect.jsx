import React, { Component } from 'react';
import { withAppSocial } from 'ioc-api-interface';
import { DropDownMenu, MenuItem, FontIcon } from 'material-ui';

class TweetsLanguageSelect extends Component {
  static defaultProps = {
    tweetsSupportedLanguages: []
  };

  _getMenuItems = () => {
    return this.props.tweetsSupportedLanguages.map(lang => (
      <MenuItem
        key={lang}
        value={lang}
        primaryText={lang.toUpperCase()}
        leftIcon={
          lang === 'any' ? (
            <FontIcon className="material-icons" color="#FFF">
              language
            </FontIcon>
          ) : (
            <FontIcon className={`flag-icon flag-icon-${lang}`} />
          )
        }
      />
    ));
  };

  _renderSelection = lang =>
    lang === 'any' ? (
      <FontIcon className="material-icons" style={{ lineHeight: '48px' }}>
        language
      </FontIcon>
    ) : (
      <FontIcon className={`flag-icon flag-icon-${lang}`} />
    );

  _onChange = (e, i, v) => this.props.setTweetFetchingLanguage(v);
  render() {
    return (
      <DropDownMenu
        className="tweets list-header__item"
        autoWidth={true}
        anchorOrigin={{
          horizontal: 'left',
          vertical: 'center'
        }}
        targetOrigin={{
          horizontal: 'left',
          vertical: 'center'
        }}
        // style={{ position: 'absolute', top: -4, right: 185, height: 40 }}
        iconStyle={{
          height: 48,
          position: 'relative',
          // top: -52,
          // right: -32,
          top: -12,
          right: 8,
          padding: 0,
          width: 24
        }}
        labelStyle={{
          height: 48,
          lineHeight: '48px',
          paddingLeft: 0,
          paddingRight: 8,
          display: 'inline-block'
        }}
        underlineStyle={{ margin: 'auto', borderTopColor: 'transparent' }}
        onChange={this._onChange}
        value={this.props.tweetsFetchingLanguage}
        selectionRenderer={this._renderSelection}
      >
        {this._getMenuItems()}
      </DropDownMenu>
    );
  }
}

export default withAppSocial(TweetsLanguageSelect);
