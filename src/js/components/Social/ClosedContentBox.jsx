import React, { Component } from 'react';
import { /*  FontIcon, IconButton, */ FlatButton, FontIcon } from 'material-ui';
import SelectField from 'js/components/Registration/SSField';
import styled from 'styled-components';
import nop from 'nop';
import CirclePlus from 'material-ui/svg-icons/content/add-circle-outline';
import { iReactTheme } from 'js/startup/iReactTheme';
import { lighten } from 'material-ui/utils/colorManipulator';

export const ClosedContentItem = styled.span.attrs(props => ({ className: 'closed-menu-item' }))`
  color: inherit;
  background-color: inherit;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  height: 36px;
  max-width: 100%;
  justify-content: space-between;
  /* max-width: ${256 + 48 - 24}px; */

  &.list {
    /* font-size: 0.75em; */

    span:nth-child(2) {
      max-width: calc(100% - 32px);
      line-height: 1em;
    }
  }

  &.selection span:nth-child(2) {
    white-space: nowrap;
    max-width: calc(100% - 32px);
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

class ClosedContentBox extends Component {
  static defaultProps = {
    name: 'ccbox',
    iconsMap: {},
    iconsClassName: '',
    onChange: nop,
    closedSetOfNames: [],
    title: 'Add Item',
    hint: 'Start typing...',
    values: [],
    dictionary: s => s
  };

  _selectRef = null;

  _setSelectRef = elem => (this._selectRef = elem ? elem.root : elem);

  _renderSelection = () => '';
  // _renderSelection = (selection, hintText) => (
  //   <span>{selection && selection.value ? selection.value : hintText}</span>
  // );

  _renderMenu = name => {
    const { dictionary, iconsClassName, iconsMap } = this.props;
    const label = dictionary(`_soc_cls_${name}`);
    return (
      <ClosedContentItem className="list" key={name} value={name} label={label}>
        {iconsMap[name] ? (
          iconsClassName === 'mdi' ? (
            <FontIcon className={`${iconsClassName} mdi-${iconsMap[name]}`} />
          ) : (
            <FontIcon className={iconsClassName}>{iconsMap[name]}</FontIcon>
          )
        ) : null}
        <span>{label}</span>
      </ClosedContentItem>
    );
  };

  _onChange = selectedValues => {
    console.log('selectedValues', selectedValues);
    this.props.onChange(selectedValues.map(sv => sv.value));
  };

  render() {
    const { closedSetOfNames, name, values, dictionary } = this.props;

    return (
      <SelectField
        style={{ display: 'flex' }}
        name={name}
        elementHeight={72}
        className="ccbox-select"
        ref={this._setSelectRef}
        value={values.map(n => ({ value: n, label: dictionary(`_soc_cls_${n}`) }))}
        multiple={true}
        selectionsRenderer={this._renderSelection}
        onChange={this._onChange}
        menuCloseButton={
          <FlatButton label={dictionary('_close')} secondary={true} style={{ width: '100%' }} />
        }
        dropDownIcon={<CirclePlus />}
        underlineStyle={{ display: 'none' }}
        canAutoPosition={false}
        popoverStyle={{
          position: 'fixed',
          boxSizing: 'border-box',
          border: `1px solid ${iReactTheme.palette.primary1Color}`,
          backgroundColor: lighten(iReactTheme.palette.canvasColor, 0.1)
        }}
        popoverClassName="geonames-superselect-popover"
        popoverWidth={window.innerWidth}
      >
        {closedSetOfNames.map(this._renderMenu)}
      </SelectField>
    );
  }
}

export default ClosedContentBox;

export { ClosedContentBox };
