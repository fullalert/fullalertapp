import React, { PureComponent } from 'react';
import { SeverityLevel, appInsights } from 'js/startup/errorHandlingAndTracking';
import nop from 'nop';
// import debounce from 'lodash.debounce';
import { areBoundsEqual } from 'js/utils/ireactFeatureComparison';
import { logMain, logError } from 'js/utils/log';

const { Map } = mapboxgl;

const STYLES = {
  mapView: {
    width: '100%',
    height: '100%',
    backgroundColor: '#808080'
  },
  loader: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1
  },
  loaderInner: {
    textAlign: 'center',
    fontSize: '1.5em',
    color: '#FFF',
    backgroundColor: 'rgba(0,0,0,0.75)',
    padding: 8,
    borderRadius: '24px',
    width: '60%',
    maxWidth: 400
  }
};

/**
 * returns most important settings for map status update/animation
 * @param {mapboxgl.Map} map
 */
export function getMapSettings(map) {
  if (map) {
    const bounds = map.getBounds();
    return {
      center: map.getCenter().toArray(),
      zoom: map.getZoom(),
      bearing: map.getBearing(),
      pitch: map.getPitch(),
      bounds: [bounds.getWest(), bounds.getSouth(), bounds.getEast(), bounds.getNorth()],
      maxBounds: map.getMaxBounds()
    };
  } else {
    return null;
  }
}

/**
 * Attach mouse event for changing pointer when select a layer features
 * @param {mapboxgl.Map} map
 * @param {String} layerName layer id
 * @param {Function|null} onMouseEnter event handler
 * @param {Function|null} onMouseLeave event handler
 */
export function attachCursorEvents(map, layerName, onMouseEnter = null, onMouseLeave = null) {
  if (!IS_CORDOVA) {
    // Change the cursor to a pointer when the it enters a feature in the 'symbols' layer.
    map.on('mouseenter', layerName, function(e) {
      map.getCanvas().style.cursor = 'pointer';

      if (typeof onMouseEnter === 'function') {
        onMouseEnter(e);
      }
    });

    // Change it back to a pointer when it leaves.
    map.on('mouseleave', layerName, function(e) {
      map.getCanvas().style.cursor = '';

      if (typeof onMouseLeave === 'function') {
        onMouseLeave(e);
      }
    });
  }
}

/** 
 * Props
 *  accessToken: Mapbox access token (optional)
 *  mapSettings: { style, center, center, zoom, pich, bearing, bounds, minZoom, maxZoom }
 *  function onMapLoad(event, doneCallback, {...props}) {
        // All operations that the map should do just after it's been loaded
        // The callback gets passed the mapbox load event, the doneCallback and other props passed to
        // the MapView component (e.g. geolocation)
        // You must call doneCallback once the initialization is finished
    }
*/
class MapView extends PureComponent {
  static defaultProps = {
    accessToken: null,
    mapSettings: {},
    mapLoadingMessage: 'Map Loading, please wait...',
    onMapLoad: (event, doneCallback = nop) => {
      doneCallback();
    },
    onTransformRequest: xhrCfg => xhrCfg,
    style: {
      width: '100%',
      height: '100%'
    }
  };

  state = {
    ready: false
  };

  constructor(props) {
    super(props);
    this.container = null;
    this.map = null;
  }

  componentDidMount() {
    if (this.props.accessToken) {
      mapboxgl.accessToken = this.props.accessToken;
    }
    this._createMap();
  }

  componentWillUnmount() {
    if (this.map !== null) {
      if (typeof this.props.onSettingsUpdate === 'function') {
        const mapSettings = getMapSettings(this.map);
        this.props.onSettingsUpdate(mapSettings);
      }
      //Removes also all listeners
      this.map.remove();
      this.map = null;
    }
  }

  onMapLoaded = () => {
    this.setState({ ready: true });
  };

  _transformRequest = (url, resourceType) => {
    const headers = {};
    if (url.includes(MAP_SERVER_URL)) {
      // logMain('%c MAP TR REQUEST  ', 'color: black; background: linear-gradient(to right, orange , yellow, green, cyan, blue, violet); font-weight: bold', url, resourceType);
      headers['apikey'] = MAP_SERVER_APIKEY;
    }

    return this.props.onTransformRequest({
      url,
      headers,
      credentials: 'same-origin'
    });
  };

  /**
   * Create a new map instance using initial props settings
   */
  _createMap = () => {
    if (this.map !== null) {
      return;
    }
    // See https://github.com/mapbox/mapbox-gl-js/issues/1970#issuecomment-356372603
    const { maxBounds, ...options } = this.props.mapSettings;
    const mapSettings = {
      container: this.container,
      ...options,
      transformRequest: this._transformRequest
    };
    this.map = new Map(mapSettings);
    // this.map.on('styledata', (event) => {
    //     logMain('ooo', this.state.ready, event);
    //     if (this.map.isStyleLoaded() === true && !this.state.ready) {
    //         logMain('ooo', event);
    //         this.setState({ ready: true });
    //     }
    // });
    this.map.on('load', event => {
      logMain('Map#load', event);
      if (Array.isArray(maxBounds)) {
        this.map
          .jumpTo(maxBounds) // avoid initial transitions which causes flickering
          .setMaxBounds(maxBounds);
      }
      // TODO should pass only non-function properties
      this.props.onMapLoad(
        event,
        this.onMapLoaded,
        this.props.onMapHover,
        this.props.onMapMouseLeave,
        { ...this.props }
      );
    });

    this.map.on('style.load', event => {
      if (typeof this.props.onStyleLoad === 'function') {
        this.props.onStyleLoad(event);
      }
    });

    if (typeof this.props.onMapMoveStart === 'function') {
      this.map.on('movestart', this.props.onMapMoveStart);
    }
    this.map.on('mousedown', this._onMapTouchStart);
    this.map.on('touchstart', this._onMapTouchStart);
    // if (typeof this.props.onMapTouchStart === 'function') {
    //   this.map.on('mousedown', this.props.onMapTouchStart);
    //   this.map.on('touchstart', this.props.onMapTouchStart);
    // }
    this.map.on('moveend', this._onMapMoveEnd);
    if (typeof this.props.onMapClick === 'function') {
      this.map.on('click', this.props.onMapClick);
    }

    if (typeof this.props.onMapContextMenu === 'function') {
      this.map.on('contextmenu', this.props.onMapContextMenu);
    }

    this.map.on('error', errorEvent => {
      // 400 error thrown by geoserver in WMTS mode with requests out of bound
      const isEmptyTile =
        errorEvent.source &&
        errorEvent.source.type === 'raster' &&
        errorEvent.error &&
        errorEvent.error.status === 400;
      if (isEmptyTile) {
        return;
      }
      if (errorEvent.sourceId === 'features' && errorEvent.error.message === 'Failed to fetch') {
        return;
      }
      appInsights.trackException(
        errorEvent,
        'Mapview.jsx error handler',
        {},
        {},
        SeverityLevel.Error
      );
      if (ENVIRONMENT !== 'production') {
        logError('Map Error', errorEvent);
        throw errorEvent.error;
      }
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.ready === false && this.state.ready === true) {
      logMain('Map ready');
      if (typeof this.props.onMapLoadEnd === 'function') {
        const mapSettings = getMapSettings(this.map);
        this.props.onMapLoadEnd(this.map, mapSettings);
      }
    }
    this._updateMap(this.props.mapSettings, prevProps.mapSettings);
    if (
      prevProps.style.height !== this.props.style.height ||
      prevProps.style.width !== this.props.style.width
    ) {
      if (this.map) {
        this.map.resize();
      }
    }
  }

  /**
   * This method will report the status update to the parent (and eventually, to redux)
   */
  // _onMapMoveEnd = () => {
  //   if (!this.map.isMoving()) {
  //     const mapSettings = getMapSettings(this.map);
  //     if (typeof this.props.onSettingsUpdate === 'function') {
  //       this.props.onSettingsUpdate(mapSettings);
  //     }
  //     if (typeof this.props.onMapMoveEnd === 'function') {
  //       this.props.onMapMoveEnd(this.map, mapSettings);
  //     }
  //   }
  // };

  _onMapMoveEnd = () => {
    if (!this.map) {
      return;
    }
    if (!this.map.isMoving()) {
      const mapSettings = getMapSettings(this.map);
      if (typeof this.props.onSettingsUpdate === 'function') {
        this.props.onSettingsUpdate(mapSettings);
      }
      if (typeof this.props.onMapMoveEnd === 'function') {
        this.props.onMapMoveEnd(this.map, mapSettings);
      }
    }
  };

  // _onMapMoveEnd = debounce(
  //   () => {
  //     if (!this.map) {
  //       return;
  //     }
  //     if (!this.map.isMoving()) {
  //       const mapSettings = getMapSettings(this.map);
  //       if (typeof this.props.onSettingsUpdate === 'function') {
  //         this.props.onSettingsUpdate(mapSettings);
  //       }
  //       if (typeof this.props.onMapMoveEnd === 'function') {
  //         this.props.onMapMoveEnd(this.map, mapSettings);
  //       }
  //     }
  //   },
  //   200,
  //   { leading: true }
  // );

  _onMapTouchStart = evt => {
    if (!this.map) {
      return;
    }
    if (this.map.isMoving()) {
      this.map.stop();
    }
    if (typeof this.props.onMapTouchStart === 'function') {
      this.map.on('mousedown', this.props.onMapTouchStart);
      this.map.on('touchstart', this.props.onMapTouchStart);
    }
  };

  /**
   * This method will update the map configuration when the change
   * is not triggered by direct user interaction
   */
  _updateMap = (nextMapSettings, prevSettings) => {
    let map = this.map;
    if (map) {
      if (nextMapSettings.style !== prevSettings.style) {
        map.setStyle(nextMapSettings.style);
      }
      if (map.isMoving()) {
        return;
        // map.stop();
      }
      const currentMapSettings = getMapSettings(map);
      const { /*  center, zoom, pitch, bearing, */ maxBounds } = currentMapSettings;
      const updateMaxBounds = !areBoundsEqual(maxBounds, nextMapSettings.maxBounds);
      // const updateMap =
      //   center[0] !== nextMapSettings.center[0] ||
      //   center[1] !== nextMapSettings.center[1] ||
      //   zoom !== nextMapSettings.zoom ||
      //   bearing !== nextMapSettings.bearing ||
      //   pitch !== nextMapSettings.pitch;
      // if (updateMap) {
      //   map.flyTo({
      //     center: nextMapSettings.center,
      //     zoom: nextMapSettings.zoom,
      //     bearing: nextMapSettings.bearing,
      //     pitch: nextMapSettings.pitch
      //   });
      if (updateMaxBounds) {
        if (Array.isArray(nextMapSettings.maxBounds)) {
          map.setMaxBounds(nextMapSettings.maxBounds);
        } else {
          map.setMaxBounds();
        }
      }
      // }
    }
  };

  /**
   * Get Map instance
   */
  getMap = () => this.map;

  setContainerRef = ref => (this.container = ref);

  render() {
    return (
      <div
        ref={this.setContainerRef}
        className="light-map-wiew"
        style={{ ...STYLES.mapView, ...this.props.style }}
      >
        {/*TODO pass loading component as a prop*/}
        {this.state.ready === false && (
          <div className="map-loader" style={STYLES.loader}>
            <div
              className="map-loader-inner"
              style={typeof this.props.mapLoadingMessage === 'string' ? STYLES.loaderInner : {}}
            >
              {this.props.mapLoadingMessage}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default MapView;
