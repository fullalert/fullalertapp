import React, { Component } from 'react';
import { Header, Content } from 'js/components/app';
import { withLogin } from 'ioc-api-interface';
import { getUserType } from 'js/utils/userPermissions';
import { withBluetooth } from 'js/modules/bluetooth/';
import { GeneralSettings } from './GeneralSettings';
import { withDictionary } from 'ioc-localization';
import { Link } from 'react-router-dom';
import { List, ListItem, Divider } from 'material-ui';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import BluetoothConnected from 'material-ui/svg-icons/device/bluetooth-connected';
import BluetoothSearching from 'material-ui/svg-icons/device/bluetooth-searching';
import BluetoothDisabled from 'material-ui/svg-icons/device/bluetooth-disabled';

import Language from 'material-ui/svg-icons/action/language';
import Layers from 'material-ui/svg-icons/maps/layers';
import AccessTime from 'material-ui/svg-icons/device/access-time';
import Security from 'material-ui/svg-icons/hardware/security';
import { BT_TEXT_COLOR } from 'js/components/Wearable';
import { /* IS_ANDROID, */ IS_IOS } from 'js/utils/getAppInfo';
import { Redirect } from 'react-router-dom';

const STYLES = {
  content: { display: 'flex', flexDirection: 'column' },
  list: {
    /*  height: '100%', */
    width: '100%',
    overflowX: 'hidden',
    overflowY: 'auto'
    // flexGrow: 1
  },
  disabled: { opacity: 0.5 }
};

const TOP_LEVEL_SETTINGS = [
  { name: 'language', label: '_language', route: 'language', leftIcon: <Language /> },
  { name: 'map', label: '_map_settings', route: 'map', leftIcon: <Layers /> },
  {
    name: 'timeWindow',
    label: '_app_settings',
    route: 'timewindow',
    leftIcon: <AccessTime />
  }
];

const ANDROID_PRO_ONLY_SETTINGS = [{ name: 'bluetooth', label: '_bt_settings', route: 'wearable' }];

const PRO_ONLY_SETTINGS = [
  { name: 'privacy', label: '_privacy_settings', route: null, leftIcon: <Security /> }
];

class Settings extends Component {
  render() {
    const { dictionary, user, isConnected, isConnecting, loggedIn } = this.props;
    if (!loggedIn) {
      return <Redirect to="/" />;
    }
    const userType = getUserType(user);
    const settingsList =
      userType === 'citizen'
        ? TOP_LEVEL_SETTINGS
        : IS_IOS
        ? [...TOP_LEVEL_SETTINGS, ...PRO_ONLY_SETTINGS]
        : [...TOP_LEVEL_SETTINGS, ...ANDROID_PRO_ONLY_SETTINGS, ...PRO_ONLY_SETTINGS];
    const btLeftIcon = isConnecting ? (
      <BluetoothSearching />
    ) : isConnected ? (
      <BluetoothConnected color={BT_TEXT_COLOR} />
    ) : (
      <BluetoothDisabled />
    );

    return (
      <div className="settings page">
        <Header title={'_settings'} leftButtonType="back" />
        <Content style={STYLES.content}>
          <List style={STYLES.list}>
            {settingsList.map(setting => {
              {
                const { name, route, label, leftIcon } = setting;
                const _leftIcon = name === 'bluetooth' ? btLeftIcon : leftIcon || <span />;
                const disabled = route === null;
                return (
                  <div key={name}>
                    <ListItem
                      leftIcon={_leftIcon}
                      style={
                        disabled
                          ? STYLES.disabled
                          : name === 'bluetooth' && isConnected
                          ? { color: BT_TEXT_COLOR }
                          : {}
                      }
                      disabled={disabled}
                      containerElement={route ? <Link to={`/settings/${route}`} /> : <div />}
                      primaryText={dictionary(label)}
                      rightIcon={<ChevronRight />}
                    />
                    <Divider inset={false} />
                  </div>
                );
              }
            })}
          </List>
          <GeneralSettings dictionary={dictionary} />
        </Content>
      </div>
    );
  }
}

export default withBluetooth(withLogin(withDictionary(Settings)));
