import React from 'react';
import { Toggle, FontIcon, ListItem, List } from 'material-ui';
import { withDevicePermissions } from 'js/modules/devicePermissions';
import { withCameraPreferences } from 'js/modules/camera';
import { withMessages } from 'js/modules/ui';
import { compose } from 'redux';

const enhance = compose(
  withMessages,
  withDevicePermissions,
  withCameraPreferences
);

function SaveToGalleryFn({
  dictionary,
  galleryIsGranted,
  saveToGallery,
  setSaveToGallery,
  checkDevicePermissionGranted,
  requestDevicePermission,
  pushError
}) {
  const saveToPhotoAlbum = async () => {
    try {
      const galleryIsGranted = await checkDevicePermissionGranted('gallery');
      if (!galleryIsGranted) {
        await requestDevicePermission(['camera', 'gallery']);
      } else {
        setSaveToGallery(!saveToGallery);
      }
    } catch (err) {
      pushError(err);
    }
  };

  return (
    <ListItem
      className="gallery setting"
      leftIcon={
        <FontIcon style={{ top: 8 }} className="material-icons">
          photo_camera
        </FontIcon>
      }
      primaryText={
        <Toggle
          label={dictionary._sett_save_pictures_to_device}
          onToggle={saveToPhotoAlbum}
          toggled={galleryIsGranted && saveToGallery}
        />
      }
    />
  );
}

const SaveToGallery = enhance(SaveToGalleryFn);

export function GeneralSettings({ dictionary }) {
  return (
    <List style={{ padding: 0 }}>
      <SaveToGallery dictionary={dictionary} />
    </List>
  );
}
