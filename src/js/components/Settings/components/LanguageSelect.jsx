import React, { Component } from 'react';
import { SelectField, MenuItem, FontIcon } from 'material-ui';
import { iReactTheme } from 'js/startup/iReactTheme';
import { loadDictionary } from 'js/utils/getAssets';
import { withDictionary } from 'ioc-localization';
import { API } from 'ioc-api-interface';

const STYLES = {
  button: {
    margin: 12
  },
  header: {
    color: iReactTheme.palette.textColor
  },
  menuItemMini: { width: 48 },
  flagIcon: { width: 24 },
  selectMini: { width: 64, margin: 12 },
  underlineMini: {
    color: 'transparent',
    borderColor: 'transparent'
  }
};

class LanguageSelectComponent extends Component {
  static defaultProps = {
    mini: false,
    id: 'language-select'
  };

  handleChange = async (event, index, lang) => {
    const params = await loadDictionary(lang);
    const { locale, translationLoader, loading } = params;
    if (loading !== true) {
      const api = API.getInstance();
      api.culture = lang;
      this.props.loadDictionary(locale, translationLoader);
    }
    // this.props.loadDictionary(lang, require(`bundle-loader?lazy!locale/lang_${lang}/translation.json`));
  };

  _getLanguageMenuItem = (lang, index) =>
    this.props.mini ? (
      <MenuItem className="menuItem" key={lang} value={lang} style={STYLES.flagIcon}>
        <FontIcon style={STYLES.flagIcon} className={`flag-icon flag-icon-${lang}`} />
      </MenuItem>
    ) : (
      <MenuItem
        key={lang}
        value={lang}
        primaryText={this.props.supportedLanguages[lang]}
        leftIcon={<FontIcon className={`flag-icon flag-icon-${lang}`} />}
      />
    );

  render() {
    const { id, dictionary, mini, locale, supportedLanguages, style } = this.props;
    const _selectStyle = mini === true ? STYLES.selectMini : STYLES.button;
    const selectStyle = { ..._selectStyle, ...style };
    return (
      <SelectField
        autoWidth={false}
        id={id}
        className="language-select"
        underlineStyle={this.props.mini ? STYLES.underlineMini : {}}
        menuItemStyle={this.props.mini ? STYLES.menuItemMini : {}}
        style={selectStyle}
        floatingLabelText={mini === true ? undefined : dictionary._language}
        value={locale}
        selectionRenderer={() => (
          <div style={mini === true ? STYLES.flagIcon : {}}>
            <FontIcon style={STYLES.flagIcon} className={`flag-icon flag-icon-${locale}`} />
            {mini === false && <span>{`  ${supportedLanguages[locale]}`}</span>}
          </div>
        )}
        onChange={this.handleChange}
      >
        {Object.keys(supportedLanguages).map(this._getLanguageMenuItem)}
      </SelectField>
    );
  }
}

export const LanguageSelect = withDictionary(LanguageSelectComponent);
export default LanguageSelect;
