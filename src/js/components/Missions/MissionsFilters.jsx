import React, { Component } from 'react';
import styled from 'styled-components';
import { IconButton, IconMenu, MenuItem } from 'material-ui';
import nop from 'nop';
import { withMissionsFilters, FILTERS, availableSorters } from 'js/modules/missionsFilters';
import { withDictionary } from 'ioc-localization';

const FiltersContainer = styled.div`
  width: 100%;
  height: 48px;
  color: ${props => props.theme.palette.textColor};
  background-color: ${props => props.theme.palette.backgroundColor};
`;

const filtersButtonStyle = { padding: '0 12px' };

class MissionsFilters extends Component {
  static defaultProps = {
    onRefreshButtonClick: nop,
    missionFilters: {
      missionFilters: []
    },
    missionsSorter: 'lastModified_asc'
  };

  _onFiltersButtonClick = (evt, values) => {
    console.log('MissionsFilters::_onFiltersButtonClick', values);
    this.props.setMissionsFilters('temporalStatus', values);
  };

  _onSortersButtonClick = (evt, value) => {
    console.log('MissionsFilters::_onSortersButtonClick', value);
    this.props.setMissionsSorter(value);
  };

  render() {
    const { onRefreshButtonClick, missionFilters, missionsSorter, dictionary } = this.props;
    const filters = missionFilters.temporalStatus; // single category here

    return (
      <FiltersContainer className="missions-filters">
        <IconButton
          className="refresh-button"
          iconClassName="material-icons"
          style={filtersButtonStyle}
          onClick={onRefreshButtonClick}
          disabled={false}
          tooltip={'Refresh'}
        >
          refresh
        </IconButton>
        <IconMenu
          iconButtonElement={
            <IconButton className="filters-button" iconClassName="material-icons" tooltip="Filter">
              filter_list
            </IconButton>
          }
          iconStyle={{ color: filters.length > 0 ? 'red' : 'white' }}
          multiple={true}
          onChange={this._onFiltersButtonClick}
          value={filters}
        >
          {FILTERS.temporalStatus.map((filterName, i) => (
            <MenuItem
              key={i}
              value={filterName}
              primaryText={dictionary(`_mission_filter_${filterName}`)}
            />
          ))}
        </IconMenu>
        <IconMenu
          iconButtonElement={
            <IconButton className="sorters-button" iconClassName="material-icons" tooltip="Sort">
              sort
            </IconButton>
          }
          onChange={this._onSortersButtonClick}
          value={missionsSorter}
        >
          {availableSorters.map((sorter, i) => (
            <MenuItem key={i} value={sorter} primaryText={dictionary(`_${sorter}`)} />
          ))}
        </IconMenu>
      </FiltersContainer>
    );
  }
}

export default withDictionary(withMissionsFilters(MissionsFilters));
