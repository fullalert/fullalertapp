import React, { Component } from 'react';
import { compose } from 'redux';
import { withMissionsFilters, SORTERS } from 'js/modules/missionsFilters';
import { withMissions } from 'ioc-api-interface';
import { withRouter } from 'react-router';
import { GeoJSONFeaturesProvider } from 'js/components/GeoJSONFeaturesProvider';
import { FeatureSelectionContext } from 'js/components/app/FeatureSelectionContext';
import MissionsInner from './MissionsInner';
import { MissionUpdater } from 'js/components/MissionCard';

const enhance = compose(withMissionsFilters, withMissions);

class Missions extends Component {
  render() {
    const {
      missionFeaturesURL,
      missionTaskFeaturesURL,
      missionFeaturesStats,
      updateMissions,
      accomplishTask,
      takeChargeOfTask,
      releaseTaskOfMission,
      missionFiltersDefinition,
      missionsSorter,
      match,
      history,
      location
    } = this.props;
    const routerProps = { match, history, location };
    const taskMgmtProps = {
      accomplishTask,
      takeChargeOfTask,
      releaseTaskOfMission
    };
    return (
      <GeoJSONFeaturesProvider
        sourceURL={missionFeaturesURL}
        filters={missionFiltersDefinition}
        sorter={SORTERS[missionsSorter]}
        itemType={'mission'}
      >
        {(missions, missionsUpdating, missionsUpdateError, missionsFiltering, missionsSorting) => (
          <GeoJSONFeaturesProvider sourceURL={missionTaskFeaturesURL} itemType={'missionTask'}>
            {(
              missionTasks,
              missionTasksUpdating,
              missionTasksUpdateError,
              missionTasksFiltering,
              missionTaskSorting
            ) => {
              const missionsProps = {
                missionFeaturesStats,
                missions,
                missionsUpdating,
                missionsUpdateError,
                missionsFiltering,
                missionsSorting
              };

              const tasksProps = {
                missionTasks,
                missionTasksUpdating,
                missionTasksUpdateError,
                missionTasksFiltering,
                missionTaskSorting
              };

              return [
                <FeatureSelectionContext.Consumer key="selection-consumer">
                  {FeatureSelectionState => (
                    <MissionsInner
                      updateMissions={updateMissions}
                      {...FeatureSelectionState}
                      {...missionsProps}
                      // missions={features}
                      // featuresUpdating={featuresUpdating}
                      {...tasksProps}
                      {...routerProps}
                      {...taskMgmtProps}
                    />
                  )}
                </FeatureSelectionContext.Consumer>,
                <MissionUpdater key="missions-updater" />
              ];
            }}
          </GeoJSONFeaturesProvider>
        )}
      </GeoJSONFeaturesProvider>
    );
  }
}

export default withRouter(enhance(Missions));
