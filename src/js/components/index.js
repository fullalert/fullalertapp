import * as app from './app/';
export { app };
export { Login, Logout /* , UserRedirectLock */ } from './auth';
export { Registration, TermsAndConditions, SMSValidation, EmailValidation } from './Registration/';
export { MapTabs } from './MapTabs/';
export { Home } from './Home/';
export { ReportDetails } from './ReportDetails/';
export { EmergencyCommunicationDetails } from './EmergencyCommunicationDetails';
export { ScrollableHeaderPage } from './ScrollableHeaderPage';

export {
  Achievements,
  AchievementsHeader,
  AchievementsRedirectLock,
  CongratsPage
} from './Achievements/';
export { Learner, ProgressPage, CardsPlayer } from './Learner/';
export { About } from './About/';
export { Social } from './Social/';
export { Dashboard } from './Dashboard/';
export { Help } from './Help/';
export { LayersLegend } from './LayersLegend/';

export { Settings, TimeWindowSetting, LanguageSettings, MapSettings } from './Settings/';
export { PasswordForgot } from './PasswordForgot/';
export { UserProfile, PasswordChange } from './UserProfile';
export {
  NewReportRedirectLock,
  NewReportCapture,
  NewReport,
  HazardSelect,
  PickPositionFromMap,
  ContentSelect,
  CategorySelect,
  ContentValue,
  WebcamCapture
} from './NewReport2/';
export { MissionCard } from './MissionCard';
export { Missions } from './Missions';
export { MissionDetails } from './MissionDetails';
export { MissionTaskDetails } from './MissionTaskDetails';
export { Wearable } from './Wearable';
