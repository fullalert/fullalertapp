import React from 'react';
import axios from 'axios';
import { logWarning, logMain } from 'js/utils/log';
import { featureFilter } from '@mapbox/mapbox-gl-style-spec';
const CancelToken = axios.CancelToken;
// const cancelTokenSource = CancelToken.source();

class GeoJSONFeaturesProvider extends React.Component {
  static defaultProps = {
    sourceURL: null,
    filters: null, // filter function, array of mapbox defs or object : {fn: function, def: Array} for combining
    sorter: null,
    style: { width: '100%', height: '100%' }
  };

  // See https://stackoverflow.com/questions/34544314/setstate-can-only-update-a-mounted-or-mounting-component-this-usually-mea/40969739#40969739
  _autoRef = null;

  _reqCancel = null;

  _updateCancelRequest = c => {
    // An executor function receives a cancel function as a parameter
    this._reqCancel = c;
  };

  _setAutoRef = e => {
    this._autoRef = e;
  };

  state = {
    features: [],
    updating: true,
    error: null
  };

  _requestFeatures = sourceURL => {
    if (this._autoRef) {
      this.setState({ error: null, updating: true }, async () => {
        try {
          let features = [];
          if (this.props.sourceURL !== null) {
            try {
              const response = await axios.get(sourceURL, {
                cancelToken: new CancelToken(this._updateCancelRequest)
              });
              features =
                response.data.type === 'FeatureCollection' ? response.data.features : response.data;
            } catch (err) {
              if (axios.isCancel(err)) {
                logMain('Request canceled:', err.message.text);
                if (err.message.nextURL) {
                  this._requestFeatures(err.message.nextURL);
                }
              } else {
                logWarning(`${this.props.itemType}: ${sourceURL} not valid`);
              }
            }
          }
          if (this._autoRef) {
            this.setState({ features, updating: false });
          }
        } catch (error) {
          if (this._autoRef) {
            this.setState({ error, updating: false });
          }
        }
      });
    }
  };

  _cancelRequest = nextURL => {
    if (this._reqCancel) {
      this.setState({ updating: false }, () => {
        this._reqCancel({ text: 'Operation canceled due to update', nextURL });
        logMain('Canceled', this.props.className);
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.sourceURL !== this.props.sourceURL) {
      if (this.state.updating) {
        this._cancelRequest(this.props.sourceURL);
      } else {
        if (this.props.sourceURL) {
          this._requestFeatures(this.props.sourceURL);
        }
      }
    }
  }

  componentDidMount() {
    if (this.props.sourceURL) {
      this._requestFeatures(this.props.sourceURL);
    } else {
      this.setState({ updating: false });
    }
  }

  _getFilterFn(filters) {
    if (!filters) {
      return null;
    }
    let filterFn =
      typeof filters === 'function'
        ? filters
        : typeof filters.fn === 'function'
        ? filters.fn
        : null;
    let mapboxFilterDefs = Array.isArray(filters)
      ? filters
      : Array.isArray(filters.def)
      ? filters.def
      : null;
    if (filterFn && mapboxFilterDefs) {
      // combine both
      const mapboxFilterFn = featureFilter(mapboxFilterDefs).bind(null, null);
      return feature => filterFn(feature) && mapboxFilterFn(feature);
    } else {
      return filterFn
        ? filterFn
        : mapboxFilterDefs
        ? featureFilter(mapboxFilterDefs).bind(null, null)
        : null;
    }
  }

  render() {
    const { sourceURL, children, filters, sorter, ...rest } = this.props;
    const { features } = this.state;
    let _features = [...features];
    const filterFn = this._getFilterFn(filters);
    if (typeof filterFn === 'function') {
      _features = _features.filter(filterFn);
    }
    if (typeof sorter === 'function') {
      _features = _features.sort(sorter); // TODO check if _.sort() is faster
    }
    return (
      <div ref={this._setAutoRef} {...rest}>
        {children(
          _features,
          this.state.updating,
          this.state.error,
          filterFn !== null,
          sorter !== null
        )}
      </div>
    );
  }
}

export default GeoJSONFeaturesProvider;
