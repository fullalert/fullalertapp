import React, { Component } from 'react';
import { FloatingActionButton, FontIcon, Paper } from 'material-ui';
import muiThemeable from 'material-ui/styles/muiThemeable';

const themed = muiThemeable();

const STYLES = {
  scrollButton: {
    position: 'absolute',
    display: 'block',
    zIndex: 99,
    bottom: 40,
    right: 16,
    backgroundColor: 'transparent'
  }
};

class ScrollToTopButton extends Component {
  render() {
    const height = this.props.offset > 100 ? 40 : 0;
    return (
      <Paper
        className="floating-controls"
        circle={true}
        style={{ ...STYLES.scrollButton, height: height }}
      >
        {height > 0 && (
          <FloatingActionButton
            mini={true}
            secondary={true}
            iconStyle={{ color: this.props.muiTheme.palette.textColor }}
            onClick={this.props.onClick}
          >
            <FontIcon className="material-icons">keyboard_arrow_up</FontIcon>
          </FloatingActionButton>
        )}
      </Paper>
    );
  }
}

export default themed(ScrollToTopButton);
