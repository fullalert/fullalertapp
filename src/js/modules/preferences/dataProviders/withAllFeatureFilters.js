import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { allFeatureFiltersSelector } from './withMapPreferences';

const select = createStructuredSelector({
  allFeatureFiltersDefinitions: allFeatureFiltersSelector
});

export default connect(select);
