import {
  RESTORE_DEFAULT,
  SET_DEFAULT_MAP_STYLE,
  SET_MAP_STYLE,
  SET_IREACT_FEATURES_ON_MAP,
  RESET_IREACT_FEATURES_ON_MAP,
  UPDATE_CONTENTS_RADIUS
} from './Actions';
import { DEFAULT_RADIUS } from './DefaultState';
import { MAP_STYLES_NAMES } from '../mapStyles';
import { setLiveAreaOfInterestFromUserPosition } from 'ioc-api-interface';
// TODO into some general provider for preferences
export function restoreDefaultPreferences() {
  return {
    type: RESTORE_DEFAULT
  };
}

export function restoreDefaultMapPreferences() {
  return {
    type: SET_DEFAULT_MAP_STYLE
  };
}

export function setMapStyleName(mapStyleName) {
  return {
    type: SET_MAP_STYLE,
    mapStyleName
  };
}

export function changeMapStyle(mapStyleName) {
  return (dispatch, getState) => {
    const currentStyleName = getState().preferences.mapStyleName;
    // if it's not the same AND style is supported
    if (currentStyleName !== mapStyleName && MAP_STYLES_NAMES.indexOf(mapStyleName) > -1) {
      dispatch(setMapStyleName(mapStyleName));
    }
  };
}

export function restoreIreactFeaturesOnMap() {
  return {
    type: RESET_IREACT_FEATURES_ON_MAP
  };
}

export function setIreactFeaturesOnMap(itemTypesOnMap) {
  return {
    type: SET_IREACT_FEATURES_ON_MAP,
    itemTypesOnMap
  };
}

function updateRadius(contentsRadius) {
  return {
    type: UPDATE_CONTENTS_RADIUS,
    contentsRadius
  };
}

const MIN_RADIUS = DEFAULT_RADIUS;

export function updateContentsRadius(radius) {
  return async (dispatch, getState) => {
    const state = getState();
    const { preferences, geolocation, api_authentication } = state;
    if (api_authentication.logged_in && geolocation.currentPosition) {
      const { coords, timestamp } = geolocation.currentPosition;

      if (timestamp > 0 && radius >= MIN_RADIUS && radius !== preferences.contentsRadius) {
        const { latitude, longitude } = coords;
        const position = [longitude, latitude];
        dispatch(updateRadius(radius));
        dispatch(setLiveAreaOfInterestFromUserPosition(position, radius));
      }
    }
  };
}
