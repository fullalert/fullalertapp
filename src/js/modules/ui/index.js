import uiReducer from './dataSources/Reducer';
// export { default as withAppForegroundState } from './dataProviders/withAppForegroundState';
export { default as withLoader } from './dataProviders/withLoader';
export { default as withMessages } from './dataProviders/withMessages';
export { default as withMessagesOnly } from './dataProviders/withMessagesOnly';
export { default as withDrawer } from './dataProviders/withDrawer';
export { uiReducer as Reducer };
export {
  loadingStart,
  loadingStop,
  openDrawer,
  closeDrawer,
  pushError,
  popError,
  pushModalMessage,
  popModalMessage,
  pushMessage,
  popMessage // setAppBackgroundState,
} from // setAppForegroundState
'./dataSources/ActionCreators';

export default uiReducer;
