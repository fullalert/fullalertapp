import { HAS_NAVIGATOR_ONLINE } from './DefaultState';
import { CONNECTION_TYPE_CHANGED, STATUS_CHANGED } from './Actions';



function updateStatus(offline) {
    return {
        type: STATUS_CHANGED,
        offline
    };
}

function updateConnectionType(connectionType = 'unknown') {
    return {
        type: CONNECTION_TYPE_CHANGED,
        connectionType
    };
}

function onOnline(callback) {
    console.debug('Device online');
    callback();
}

function onOffline(callback) {
    console.debug('Device offline');
    callback();
}

let _onlineHandler = null,
    _offlineHandler = null;

function watchNetworkEvents(watch = true) {
    return dispatch => {
        if (HAS_NAVIGATOR_ONLINE) {
            dispatch(updateStatus(navigator.onLine === false));
        }
        if (watch === true && _onlineHandler === null && _offlineHandler === null) {
            console.debug('Start watching network events');
            _onlineHandler = onOnline.bind(null, () => {
                dispatch(updateStatus(false));
                dispatch(updateConnectionType(navigator.connection.type));
            });
            _offlineHandler = onOffline.bind(null, () => {
                dispatch(updateStatus(true));
                dispatch(updateConnectionType(navigator.connection.type));
            });
            if (IS_CORDOVA) {
                document.addEventListener('online', _onlineHandler, false);
                document.addEventListener('offline', _offlineHandler, false);
            }
            else {
                window.addEventListener('online', _onlineHandler, false);
                window.addEventListener('offline', _offlineHandler, false);
            }
        } else if (watch === false && _onlineHandler !== null && _offlineHandler !== null) {
            console.debug('Stop watching network events');
            if (IS_CORDOVA) {
                document.removeEventListener('online', _onlineHandler, false);
                document.removeEventListener('offline', _offlineHandler, false);
            }
            else {
                window.removeEventListener('online', _onlineHandler, false);
                window.removeEventListener('offline', _offlineHandler, false);
            }
            _onlineHandler = null;
            _offlineHandler = null;
        }
    };
}

/**
 * Possible Value (Connection.<KEY>)
 * 

        UNKNOWN: "unknown",
        ETHERNET: "ethernet",
        WIFI: "wifi",
        CELL_2G: "2g",
        CELL_3G: "3g",
        CELL_4G: "4g",
        CELL:"cellular",
        NONE: "none"
 */
function checkConnection() {
    return dispatch => {
        dispatch(updateConnectionType(navigator.connection ? navigator.connection.type : 'unknown'));
    };
}

export { watchNetworkEvents, checkConnection };