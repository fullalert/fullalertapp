import { logError, logWarning } from 'js/utils/log';
import { IS_ANDROID, IS_IOS } from 'js/utils/getAppInfo';

export const GRANTED = 'GRANTED'; //IS_CORDOVA ? cordova.plugins.diagnostic.permissionStatus.GRANTED : 'GRANTED';

export const NOT_REQUESTED = 'NOT_REQUESTED'; // IS_CORDOVA  ? cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED  : 'NOT_REQUESTED';

export const DENIED = 'DENIED'; //IS_CORDOVA ? cordova.plugins.diagnostic.permissionStatus.DENIED : 'DENIED';

// export const DENIED_ALWAYS = IS_CORDOVA
//   ? cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS
//   : 'DENIED_ALWAYS';

/**
 * Normalize platform-dependent statuses
 * @param {String} status
 * @return {String} value: GRANTED|NOT_REQUESTED|DENIED
 */
function status2Value(status) {
  let value = status;
  if (IS_IOS) {
    switch (status) {
      case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
        value = NOT_REQUESTED;
        break;
      case cordova.plugins.diagnostic.permissionStatus.GRANTED:
      case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
        value = GRANTED;
        break;
      case cordova.plugins.diagnostic.permissionStatus.DENIED:
      case cordova.plugins.diagnostic.permissionStatus.RESTRICTED:
        value = DENIED;
        break;
      default:
        break;
    }
  } else if (IS_ANDROID) {
    switch (status) {
      case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
        value = NOT_REQUESTED;
        break;
      case cordova.plugins.diagnostic.permissionStatus.GRANTED:
        value = GRANTED;
        break;
      case cordova.plugins.diagnostic.permissionStatus.DENIED:
      case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
        value = DENIED;
        break;
      default:
        break;
    }
  }

  return value;
}

function promisify(permissionFunction) {
  return new Promise((success, failure) => {
    if (IS_CORDOVA) {
      permissionFunction(status => success(status2Value(status)), failure);
    } else {
      // mock-up behavior
      success(GRANTED);
    }
  });
}

export async function getLocationAuthorizationStatus() {
  try {
    // https://github.com/dpa99c/cordova-diagnostic-plugin#getlocationauthorizationstatus
    const status = promisify(
      IS_CORDOVA ? cordova.plugins.diagnostic.getLocationAuthorizationStatus : ''
    );
    return status;
  } catch (err) {
    logError('Error checking location permissions', err);
    throw err;
  }
}

export async function getCameraAuthorizationStatus() {
  try {
    // https://github.com/dpa99c/cordova-diagnostic-plugin#getcameraauthorizationstatus
    const status = promisify(
      IS_CORDOVA ? cordova.plugins.diagnostic.getCameraAuthorizationStatus : ''
    );
    return status;
  } catch (err) {
    logError('Error checking camera permissions', err);
    throw err;
  }
}

export async function getCameraRollAuthorizationStatus() {
  try {
    let status = IS_CORDOVA ? NOT_REQUESTED : GRANTED;
    if (IS_IOS) {
      // https://github.com/dpa99c/cordova-diagnostic-plugin#getcamerarollauthorizationstatus
      status = promisify(
        IS_CORDOVA ? cordova.plugins.diagnostic.getCameraRollAuthorizationStatus : ''
      );
    } else if (IS_ANDROID) {
      // https://github.com/dpa99c/cordova-diagnostic-plugin#getexternalstorageauthorizationstatus
      status = promisify(
        IS_CORDOVA ? cordova.plugins.diagnostic.getExternalStorageAuthorizationStatus : ''
      );
    }
    return status;
  } catch (err) {
    logError('Error checking photo gallery permissions', err);
    throw err;
  }
}

export async function getRemoteNotificationsAuthorizationStatus() {
  try {
    let status = IS_CORDOVA ? NOT_REQUESTED : GRANTED;
    if (IS_IOS) {
      // https://github.com/dpa99c/cordova-diagnostic-plugin#getremotenotificationsauthorizationstatus
      status = promisify(
        IS_CORDOVA ? cordova.plugins.diagnostic.getRemoteNotificationsAuthorizationStatus : ''
      );
    } else if (IS_ANDROID) {
      // https://github.com/dpa99c/cordova-diagnostic-plugin#isremotenotificationsenabled
      const result = promisify(
        IS_CORDOVA ? cordova.plugins.diagnostic.isRemoteNotificationsEnabled : ''
      );
      status = result === true ? GRANTED : DENIED;
    }
    return status;
  } catch (err) {
    logError('Error checking photo gallery permissions', err);
    throw err;
  }
}

// OPEN APP SETTINGS
export async function switchToSettings() {
  try {
    if (IS_CORDOVA) {
      await promisify(cordova.plugins.diagnostic.switchToSettings);
    }
    logWarning('This has no effect on web ui');
  } catch (err) {
    logError('Error checking photo gallery permissions', err);
    throw err;
  }
}

function name2Permission(permName) {
  let permission = null;
  switch (permName) {
    case 'camera':
      permission = cordova.plugins.diagnostic.permission.CAMERA;
      break;
    case 'gallery':
      permission = cordova.plugins.diagnostic.permission.WRITE_EXTERNAL_STORAGE;
      break;
    case 'location':
      permission = cordova.plugins.diagnostic.permission.ACCESS_FINE_LOCATION;
      break;
    case 'notifications':
      permission = cordova.plugins.diagnostic.remoteNotificationType.ALERT; // ACHTUNG has multiple args on iOS
      break;
    default:
      break;
  }
  return permission;
}

function name2statusFn(permName) {
  let permission = null;
  switch (permName) {
    case 'camera':
      // https://github.com/dpa99c/cordova-diagnostic-plugin#getcameraauthorizationstatus
      permission = cordova.plugins.diagnostic.getCameraAuthorizationStatus;
      break;
    case 'gallery':
      if (IS_IOS) {
        permission = cordova.plugins.diagnostic.getCameraRollAuthorizationStatus;
      } else {
        permission = cordova.plugins.diagnostic.getExternalStorageAuthorizationStatus;
      }
      break;
    case 'location':
      // https://github.com/dpa99c/cordova-diagnostic-plugin#getlocationauthorizationstatus
      permission = cordova.plugins.diagnostic.getLocationAuthorizationStatus;
      break;
    case 'notifications':
      if (IS_IOS) {
        permission = cordova.plugins.diagnostic.getRemoteNotificationsAuthorizationStatus;
      } else {
        // https://github.com/dpa99c/cordova-diagnostic-plugin#isremotenotificationsenabled
        permission = (success, failure) => {
          cordova.plugins.diagnostic.isRemoteNotificationsEnabled(enabled => {
            success(enabled ? GRANTED : DENIED); // ACHTUNG it's boolean
          }, failure);
        };
      }
      break;
    default:
      break;
  }
  return permission;
}

// IOS ONLY
function name2requestFn(permName) {
  let permission = null;
  switch (permName) {
    case 'camera':
      permission = cordova.plugins.diagnostic.requestCameraAuthorization;
      break;
    case 'gallery':
      permission = cordova.plugins.diagnostic.requestCameraRollAuthorization;
      break;
    case 'location':
      permission = cordova.plugins.diagnostic.requestLocationAuthorization;
      break;
    case 'notifications':
      permission = (successCallback, errorCallback) => {
        cordova.plugins.diagnostic.requestRemoteNotificationsAuthorization({
          successCallback,
          errorCallback,
          types: [
            cordova.plugins.diagnostic.remoteNotificationType.ALERT,
            cordova.plugins.diagnostic.remoteNotificationType.SOUND,
            cordova.plugins.diagnostic.remoteNotificationType.BADGE
          ],
          omitRegistration: false
        });
      };
      break;
    default:
      break;
  }
  return permission;
}

function notNull(e) {
  return e !== null;
}

/**
 * Request a permission and update its status in Redux
 * @param {String|Array<String>} permissionName
 * @return Promise<String|Array<String>>
 */
export function requestPermission(permissionName) {
  const isArray = Array.isArray(permissionName);
  const _perms = isArray ? permissionName : [permissionName];

  if (IS_IOS) {
    if (isArray) {
      const promises = _perms
        .map(name2requestFn)
        .filter(notNull)
        .map(fn => promisify(fn));
      return Promise.all(promises);
    } else {
      const fn = name2requestFn(permissionName);
      if (!fn) {
        throw new Error(`Wrong or unsupported permission ${permissionName}`);
      }
      return promisify(fn);
    }
  } else if (IS_ANDROID) {
    let _permNames = _perms.map(name2Permission).filter(notNull);
    if (_permNames.length > 0) {
      return new Promise((success, failure) => {
        // Android only: https://github.com/dpa99c/cordova-diagnostic-plugin#requestruntimepermissions
        cordova.plugins.diagnostic.requestRuntimePermissions(
          statuses => {
            const results = _perms.map(pName => {
              return statuses[name2Permission(pName)];
            });
            success(isArray ? results : results[0]);
          },
          failure,
          _permNames
        );
      });
    } else {
      return Promise.reject(`Invalid permission names: ${_perms.join(', ')}`);
    }
  } else {
    // mockup
    if (isArray) {
      return Promise.resolve(new Array(permissionName.length).fill(GRANTED));
    } else {
      return Promise.resolve(GRANTED);
    }
  }
}

/**
 * Request a permission and update its status in Redux
 * @param {String|Array<String>} permissionName
 * @param {Object} prevPermissions - used only in mockup
 * @return Promise<String|Array<String>>
 */
export function getPermissionStatus(
  permissionName,
  prevPermissions = {
    location: NOT_REQUESTED,
    camera: NOT_REQUESTED,
    gallery: NOT_REQUESTED,
    notifications: NOT_REQUESTED
  }
) {
  const isArray = Array.isArray(permissionName);
  const perms = isArray ? permissionName : [permissionName];
  if (IS_IOS) {
    if (isArray) {
      const promises = perms
        .map(name2statusFn)
        .filter(notNull)
        .map(fn => promisify(fn));
      return Promise.all(promises);
    } else {
      const fn = name2statusFn(permissionName);
      if (!fn) {
        throw new Error(`Wrong or unsupported permission ${permissionName}`);
      }
      return promisify(fn);
    }
  } else if (IS_ANDROID) {
    if (isArray) {
      const promises = perms
        .map(name2statusFn)
        .filter(notNull)
        .map(fn => promisify(fn));
      return Promise.all(promises);
    } else {
      const fn = name2statusFn(permissionName);
      if (!fn) {
        throw new Error(`Wrong or unsupported permission ${permissionName}`);
      }
      return promisify(fn);
    }
  } else {
    // mockup
    if (isArray) {
      return Promise.resolve(perms.map(pn => prevPermissions[pn]));
    } else {
      return Promise.resolve(prevPermissions[permissionName]);
    }
  }
}
