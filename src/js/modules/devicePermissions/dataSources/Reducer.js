import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
  CHECK_CAMERA,
  CHECK_GALLERY,
  CHECK_LOCATION,
  CHECK_NOTIFICATIONS,
  RESET_ALL
} from './Actions';

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [CHECK_LOCATION]: {
    location: action => action.location
  },
  [CHECK_CAMERA]: {
    camera: action => action.camera
  },
  [CHECK_GALLERY]: {
    gallery: action => action.gallery
  },
  [CHECK_NOTIFICATIONS]: {
    notifications: action => action.notifications
  }
};

export default reduceWith(mutators, DefaultState);
