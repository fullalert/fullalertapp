import {
  // CHECK_ALL,
  CHECK_CAMERA,
  CHECK_GALLERY,
  CHECK_LOCATION,
  CHECK_NOTIFICATIONS
} from './Actions';
import * as helpers from './helpers';
const { requestPermission, getPermissionStatus, GRANTED } = helpers;

if (!IS_PRODUCTION) {
  window.permissionHelpers = { ...helpers };
}

function _checkCamera(camera) {
  return {
    type: CHECK_CAMERA,
    camera
  };
}

function _checkGallery(gallery) {
  return {
    type: CHECK_GALLERY,
    gallery
  };
}

function _checkLocation(location) {
  return {
    type: CHECK_LOCATION,
    location
  };
}

function _checkNotifications(notifications) {
  return {
    type: CHECK_NOTIFICATIONS,
    notifications
  };
}

function _permToAction(permissionName) {
  let action = null;
  switch (permissionName) {
    case 'location':
      action = _checkLocation;
      break;
    case 'camera':
      action = _checkCamera;
      break;
    case 'gallery':
      action = _checkGallery;
      break;
    case 'notifications':
      action = _checkNotifications;
      break;
    default:
      break;
  }
  return action;
}

/**
 * Request a permission and update its status in Redux
 * @param {String} operationType "request" or "check"
 * @param {String|Array<String>} permissionName "camera", "location", "gallery" or "notification"
 * @param {Function} dispatch redux dispatch
 * @param {Function} state redux state
 * @return {Promise<String|Array<String>>}
 */
const _helperAction = async (operationType, permissionName, dispatch, state) => {
  const operation = operationType === 'request' ? requestPermission : getPermissionStatus;
  const values = await operation(permissionName, state);
  if (Array.isArray(permissionName)) {
    values.forEach((value, index) => {
      let action = _permToAction(permissionName[index]);
      if (action !== null && value !== null && value !== state[permissionName]) {
        dispatch(action(value));
      }
    });
  } else {
    // single string mode
    let action = _permToAction(permissionName);
    if (action !== null && values !== null && values !== state[permissionName]) {
      dispatch(action(values));
    }
  }

  return values;
};

/**
 * Return string status for each permission
 * if input is a string, output is single value, if array, array of values
 * @param {String|Array<String>} permissionName
 * @return Promise<String|Array<String>>
 */
export function requestDevicePermission(permissionName) {
  return async (dispatch, getState) => {
    const state = getState().devicePermissions;
    const value = await _helperAction('request', permissionName, dispatch, state);
    return value;
  };
}

/**
 * Check permission status and update it in Redux
 * Return boolean granted: true/false for each permission (true if granted)
 * if input is a string, output is single value, if array, array of values
 * @param {String|Array<String>} permissionName
 * @return Promise<Boolean|Array<Boolean>>
 */
export function checkDevicePermissionGranted(permissionName) {
  return async (dispatch, getState) => {
    const state = getState().devicePermissions;
    const value = await _helperAction('check', permissionName, dispatch, state);
    return Array.isArray(value) ? value.map(v => v === GRANTED) : value === GRANTED;
  };
}
