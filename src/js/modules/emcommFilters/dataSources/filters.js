/* EMCOM TYPE FILTERS */
const _is_report_request = ['==', 'itemType', 'reportRequest'];
const _is_emergency_communication = ['==', 'itemType', 'emergencyCommunication'];
const _is_alert = ['all', _is_emergency_communication, ['==', 'type', 'alert']];
const _is_warning = ['all', _is_emergency_communication, ['==', 'type', 'warning']];

export const emcommType = {
  _is_alert,
  _is_warning,
  _is_report_request
};

export const FILTERS = {
  emcommType: Object.keys(emcommType)
};

export const SORTERS = {
  _last_updated_desc: (f1, f2) =>
    new Date(f2.properties.lastModified).getTime() - new Date(f1.properties.lastModified).getTime(),
  _last_updated_asc: (f1, f2) =>
    new Date(f1.properties.lastModified).getTime() - new Date(f2.properties.lastModified).getTime(),
  _date_time_asc: (f1, f2) =>
    new Date(f1.properties.start).getTime() - new Date(f2.properties.start).getTime(),
  _date_time_desc: (f1, f2) =>
    new Date(f2.properties.start).getTime() - new Date(f1.properties.start).getTime()
};

export const availableFilters = Object.keys(FILTERS);
export const availableSorters = Object.keys(SORTERS);
