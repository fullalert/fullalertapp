const state = {
  isBtEnabled: false,
  isConnecting: false,
  isConnected: false,
  isScanning: false,
  isNotifying: false,
  selectedDevice: null,
  // rssiValue: NaN,
  availableDevices: [],
  messages: []
  // selectedServices: [],
  // selectedCharacteristics: []
};

export default state;
