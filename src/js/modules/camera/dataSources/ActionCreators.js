import {
  TAKE_PICTURE,
  CANCEL_PICTURE,
  SET_STATUS_CAPTURING,
  CAPTURE_ERROR,
  SET_SAVE_TO_GALLERY
} from './Actions';
import { API } from 'ioc-api-interface';
import { prepareImageForUpload } from '../prepareImage';
import { getAsset } from 'js/utils/getAssets';
const IMAGE_SIZE = [1024, 768];

const DEFAULT_CAMERA_SETTINGS = {
  quality: 100,
  destinationType: IS_CORDOVA && cordova.platformId === 'browser' ? 0 : 1, //0, //DATA_URL 1 //FILE_URI,
  allowEdit: false,
  encodingType: 0, //JPEG
  sourceType: 1, //Camera
  targetWidth: IMAGE_SIZE[0],
  targetHeight: IMAGE_SIZE[1],
  correctOrientation: true,
  saveToPhotoAlbum: true,
  cameraDirection: 0 //BACK camera
};

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const HAZARDS = [/*'earthquake',*/ 'fire', 'flood', 'extremeWeather', 'landslide'];
const ORIENTATIONS = ['landscape', 'portrait'];

const FILE_URL_RX = /$file:\/\/\//;

//Read a binary file using cordova plugin
function _readBinaryFile(fileEntry, type = 'image/jpeg', success, failure) {
  fileEntry.file(function(file) {
    const reader = new FileReader();
    reader.onloadend = function() {
      const blob = new Blob([new Uint8Array(this.result)], { type });
      success(blob);
    };
    reader.readAsArrayBuffer(file);
  }, failure);
}

// wraps a promise for reading a binary file using cordova plugin
function _getPictureFromFile(fileUrl, type = 'image/jpeg') {
  return new Promise((succ, fail) => {
    window.resolveLocalFileSystemURL(
      fileUrl.replace(FILE_URL_RX, 'cdvfile://'),
      function(fileEntry) {
        _readBinaryFile(fileEntry, type, succ, fail);
      },
      fail
    );
  });
}

async function simulateCameraImage(asBlob = false) {
  const hazard = HAZARDS[getRandomInt(0, HAZARDS.length - 1)];
  const orientation = ORIENTATIONS[getRandomInt(0, ORIENTATIONS.length - 1)];
  const randomURL = getAsset(`/fakeImages/${hazard}-${orientation}.jpg`);
  try {
    const imageBlobOrFormData = await prepareImageForUpload(randomURL, asBlob);
    return Promise.resolve(imageBlobOrFormData);
  } catch (err) {
    return Promise.reject(err);
  }
}

async function getSimulatedImageURL(getState, asBlob = false) {
  //    const user = getState().api.user;
  const api = API.getInstance();
  if (!api.token) {
    return Promise.reject(new Error('You must be logged in in order to upload pictures'));
  } else {
    try {
      const imageBlobOrFormData = await simulateCameraImage(asBlob);
      if (asBlob) {
        return Promise.resolve(imageBlobOrFormData);
      } else {
        //OLD way with 2 calls
        const response = await api.report.uploadPicture(api.token, imageBlobOrFormData);
        return Promise.resolve(response.result.uri);
      }
    } catch (ex) {
      return Promise.reject(ex);
    }
  }
}

async function uploadCameraImage(getState, asBlob = false) {
  const api = API.getInstance();
  if (!api.token) {
    return Promise.reject(new Error('You must be logged in in order to upload pictures'));
  } else {
    try {
      const state = getState();
      const saveToPhotoAlbum =
        state.devicePermissions.gallery === 'GRANTED' && state.camera.saveToGallery === true;
      const cameraImageUrl = await getCameraPicture(saveToPhotoAlbum);
      const imageBlobOrFormData = await prepareImageForUpload(cameraImageUrl, asBlob);
      if (asBlob) {
        return Promise.resolve(imageBlobOrFormData);
      } else {
        const response = await api.report.uploadPicture(api.token, imageBlobOrFormData);
        return Promise.resolve(response.result.uri);
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}

async function getCameraPicture(saveToPhotoAlbum = true) {
  return new Promise((success, fail) => {
    const done = imageUri => {
      (async imageUri => {
        const clean = result => {
          //Remove temp files
          navigator.camera.cleanup();
          success(result);
        };
        switch (cordova.platformId) {
          case 'ios':
            const imageStringResponse = await _getPictureFromFile(imageUri);
            clean(imageStringResponse);
            break;
          case 'browser':
            clean(`data:image/jpeg;base64,${imageUri}`);
            break;
          case 'android':
          default:
            clean(imageUri);
            break;
        }
      })(imageUri);
    };

    navigator.camera.getPicture(
      done,
      err => {
        console.log('%c Capture KO', 'background:red, color: white', JSON.stringify(err), err);
        fail(typeof err === 'string' ? new Error(err) : err);
      },
      { ...DEFAULT_CAMERA_SETTINGS, saveToPhotoAlbum }
    );
  });
}

function takePictureAction(imageURL) {
  return {
    type: TAKE_PICTURE,
    imageURL
  };
}

export function clearCamera() {
  return {
    type: CANCEL_PICTURE
  };
}

function isCapturing() {
  return {
    type: SET_STATUS_CAPTURING
  };
}

function captureError(error) {
  return {
    type: CAPTURE_ERROR,
    error
  };
}

export function takePicture(asBlob = false) {
  return async (dispatch, getState) => {
    const cameraState = getState().camera;
    if (!cameraState.isCapturing) {
      dispatch(isCapturing());
      let imageURL = null;
      try {
        if (IS_CORDOVA && navigator.camera) {
          imageURL = await uploadCameraImage(getState, asBlob);
        } else {
          imageURL = await getSimulatedImageURL(getState, asBlob); // fakeImageURL();
          console.log('%c IMG URL', 'color: magenta', imageURL);
        }
        dispatch(takePictureAction(imageURL));
        return Promise.resolve(imageURL);
      } catch (error) {
        dispatch(captureError(error));
        return Promise.reject(error);
      }
    }
  };
}

export function setSaveToGallery(saveToGallery = true) {
  return { type: SET_SAVE_TO_GALLERY, saveToGallery };
}
