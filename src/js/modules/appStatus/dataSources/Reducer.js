import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import { READY, RESUME, PAUSE } from './Actions';

const mutators = {
    [READY]: {
        status: 'ready'
    },
    [RESUME]: {
        status: 'resumed'
    },
    [PAUSE]: {
        status: 'paused'
    },
};

export default reduceWith(mutators, DefaultState);