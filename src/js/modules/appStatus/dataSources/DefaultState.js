const DefaultStatus = {
    status: 'not_ready' // other values = 'ready', 'resumed', 'paused'
};

export default DefaultStatus;