const isOpen = ['==', 'temporalStatus', 'open'];
const isOngoing = ['==', 'temporalStatus', 'ongoing'];
const isCompleted = ['==', 'temporalStatus', 'completed'];
const isExpired = ['==', 'temporalStatus', 'expired'];


export const temporalStatus = {
    isOpen, isOngoing, isCompleted, isExpired
};

export const FILTERS = {
    temporalStatus: Object.keys(temporalStatus)
};


function sorterByDate(dateAttr = 'lastModified', criterion = 'asc') {
    return (item1, item2) => {
        const d1 = new Date(item1.properties[dateAttr]).getTime();
        const d2 = new Date(item2.properties[dateAttr]).getTime();
        return criterion === 'asc' ? d1 - d2 : d2 - d1;
    };
}


export const SORTERS = {
    lastModified_asc: sorterByDate(),
    lastModified_desc: sorterByDate('lastModified', 'desc'),
    start_asc: sorterByDate('start'),
    start_desc: sorterByDate('start', 'desc'),
    end_asc: sorterByDate('end'),
    end_desc: sorterByDate('end', 'desc')
};

export const availableFilters = Object.keys(FILTERS);
export const availableSorters = Object.keys(SORTERS);