import { RESET_ALL as EMCOMM_FILTERS_RESET } from './emcommFilters/dataSources/Actions';
import { RESET_ALL as LAYERS_AND_SETTINGS_RESET } from './layersAndSettings/dataSources/Actions';
import { RESET_ALL as MAP_LAYERS_RESET } from './mapLayerFilters/dataSources/Actions';
import { RESET_ALL as MISSION_FILTERS_RESET } from './missionsFilters/dataSources/Actions';
import { RESET_ALL as NEWREPORT_RESET } from './newReport2/dataSources/Actions';
import { RESTORE_DEFAULT as PREFERENCES_RESET } from './preferences/dataSources/Actions';
import { RESET_ALL as REPORT_FILTERS_RESET } from './reportFilters/dataSources/Actions';
import { RESTORE_DEFAULT as UI_RESET } from './ui/dataSources/Actions';
import { RESET_ALL as BT_RESET } from './bluetooth/dataSources/Actions';
import { RESET_ALL as PERMISSIONS_RESET } from './devicePermissions/dataSources/Actions';
import { CLEAR_ALL as GAMIFICATION_AWARD_RESET } from './gamificationAwards/dataSources/Actions';
import { RESET_MAP as MAP_RESET } from './map/dataSources/Actions';

export const RESET_ACTIONS = [
  MAP_RESET,
  EMCOMM_FILTERS_RESET,
  LAYERS_AND_SETTINGS_RESET,
  MAP_LAYERS_RESET,
  MISSION_FILTERS_RESET,
  NEWREPORT_RESET,
  PREFERENCES_RESET,
  REPORT_FILTERS_RESET,
  UI_RESET,
  BT_RESET,
  PERMISSIONS_RESET,
  GAMIFICATION_AWARD_RESET
];
