import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import { UPDATE_MAP, RESET_MAP } from './Actions';

const mutators = {
  [UPDATE_MAP]: {
    minZoom: action => action.minZoom,
    maxZoom: action => action.maxZoom,
    center: action => action.center,
    zoom: action => action.zoom,
    bearing: action => action.bearing,
    pitch: action => action.pitch,
    // Map derived PARAMS
    bounds: action => action.bounds,
    interactive: action => action.interactive,
    maxBounds: action => action.maxBounds,
    rotation: action => action.rotation
  },
  [RESET_MAP]: {
    ...DefaultState
  }
};

export default reduceWith(mutators, DefaultState);
