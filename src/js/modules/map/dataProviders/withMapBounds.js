import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { setMaxBounds } from '../dataSources/ActionCreators';

const select = createStructuredSelector({
    bounds: state => state.map.bounds,
    maxBounds: state => state.map.maxBounds
});


export default connect(select, {
    setMaxBounds
});