import Reducer from './dataSources/Reducer';
import withMapLayerFilters from './dataProviders/withMapLayerFilters';
import * as mapLayerFiltersActions from './dataSources/ActionCreators';
export {
    availableFilters,
    FILTERS,
    layersVisibility as LAYERS_VISIBILITY_FILTER
} from './dataSources/filters';

export default Reducer;
export { Reducer, withMapLayerFilters, mapLayerFiltersActions };