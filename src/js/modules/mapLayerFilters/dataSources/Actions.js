const NS = 'MAPLAYERS_FILTERS';

export const ADD_FILTER = `${NS}@ADD_FILTER`;
export const REMOVE_FILTER = `${NS}@REMOVE_FILTER`;
export const RESET_ALL = `${NS}@RESET_ALL`;