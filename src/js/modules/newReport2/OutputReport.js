import { flattenObject } from 'js/utils/flattenObject';
import { b64ImageToBlob /* , blobToB64 */ } from 'js/utils/blobSerialization';
import { createGUID } from 'js/utils/createGUID';

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function isValidPosition(pos) {
  if (!pos) {
    return false;
  }
  const { latitude, longitude } = pos;
  return isNumeric(latitude) && isNumeric(longitude);
}

// let reportPicturePreviewURL = null;
// function destroyPreviewURL() {
//     if (reportPicturePreviewURL) {
//         URL.revokeObjectURL(reportPicturePreviewURL);
//     }
// }

/**
 * Tries to convert the candidate position to {latitude, longitude}
 * @param {any} candidatePosition
 * @return null if not valid, {latitude, longitude}
 */
function getPositionValue(candidatePosition) {
  if (!candidatePosition) {
    return null;
  } else {
    let pos = null;
    if (Array.isArray(candidatePosition) && candidatePosition.length === 2) {
      // [lng, lat]
      pos = { longitude: candidatePosition[0], latitude: candidatePosition[1] };
    } else if (typeof candidatePosition === 'string') {
      // unwkt
      const latitude = parseFloat(
        candidatePosition.replace('POINT (', '').replace(/\s(\d|\.)+\)$/, ''),
        10
      );
      const longitude = parseFloat(
        candidatePosition.replace(/POINT \((\d|\.)+\s/, '').replace(/\)$/, ''),
        10
      );
      pos = { latitude, longitude };
    } else if (candidatePosition instanceof mapboxgl.LngLat) {
      pos = {
        latitude: candidatePosition.lat,
        longitude: candidatePosition.lng
      };
    } else {
      const { latitude, longitude } = candidatePosition;
      pos = { latitude, longitude };
    }
    pos = isValidPosition(pos) ? pos : null;
    return pos;
  }
}

/**
 * Convert position ({longitude, latitude}) to WKT string (POINT entity)
 * @param {Object} pos
 */
function wktPosition(pos) {
  if (!pos) {
    return null;
  } else {
    return WKT_BACKEND ? `POINT (${pos.longitude} ${pos.latitude})` : pos;
  }
}

/**
 * Determine output measure type
 * @param {Object} measure
 * @return String ('unknown'|'qualitative', 'quantitative')
 */
function determineOutputMeasureType(measure) {
  let type = 'unknown';
  if (measure.hasOwnProperty('optionId')) {
    type = 'qualitative';
  } else if (measure.hasOwnProperty('value')) {
    type = 'quantitative';
  }
  return type;
}

/**
 * Class that validates, serializes and deserialize
 * an outgoing report
 */
class OutputReport {
  constructor(usercreationtime = Date.now(), usercreationguid) {
    let _timestamp =
      usercreationtime instanceof Date
        ? isNaN(usercreationtime.getTime())
          ? new Date()
          : usercreationtime
        : new Date(usercreationtime);
    if (isNaN(_timestamp.getTime())) {
      _timestamp = new Date();
    }
    const _guid =
      typeof usercreationguid === 'string' && usercreationguid.length === 36
        ? usercreationguid
        : createGUID();

    // Private variables
    let _isTraining = false;
    let _position = null;
    let _userPosition = null;
    let _hazard = null;
    let _category = null;
    let _pictureFile = null;
    let _sentByUser = false;
    let _visibility = undefined;
    // let _picturePreview = '';
    let _measures = {
      qualitative: [],
      quantitative: []
    };

    let _damages = [];
    let _resources = [];
    let _people = [];

    const _setMeasures = function(measures) {
      if (Array.isArray(measures)) {
        if (measures.length > 0) {
          _measures.qualitative = measures.filter(
            m => determineOutputMeasureType(m) === 'qualitative'
          );
          _measures.quantitative = measures.filter(
            m => determineOutputMeasureType(m) === 'quantitative'
          );
        } else {
          _measures.qualitative = [];
          _measures.quantitative = [];
        }
      }
    };

    this.getMeasuresAsObject = function() {
      return _measures;
    };

    const setPictureFile = function(value) {
      if (value instanceof Blob) {
        _pictureFile = value;
        // return blobToB64(value).then(b64encoded => {
        //     _picturePreview = b64encoded;
        // });
      } else if (typeof value === 'string' && value.length) {
        // _picturePreview = value;
        _pictureFile = b64ImageToBlob(value);
      } else {
        _pictureFile = null;
        // _picturePreview = '';
      }
    };

    // Define property setters and getters
    Object.defineProperties(this, {
      // Required data properties
      usercreationtime: {
        enumerable: true,
        configurable: false,
        writable: false,
        value: _timestamp
      },
      usercreationguid: {
        enumerable: true,
        configurable: false,
        writable: false,
        value: _guid
      },
      pictureFile: {
        enumerable: true,
        configurable: false,
        get: () => _pictureFile,
        set: setPictureFile
      },
      // User explictly added this report to the queue by calling enqueueReportDraft()
      sentByUser: {
        enumerable: true,
        configurable: false,
        writable: true,
        value: _sentByUser
      },
      isTrainingReport: {
        enumerable: true,
        configurable: false,
        writable: true,
        value: _isTraining
      },
      position: {
        //Position that the user may indicate on the map
        enumerable: true,
        configurable: false,
        get: () => (_position === null ? _userPosition : _position),
        set: pos => {
          if (pos === null) {
            _position = _userPosition;
          } else {
            _position = getPositionValue(pos);
          }
        }
      },
      userPosition: {
        enumerable: true,
        configurable: false,
        get: () => _userPosition,
        set: pos => {
          if (pos === null) {
            _position = null;
            _userPosition = null;
          } else {
            _userPosition = getPositionValue(pos);
          }
        }
      },
      measures: {
        enumerable: true,
        configurable: false,
        get: () => _measures.qualitative.concat(_measures.quantitative),
        set: _setMeasures
      },
      damages: {
        enumerable: true,
        configurable: false,
        get: () => _damages,
        set: damages => {
          if (Array.isArray(damages)) {
            _damages = damages;
          }
        }
      },
      resources: {
        enumerable: true,
        configurable: false,
        get: () => _resources,
        set: resources => {
          if (Array.isArray(resources)) {
            _resources = resources;
          }
        }
      },
      people: {
        enumerable: true,
        configurable: false,
        get: () => _people,
        set: people => {
          if (Array.isArray(people)) {
            _people = people;
          }
        }
      },
      category: {
        enumerable: true,
        configurable: false,
        get: () => _category,
        set: value => {
          _category = typeof value === 'string' ? value : null;
        }
      },
      hazard: {
        enumerable: true,
        configurable: false,
        get: () => _hazard,
        set: value => {
          _hazard = typeof value === 'string' ? value : null;
        }
      },
      visibility: {
        enumerable: true,
        configurable: false,
        get: () => _visibility,
        set: value => {
          if (['private', 'public'].includes(value)) {
            _visibility = value;
          }
        }
      }
    });
  }

  hasContent() {
    return this.getContentType() !== 'none';
  }

  getContentType() {
    const hasMeasures = this.measures.length > 0 ? 1 : 0; //(this.measures.qualitative.length > 0 && this.measures.qualitative.length > 0) ? 1 : 0;
    const hasDamages = this.damages.length > 0 ? 2 : 0;
    const hasResources = this.resources.length > 0 ? 4 : 0;

    const totalPeople = this.people.reduce((tot, peopleItem) => {
      if (isNumeric(peopleItem.quantity) && peopleItem.quantity > 0) {
        tot += peopleItem.quantity;
      }
      return tot;
    }, 0);
    const hasPeople = totalPeople > 0 ? 8 : 0;

    const sum = hasMeasures + hasDamages + hasResources + hasPeople;
    let contentType = 'none';
    switch (sum) {
      case 0: // empty
        break;
      case 1: // hasMeasures
        contentType = 'measure';
        break;
      case 2: // hasDamages
        contentType = 'damage';
        break;
      case 4: // hasResources
        contentType = 'resource';
        break;
      case 8: // hasPeople
        contentType = 'people';
        break;
      default:
        contentType = 'mixed';
        break;
    }
    return contentType;
  }

  /**
   * Check the minimum validity constraint before sending
   * @return boolean
   */
  isValid() {
    const hasContent = this.hasContent();
    const hasPosition = isValidPosition(this.userPosition);
    // const hasCategoryOrHazard = this.hazard !== null || this.category !== null;
    const hasPicture = this.pictureFile instanceof Blob;
    return hasContent && hasPosition && hasPicture;
  }

  /**
   * Convert the OutputReport to FormDat used for POST request
   * positions are converted to their WKT representations
   * @param String locale
   * @return FormData
   */
  toFormData(locale) {
    const pojoReport = this.toPlainObject(true);
    const flattened = flattenObject(pojoReport, locale);
    return Object.keys(flattened).reduce((fd, key) => {
      const value = flattened[key];
      if (value !== null && typeof value !== 'undefined') {
        fd.append(key, value);
      }
      return fd;
    }, new FormData());
  }

  toPlainObject(wktPositions = false) {
    const obj = {};
    for (let enumerableProperty in this) {
      const prop = this[enumerableProperty];
      if (typeof prop !== 'function') {
        if (
          wktPositions &&
          (enumerableProperty === 'position' || enumerableProperty === 'userPosition')
        ) {
          obj[enumerableProperty] = wktPosition(prop);
        } else if (enumerableProperty === 'measures') {
          obj[enumerableProperty] = this.getMeasuresAsObject();
          // if (locale) {
          //     obj[enumerableProperty].quantitative = obj[enumerableProperty].quantitative.map(m => ({ id: m.id, value: (parseFloat(m.value, 10)).toLocaleString(locale) }));
          // }
        } else {
          obj[enumerableProperty] = prop;
        }
      }
    }
    return obj;
  }

  /**
   * Convert the OutputReport to pojo (no WKT) and returns a JSON string
   * used by serializer
   * @return String
   */
  toString = () => {
    // will loose the information about the Blob image
    return JSON.stringify(this);
  };

  static fromObject(obj) {
    const or = new OutputReport(
      obj ? obj.usercreationtime : undefined,
      obj ? obj.usercreationguid : undefined
    );
    for (let key in obj) {
      if (key === 'measures') {
        or.measures = obj.measures.qualitative.concat(obj.measures.quantitative);
      } else {
        if (key !== 'usercreationtime' && key !== 'usercreationguid') {
          or[key] = obj[key];
        }
      }
    }
    return or;
  }
}

export { OutputReport };
export default OutputReport;
