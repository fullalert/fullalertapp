import Reducer from './dataSources/Reducer';
import withNewReport from './dataProviders/withNewReport';
import * as newReportActions from './dataSources/ActionCreators';

export default Reducer;
export { Reducer, withNewReport, newReportActions };