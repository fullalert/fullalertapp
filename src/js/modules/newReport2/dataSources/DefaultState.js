const STATE = {
  newReportDraft: null, // key to the draft of the new report (type OutputRepor) to be retrieved from localforage
  outputReports: [], // will hold all the reports ID/localforage keys that will be sent
  isSending: false,
  lastSentReportId: null
};

export default STATE;
