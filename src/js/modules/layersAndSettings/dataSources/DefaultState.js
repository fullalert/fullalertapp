const state = {
  remoteSettings: {},
  remoteLayers: {},
  remoteSettingsUpdating: true,
  settingLastUpdated: 0,
  activeIReactSettingNames: [],
  itemEditedList: [],
  activeCopernicusNames: [],
  activeLayersFromMapRequest: []
};

export default state;
