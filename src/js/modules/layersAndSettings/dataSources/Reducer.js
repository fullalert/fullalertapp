import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';

import {
  UPDATE_SETTINGS_OK,
  UPDATE_SETTINGS_FAIL,
  SETTINGS_UPDATING,
  UPDATE_LAYERS_OK,
  SET_ACTIVE_IREACT_SETTINGNAMES,
  ADD_EDITED_SETTING,
  SET_ACTIVE_IREACT_COPERNICUS,
  SET_ACTIVE_LAYERS_FROM_MAP_REQUEST,
  CLEAR_ALL_ACTIVE_LAYERS,
  RESET_ALL
  // DEPR
  // SET_ACTIVE_IREACT_TASKS
} from './Actions';

const mutators = {
  [RESET_ALL]: {
    ...DefaultState
  },
  [CLEAR_ALL_ACTIVE_LAYERS]: {
    activeIReactSettingNames: [],
    activeCopernicusNames: [],
    activeLayersFromMapRequest: []
  },
  [UPDATE_SETTINGS_OK]: {
    remoteSettingsUpdating: false,
    settingLastUpdated: Date.now(),
    remoteSettings: action => action.remoteSettings,
    itemEditedList: []
  },
  [UPDATE_SETTINGS_FAIL]: {
    remoteSettingsUpdating: false
  },
  [SETTINGS_UPDATING]: {
    remoteSettingsUpdating: true
  },
  [SET_ACTIVE_IREACT_SETTINGNAMES]: {
    activeIReactSettingNames: action => action.activeIReactSettingNames
  },
  [SET_ACTIVE_IREACT_COPERNICUS]: {
    activeCopernicusNames: action => action.activeCopernicusNames
  },
  [ADD_EDITED_SETTING]: {
    itemEditedList: (action, state) => {
      let itemEditedList = [...state.itemEditedList];
      const item = action.item;
      const index = itemEditedList.findIndex(a => a.key === item.key);
      if (index > -1) itemEditedList[index] = item;
      else itemEditedList.push(item);
      return itemEditedList;
    }
  },
  [UPDATE_LAYERS_OK]: {
    remoteLayers: action => action.remoteLayers
  },
  [SET_ACTIVE_LAYERS_FROM_MAP_REQUEST]: {
    activeLayersFromMapRequest: action => action.activeLayersFromMapRequest
  }
};

export default reduceWith(mutators, DefaultState);
