const STATE = {
    hasPermissions: false,
    isAvailableForPlatform: false,
    ready: false,
    pnReceived: [],
    pnError: null
};

export default STATE;