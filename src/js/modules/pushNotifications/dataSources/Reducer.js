import reduceWith from 'js/startup/reduceWith';
import DefaultState from './DefaultState';
import {
    AVAILABLE,
    ENABLED,
    SET_READY_STATUS,
    RECEIVED,
    READ,
    ERROR,
    CLEAR_PUSH_ERRORS,
    UNSUBSCRIBE_AND_RESET
} from './Actions';

const mutators = {
    [AVAILABLE]: {
        isAvailableForPlatform: action => action.isAvailableForPlatform
    },
    [ENABLED]: {
        hasPermissions: action => action.hasPermissions
    },
    [SET_READY_STATUS]: {
        ready: action => action.ready
    },
    [RECEIVED]: {
        pnReceived: (action, state) => {
            const notification = { ...action.notification, receivedTimeStamp: Date.now() };
            return [...state.pnReceived, notification];
        }
    },
    [READ]: {
        pnReceived: (action, state) => {
            const notification = action.notification;
            const receivedPNs = [...state.pnReceived];
            const index = receivedPNs.findIndex(n => n.receivedTimeStamp === notification.receivedTimeStamp);
            if (index === -1) {
                return receivedPNs;
            }
            else {
                //remove at index
                receivedPNs.splice(index, 1);
                return receivedPNs;
            }
        }
    },
    [ERROR]: {
        pnError: action => action.error
    },
    [CLEAR_PUSH_ERRORS]: {
        pnError: null
    },
    [UNSUBSCRIBE_AND_RESET]: {
        hasPermissions: false,
        isAvailableForPlatform: false,
        ready: false,
        pnReceived: [],
        pnError: null
    }
};

export default reduceWith(mutators, DefaultState);