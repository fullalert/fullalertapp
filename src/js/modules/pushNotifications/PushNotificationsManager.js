import { API } from 'ioc-api-interface';
import nop from 'nop';
import { IS_IOS } from 'js/utils/getAppInfo';

const NOTIFICATION_PLATFORM = {
  android: 'gcm',
  ios: 'apns'
};

/** See https://github.com/phonegap/phonegap-plugin-push/blob/d07fdf031e052f9c457319e6aaa9d7bfb72d1224/docs/PAYLOAD.md#android-behaviour
 * {
    "data" : {
        "title": "Test Notification",
        "body": "This offer expires at 11:30 or whatever",
        "notId": 10,
        "surveyID": "ewtawgreg-gragrag-rgarhthgbad"
    }
}
 */
const PN_TEMPLATE = {
  gcmTemplate: {
    body: '{"data":{"message":"$(message) + $(from)", "title":$(title), "notId":$(notificationId)}}'
  }
  // Understand https://github.com/phonegap/phonegap-plugin-push/blob/d07fdf031e052f9c457319e6aaa9d7bfb72d1224/docs/PAYLOAD.md#ios-behaviour
  /* ,
    'aps': {
        'alert': 'Test sound',
        'sound': 'default'
    } */
};

// function defaultMissingPermisssionHandler(/*dict*/) {
//     //PROVISIONAL - SHOW ALERT
//     alert('You need to enable the push notification permissions for this app in order to receive real-time messages.');
// }

class PushNotificationsManager {
  settings = {
    android: {
      icon: 'ireact_pn', //name of the alternate icon in platforms/android/res/drawable/
      iconColor: '#FF6E40',
      ledColor: [0, 255, 255, 0],
      sound: 'true'
    },
    browser: {
      pushServiceURL: 'http://push.api.phonegap.com/v1/push'
    },
    ios: {
      ios: {
        sound: 'true',
        alert: 'true',
        badge: 'true',
        clearBadge: 'true'
      }
    }
  };

  push = null;

  ready = false;

  hasPermissions = false;

  constructor() {
    // Read-only prop
    Object.defineProperty(this, 'isAvailableForPlatform', {
      get: () =>
        IS_CORDOVA &&
        window.device &&
        !window.device.isVirtual &&
        typeof window.PushNotification !== 'undefined'
    });
  }

  checkPermissions = function() {
    if (this.isAvailableForPlatform) {
      return new Promise(success => {
        PushNotification.hasPermission(data => {
          this.hasPermissions = data.isEnabled;
          success(data.isEnabled);
        });
      });
    } else {
      return Promise.resolve(true);
    }
  };

  _checkIOSPushNotificationPermissions = () => {
    return new Promise((success, fail) => {
      cordova.plugins.diagnostic.getRemoteNotificationsAuthorizationStatus(
        function(status) {
          let enabled = false;
          switch (status) {
            case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
              console.log('check permissions IOS: Permission not yet requested');
              break;
            case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
              console.log('heck permissions IOS: Permission denied');
              break;
            case cordova.plugins.diagnostic.permissionStatus.GRANTED:
              console.log('heck permissions IOS: Permission granted');
              enabled = true;
              break;
            default:
              break;
          }
          success(enabled);
        },
        function(error) {
          console.error('heck permissions IOS: The following error occurred: ' + error);
          fail(error);
        }
      );
    });
  };

  _requestIOSPushNotificationPermissions = () => {
    return new Promise((success, fail) => {
      cordova.plugins.diagnostic.requestRemoteNotificationsAuthorization({
        successCallback: () => {
          console.log('User granted permissions for push notifications');
          success(true);
        },
        errorCallback: e => {
          console.error('User denied permissions for push notifications', e);
          fail();
        },
        types: [
          cordova.plugins.diagnostic.remoteNotificationType.ALERT,
          cordova.plugins.diagnostic.remoteNotificationType.SOUND,
          cordova.plugins.diagnostic.remoteNotificationType.BADGE
        ],
        omitRegistration: false
      });
    });
  };

  /**
   * Append events for the plugin to work
   */
  _registerPNAPI = () => {
    return new Promise((success, fail) => {
      this.push = PushNotification.init(this.settings);

      this.push.on('registration', async data => {
        console.log('registration event: ' + data.registrationId);

        const oldRegId = localStorage.getItem('registrationId');
        if (oldRegId !== data.registrationId) {
          // Save new registration ID
          localStorage.setItem('registrationId', data.registrationId);
          // Post registrationId to your app server as the value has changed
          try {
            const api = API.getInstance();
            if (!api) {
              const errMsg = 'I-REACT api not initialized!';
              console.warn(errMsg);
              fail(new Error(errMsg));
            } else {
              const response = await api.pushNotifications.installation(
                NOTIFICATION_PLATFORM[cordova.platformId],
                data.registrationId,
                PN_TEMPLATE
              );
              console.debug('RESPONSE OF API REGISTRATION', response);
              success(response.result);
            }
          } catch (errorResponse) {
            fail(errorResponse.error ? errorResponse.error : errorResponse);
          }
        } else {
          success({
            //Same we would get from the server
            success: true,
            message: 'Device registered successfully'
          });
        }
      });

      this.push.on('notification', data => {
        console.log(
          '%c PUSH NOTIFICATION RECEIVED',
          'color: green; font-weight: bold; background-color: whitesmoke;',
          data
        );
        if (IS_IOS) {
          // do something with the push data
          // then call finish to let the OS know we are done
          this.push.finish(
            () => console.log('IOS: processing of push data is finished'),
            () =>
              console.error(
                'IOS: something went wrong with push.finish for ID = ' + data.additionalData.notId
              ),
            data.additionalData.notId
          );
        }
        let notification = data;
        if (typeof notification === 'string') {
          try {
            notification = JSON.parse(notification);
          } catch (err) {
            console.warn('Invalid JSON payload in Push Notification', err);
          }
        }

        this.notificationHandler(notification);
      });
      this.push.on('error', err => {
        console.error('Push Notification Error', err);
        this.notificationErrorHandler(err);
      });
    });
  };

  initialize = async (notificationHandler = nop, notificationErrorHandler = nop) => {
    this.notificationHandler = notificationHandler;
    this.notificationErrorHandler = notificationErrorHandler;

    if (IS_CORDOVA && !window.device.isVirtual && window.PushNotification) {
      if (this.ready) {
        return Promise.resolve({ registered: true });
      } else {
        try {
          let pnEnabled = false;
          if (IS_IOS) {
            pnEnabled = await this._checkIOSPushNotificationPermissions();
            if (!pnEnabled) {
              pnEnabled = await this._requestIOSPushNotificationPermissions();
            }
          } else {
            pnEnabled = await this.checkPermissions();
          }
          if (pnEnabled) {
            const result = await this._registerPNAPI();
            this.ready = result && result.success ? true : false;
            console.debug('RESULT OF API REGISTRATION', result);
            if (result.success) {
              return Promise.resolve({ registered: true });
            } else {
              return Promise.reject(new Error(result.message));
            }
          } else {
            // this.missingPermissionsHandler();
            return Promise.reject(new Error('Permissions denied for push notifications'));
          }
        } catch (ex) {
          return Promise.reject(ex);
        }
      }
    } else {
      return Promise.resolve({ registered: false });
    }
  };

  unsubscribeAll = () => {
    console.log('Unsubscribing from ALL Push Notifications...');
    if (!IS_CORDOVA) {
      console.log('Unsubscribed from ALL Push Notifications...');
      return Promise.resolve();
    } else {
      return new Promise((success, fail) => {
        localStorage.removeItem('registrationId');
        if (this.push) {
          this.push.unregister(() => {
            console.log('Unsubscribed from ALL Push Notifications...');
            success();
          }, fail);
        }
        this.ready = false;
      });
    }
  };
}

const pnManager = new PushNotificationsManager();

export function getPnManager() {
  return pnManager;
}

export default getPnManager;
