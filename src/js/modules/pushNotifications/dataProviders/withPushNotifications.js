import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as ActionCreators from '../dataSources/ActionCreators';


const select = createStructuredSelector({
    pushNotifications: state => state.pushNotifications.pnReceived,
    pushNotificationsReady: state => state.pushNotifications.ready,
    lastNotification: state => state.pushNotifications.pnReceived.length > 0 ? state.pushNotifications.pnReceived[state.pushNotifications.pnReceived.length - 1] : null,
    hasPushNotifications: state => state.pushNotifications.pnReceived.length > 0,
    pushNotificationsAvailableForPlatform: state => state.pushNotifications.isAvailableForPlatform,
    pushNotificationsPermissionsEnabled: state => state.pushNotifications.hasPermissions,
    pushNotificationsError: state => state.pushNotifications.pnError
});

export default connect(select, ActionCreators);