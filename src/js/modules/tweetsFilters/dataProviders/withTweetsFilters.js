import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FILTERS, hasGeolocation, isValid, hasPicture } from '../dataSources/filters';

import * as ActionCreators from '../dataSources/ActionCreators';
import { getActiveFiltersDefinitions } from 'js/utils/getActiveFiltersDefinitions';

export const FilterMap = {
  hasGeolocation,
  isValid,
  // isRetweet,
  hasPicture
};

const select = createStructuredSelector({
  tweetsFiltersDefinition: state =>
    getActiveFiltersDefinitions(state.tweetsFilters.filters, FilterMap, FILTERS),
  tweetsFilters: state => state.tweetsFilters.filters,
  tweetsSorter: state => state.tweetsFilters.sorter
});

export default connect(
  select,
  ActionCreators
);
