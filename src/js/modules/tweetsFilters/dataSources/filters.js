const _has_geolocation = ['!=', 'positionSource', 'none'];

export const hasGeolocation = {
  _has_geolocation
};

const _is_valid = ['==', 'valid', true];

export const isValid = {
  _is_valid
};

// const _is_retweet = ['==', 'retweet', true];

// export const isRetweet = {
//   _is_retweet
// };

const _has_picture = ['!=', 'pictureURL', null];

export const hasPicture = {
  _has_picture
};

export const FILTERS = {
  hasGeolocation: Object.keys(hasGeolocation),
  isValid: Object.keys(isValid),
  // isRetweet: Object.keys(isRetweet),
  hasPicture: Object.keys(hasPicture)
};

export const SORTERS = {
  // _last_updated_desc: (f1, f2) =>
  //   new Date(f2.properties.lastModified).getTime() - new Date(f1.properties.lastModified).getTime(),
  // _last_updated_asc: (f1, f2) =>
  //   new Date(f1.properties.lastModified).getTime() - new Date(f2.properties.lastModified).getTime(),
  _date_time_desc: (f1, f2) =>
    new Date(f2.properties.timestamp).getTime() - new Date(f1.properties.timestamp).getTime(),
  _date_time_asc: (f1, f2) =>
    new Date(f1.properties.timestamp).getTime() - new Date(f2.properties.timestamp).getTime()
  // TODO add _share_desc, _share_asc, _upvote_desc, _upvote_asc, _downvote_desc, _downvote_asc
};

export const availableFilters = Object.keys(FILTERS);
export const availableSorters = Object.keys(SORTERS);
