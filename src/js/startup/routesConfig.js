import {
  // UserRedirectLock,
  Login,
  Logout,
  Registration,
  TermsAndConditions,
  SMSValidation,
  EmailValidation,
  Home,
  MapTabs,
  Dashboard,
  Social,
  ReportDetails,
  // EventDetails,
  EmergencyCommunicationDetails,
  Achievements,
  // AchievementsRedirectLock,
  CongratsPage,
  Learner,
  ProgressPage,
  CardsPlayer,
  UserProfile,
  //
  About,
  Help,
  Settings,
  TimeWindowSetting,
  LanguageSettings,
  MapSettings,
  PasswordForgot,
  NewReportCapture,
  WebcamCapture,
  NewReport,
  // NewReportRedirectLock,
  HazardSelect,
  PickPositionFromMap,
  ContentSelect,
  CategorySelect,
  ContentValue,
  Missions,
  // MissionDetails,
  // MissionTaskDetails,
  LayersLegend,
  Wearable
} from 'js/components';
import { IS_ANDROID, IS_IOS } from 'js/utils/getAppInfo';

// export const DEFAULT_PAGE_AUTHENTICATED = '/tabs/map';
export const DEFAULT_PAGE_AUTHENTICATED = '/home';
export const DEFAULT_PAGE_UNAUTHENTICATED = '/';

export function routesConfig(fileAppDirectory = null) {
  // Route configuration
  let config = [
    {
      path: '/login',
      redirect: DEFAULT_PAGE_UNAUTHENTICATED
    },
    {
      path: DEFAULT_PAGE_UNAUTHENTICATED,
      component: Login,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      /* redirect conditions
        loggedIn: undefined --> any, true: only if loggedIn, false: only if not loggedIn
        roles: whitelist of user roles (if empty, any role)

        */
      requirements: {
        loggedIn: false,
        roles: []
      }
    },
    {
      path: '/home',
      component: Home,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/social/:id',
      component: Social,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/dashboard',
      component: Dashboard,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/terms',
      component: TermsAndConditions,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: false,
        roles: []
      }
    },
    {
      path: '/registration',
      component: Registration,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: false,
        roles: []
      }
    },
    {
      path: '/smsvalidation',
      component: SMSValidation,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: false,
        roles: []
      }
    },
    {
      path: '/emailvalidation',
      component: EmailValidation,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: false,
        roles: []
      }
    },
    {
      path: '/passwordForgot',
      component: PasswordForgot,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: false,
        roles: []
      }
    },
    {
      path: '/logout',
      component: Logout,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    //HOME
    {
      path: '/tabs',
      redirect: '/tabs/map'
    },
    {
      path: '/tabs/',
      redirect: '/tabs/map'
    },
    {
      path: '/tabs/:tabname',
      component: MapTabs,
      transition: null,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    // REPORT DETAILS
    {
      path: '/reportdetails/:id',
      component: ReportDetails,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    // EVENT DETAILS
    // {
    //   path: '/eventdetails/:id',
    //   component: EventDetails,
    //   transition: {
    //     appear: 'slideDownIn-appear',
    //     appearActive: 'slideDownIn-appear-active',
    //     enter: 'slideDownIn-enter',
    //     enterActive: 'slideDownIn-enter-active',
    //     exit: 'slideUpOut-exit',
    //     exitActive: 'slideUpOut-exit-active'
    //   },
    //   transitionTimeout: 200,
    //   requirements: {
    //     loggedIn: true,
    //     roles: []
    //   }
    // },
    // EMERGENCY COMMUNICATION DETAILS
    {
      path: '/emcommdetails/:id',
      component: EmergencyCommunicationDetails,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    // GAMIFICATION - ACHIEVEMENTS
    {
      path: '/congratulations',
      component: CongratsPage,
      transition: {
        appear: 'slideDownIn-appear',
        appearActive: 'slideDownIn-appear-active',
        enter: 'slideDownIn-enter',
        enterActive: 'slideDownIn-enter-active',
        exit: 'slideUpOut-exit',
        exitActive: 'slideUpOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['citizen']
      }
    },
    {
      path: '/achievements',
      redirect: '/achievements/personal/skills'
    },
    {
      path: '/achievements/:tabname/:cardname?',
      component: Achievements, //AchievementsRedirectLock(Achievements),
      transition: 'fade',
      requirements: {
        loggedIn: true,
        roles: ['citizen']
      }
    },
    // GAMIFICATION - QUIZ AND TIPS
    {
      path: '/learner',
      component: Learner,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['citizen']
      }
    },
    {
      path: '/learner/:type',
      component: ProgressPage,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['citizen']
      }
    },
    {
      path: '/editprofile',
      component: UserProfile,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    // {
    //   path: '/learner/quizzes',
    //   component: Quizzes,
    //   transition: {
    //     appear: 'slideLeftIn-appear',
    //     appearActive: 'slideLeftIn-appear-active',
    //     enter: 'slideLeftIn-enter',
    //     enterActive: 'slideLeftIn-enter-active',
    //     exit: 'slideRightOut-exit',
    //     exitActive: 'slideRightOut-exit-active'
    //   },
    //   transitionTimeout: 200,
    //   requirements: {
    //     loggedIn: true,
    //     roles: ['citizen']
    //   }
    // },
    {
      path: '/learner/:type/cards',
      component: CardsPlayer,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['citizen']
      }
    },
    // Preferences
    {
      path: '/settings',
      component: Settings,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/settings/timewindow',
      component: TimeWindowSetting,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/settings/language',
      component: LanguageSettings,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/settings/map',
      component: MapSettings,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/settings/wearable',
      component: Wearable,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['pro']
      }
    },
    // Report creation
    {
      path: '/startCapture',
      component: NewReportCapture,
      transition: null,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/webcamCapture',
      component: WebcamCapture,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/newreport',
      component: NewReport,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/newreport/pickReportPosition',
      component: PickPositionFromMap,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/newreport/hazardSelect',
      component: HazardSelect,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/newreport/hazardSelect/contentSelect',
      component: ContentSelect,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/newreport/categorySelect/:contentType',
      component: CategorySelect,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/newreport/contentValue/:contentType',
      component: ContentValue,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    // Missions
    {
      path: '/missions',
      component: Missions,
      transition: {
        appear: 'slideRightIn-appear',
        appearActive: 'slideRightIn-appear-active',
        enter: 'slideRightIn-enter',
        enterActive: 'slideRightIn-enter-active',
        exit: 'slideLeftOut-exit',
        exitActive: 'slideLeftOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['pro']
      }
    },
    // Missions Details
    {
      path: '/missiondetails/:id',
      component: Missions, //MissionDetails,
      transition: {
        appear: 'slideRightIn-appear',
        appearActive: 'slideRightIn-appear-active',
        enter: 'slideRightIn-enter',
        enterActive: 'slideRightIn-enter-active',
        exit: 'slideLeftOut-exit',
        exitActive: 'slideLeftOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['pro']
      }
    },
    // Missions Task Details
    {
      path: '/missiondetails/:id/:taskId',
      component: Missions, //MissionTaskDetails,
      transition: {
        appear: 'slideRightIn-appear',
        appearActive: 'slideRightIn-appear-active',
        enter: 'slideRightIn-enter',
        enterActive: 'slideRightIn-enter-active',
        exit: 'slideLeftOut-exit',
        exitActive: 'slideLeftOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: ['pro']
      }
    },
    {
      path: '/layerslegend',
      component: LayersLegend,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    // Extras
    {
      path: '/help',
      component: Help,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    },
    {
      path: '/about',
      component: About,
      transition: {
        appear: 'slideLeftIn-appear',
        appearActive: 'slideLeftIn-appear-active',
        enter: 'slideLeftIn-enter',
        enterActive: 'slideLeftIn-enter-active',
        exit: 'slideRightOut-exit',
        exitActive: 'slideRightOut-exit-active'
      },
      transitionTimeout: 200,
      requirements: {
        loggedIn: true,
        roles: []
      }
    }
  ];

  if (IS_CORDOVA) {
    if (IS_ANDROID) {
      config = [
        {
          path: '/android_asset/www/index.html',
          redirect: DEFAULT_PAGE_UNAUTHENTICATED
        },
        ...config
      ];
    }
    if (IS_IOS && fileAppDirectory) {
      config = [
        {
          path: `${fileAppDirectory.replace(/^file:\/\//, '')}www/index.html`,
          redirect: DEFAULT_PAGE_UNAUTHENTICATED
        },
        ...config
      ];
    }
  }
  return config;
}

export default routesConfig;
