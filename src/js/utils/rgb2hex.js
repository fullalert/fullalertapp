const IS_RGBA = /^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i;
const IS_HEX = /^#((\d){8}|(\d){6}|(\d){4}|(\d){3})$/i;

/**
 * Credits https://jsfiddle.net/Mottie/xcqpF/1/light/
 * @param {string} rgb
 */
export function rgb2hex(rgb) {
  if (IS_HEX.test(rgb)) {
    return rgb;
  } else {
    rgb = rgb.match(IS_RGBA);
    return rgb && rgb.length === 4
      ? '#' +
          ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
          ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
          ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2)
      : '';
  }
}
