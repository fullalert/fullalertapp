import { useState, useContext, useEffect } from 'react';
import { StoreContext } from 'js/startup/app.store';

function useStore() {
  const store = useContext(StoreContext);
  if (!store) {
    throw new Error(
      'Store not found. Wrap your application within a StoreProvider and pass it the appropriate store prop.'
    );
  }
  return store;
}

export function useRedux(selector = state => state, mapDispatch = {}) {
  const store = useStore();
  const getState = () => selector(store.getState());
  // https://reactjs.org/docs/hooks-reference.html#lazy-initial-state
  const [state, setState] = useState(getState);

  useEffect(() => {
    const unsubscribe = store.subscribe(() => {
      setState(getState());
    });

    return () => {
      unsubscribe();
    };
  });

  const mappedMethods = Object.keys(mapDispatch).reduce((r, n) => {
    return {
      ...r,
      [n]: (...args) => store.dispatch(mapDispatch[n].apply(undefined, args))
    };
  }, {});

  // Sometimes state is null, no matter the initial value, especially when using the inspector
  // this avoid breaking stuff
  let _state = state ? state : getState();
  return [_state, mappedMethods, store.dispatch];
}
