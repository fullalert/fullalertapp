import { useRedux } from 'js/hooks/use-redux.hook';
import { localeSelector, localeActionCreators } from 'ioc-localization';
export function useLocale() {
  const [selector, actions, dispatch] = useRedux(localeSelector, localeActionCreators);

  return [selector, actions, dispatch];
}
