import { useRedux } from 'js/hooks/use-redux.hook';
import { apiSelector, apiSettingsActionCreators } from 'ioc-api-interface';

export function useApiSettings() {
  const [selector, actions, dispatch] = useRedux(apiSelector, apiSettingsActionCreators);

  return [selector, actions, dispatch];
}
