SE NON SI ACCETTANO I TERMINI DESCRITTI IN SEGUITO, NON SI DEVE UTILIZZARE L'APP MOBILE I-REACT.

# I-REACT Termini e Condizioni di Servizio

Selezionando "Accetto", dichiari di aver **letto**, **capito** e **accettato** sia _[l'informatva sulla privacy](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=it)_ che _[i termini di servizio](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=it)_.

_I-REACT mira a ridurre al minimo l'impatto dei disastri consentendo ai cittadini di condividere informazioni utili in tempo reale per tenere informata la comunità locale sulle emergenze e allo stesso tempo per ricevere informazioni relative ai disastri dai sistemi esistenti, compresi i social media (Twitter). I-REACT promuove la consapevolezza e l'impegno dei cittadini nella gestione delle emergenze implementando un approccio gamificato._  
_Gestiamo i tuoi dati personali in conformità con il Regolamento generale sulla protezione dei dati (GDPR) (EU) 2016/679 ("GDPR"). Nota che utilizzerai l'app I-REACT sotto la tua responsabilità._  
_Per diventare un membro della famiglia I-REACT devi leggere e accettare la nostra Informativa sulla privacy e i nostri Termini di servizio. Clicca sui link qui sotto se vuoi saperne di più._

- Leggi i nostri [Termini di servizio completi qui](https://ireactfe.azurewebsites.net/tos.html?type=terms&lang=it).
- Leggi la nostra completa [Informativa sulla privacy completa qui](https://ireactfe.azurewebsites.net/tos.html?type=privacy&lang=it).
