async function main() {
  console.debug(`Loading ${TITLE}`);
  try {
    const loadAppModule = await import('js/loadApp');
    const launch = loadAppModule.default;
    launch();
  } catch (err) {
    console.error('Error loading loadAppModule', err);
  } finally {
    console.debug('🚀 Module launched');
  }
}

if (IS_CORDOVA) {
  document.addEventListener('deviceready', async () => {
    console.debug('Device Ready... starting app', cordova.file.applicationDirectory);
    main();
  });
} else {
  main();
}
