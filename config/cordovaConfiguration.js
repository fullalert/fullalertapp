const commons = require('./commons');
const path = require('path');
const fs = require('fs-extra');
const pkg = require('../package.json');

const { mobileAppFolder, defaultAppFolder } = pkg.config;

/**
 * Directory structure
 * mobile-app
 *    /default
 *       /www (gen. by webpack)
 *       /assets
 *       /cordovaScripts (symlink)
 *       config.xml
 */

function getMobileAppRootFolderForConfig(appDirectory) {
  return path.join(appDirectory, mobileAppFolder, defaultAppFolder);
}

async function prepareCordovaDirectoryStructure(appDirectory) {
  const destDir = getMobileAppRootFolderForConfig(appDirectory);
  const appFolder = defaultAppFolder;
  const assetsSrcDir = path.join(appDirectory, 'mobileAppConfigs', appFolder);

  try {
    await fs.ensureDir(destDir);
    // Copy assets in dest folder
    await fs.copy(assetsSrcDir, destDir);
    // Symlink cordova script in dest folder
    await fs.ensureSymlink(
      path.join(appDirectory, 'cordovaScripts'),
      path.join(destDir, 'cordovaScripts')
    );
  } catch (err) {
    throw err;
  }

  return destDir;
}

async function getConfiguration(appDirectory, envConfig, contentSecurityPolicy, appMountId) {
  const mobileAppRootFolder = await prepareCordovaDirectoryStructure(appDirectory);
  const outputPath = path.join(mobileAppRootFolder, envConfig.DEST);
  const htmlPlugin = commons.getHtmlConfig(
    appDirectory,
    envConfig,
    contentSecurityPolicy,
    appMountId
  );
  const copyPlugins = commons.getCopyConfig(
    appDirectory,
    outputPath,
    envConfig,
    htmlPlugin.FAVICONS
  );

  return {
    outputPath,
    htmlPlugin: htmlPlugin.plugin,
    copyPlugins
  };
}

module.exports = { getConfiguration, mobileAppFolder, getMobileAppRootFolderForConfig };
