module.exports = {
  extends: 'react-app',
  globals: {
    //General stuff
    componentHandler: true,
    screen: true,
    navigator: true,
    cordova: true,
    mapboxgl: true,
    Windows: true,
    Microsoft: true,
    PushNotification: true,
    //Global Config
    PUBLIC_PATH: true,
    CORDOVA_APP_ATTRIBUTES: true,
    IS_CORDOVA: true,
    TITLE: true,
    VERSION: true,
    PKG_NAME: true,
    DESCRIPTION: true,
    ENVIRONMENT: true,
    IS_PRODUCTION: true,
    BUILD_DATE: true,
    APP_MOUNT_ID: true,
    SUPPORTED_LANGUAGES: true,
    //User Avatars
    AVATAR_FILE_NAMES: true,
    //Mapbpx - OSM Tiles
    OSM_TILES_KEY: true,
    CLIENT_TYPE: true,
    SOCIAL_BACKEND: true,
    IREACT_BACKEND: true,
    IREACT_BACKEND_TYPE: true,
    PRIVACY_URL: true,
    TOS_URL: true,
    WKT_BACKEND: true,
    RECAPTCHA_SITEKEY: true,
    APP_INSIGHTS_INSTR_KEY: true,
    MAP_SERVER_APIKEY: true,
    MAP_SERVER_URL: true,
    MAP_STYLES_URL: true,
    GEOSERVER_URL: true,
    USE_WMTS: false,
    //GEONAMES
    GEONAMES_API_CREDENTIALS: true,
    //CORDOVA PLUGINS
    Connection: true
  },
  settings: {
    react: {
      version: 'detect'
    }
  },
  rules: {
    indent: ['off', 2],
    'linebreak-style': ['off'],
    'jsx-a11y/img-has-alt': ['off'],
    'jsx-a11y/href-no-hash': 'off',
    'jsx-a11y/anchor-is-valid': ['warn', { aspects: ['invalidHref'] }],
    'import/first': ['warn'],
    'import/no-webpack-loader-syntax': ['warn'],
    quotes: [
      'error',
      'single',
      {
        avoidEscape: true
      }
    ],
    semi: ['error', 'always']
  }
};
