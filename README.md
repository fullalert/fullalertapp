# I-REACT Open Core Cordova App

Main repository for the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/) Cordova App.

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

## Service dependencies

In order to work properly, the app needs the following services to be active and reachable:

- I-REACT Open Core Backend
- SOCIAL Analysis API
- Geonames.org subscription for reverse geocoding
- Mapbox subscription or Mapbox-compatible tile server

You must have access to a version of the **I-REACT Open Core** backend deployed and reachable at a `https` URL.
Please download and deploy the [**I-REACT Open Core Backend**](https://bitbucket.org/mobilesolutionsismb/ioc-backend) from [this link](https://bitbucket.org/mobilesolutionsismb/ioc-backend/raw/997c9d7bf9b5119e113dbd5df132364ef2d71d7e/release/ioc-backend.zip)

In order to have the Social Analysis API working you must also have access to a version of the social analyisis tool from [CELI](https://www.celi.it/). Please [contact CELI](mailto:info@celi.it) and ask for purchasing their solution.

Optionally, you may want to deploy your own mapbox-compatible tile server for base layers, like http://tileserver.org/. Otherwise Mapbox subscription will do just fine.

Furthermore you may add Microsoft Application Insights in order to get stats and error reporting. See the comment in env.example file.

## Installation and Develeopment Dependencies

In order to generate the mobile app packages, you will need Android SDK and Xcode for Android and iOS app build (the latter is Mac only)

### Requirements

- Java SDK 8
- Android SDK 27 - make sure no other higher SDK interferes with this or Push Notifications won't work. Use Android SDK Manager to install this exact version
- cordova 8 (`npm i -g cordova@8.x`) - higher cordova versions do not seem to work correctly

### App Setup

These steps will add all required JavaScript dependencies

```bash
npm install
npm i -g cordova # in case you don't have it installed
```

## Starting the dev server

Create a `.env` file from the template and edit it with required values

```bash
cp .env.example .env
```

Start the dev server

```bash
npm start
```

Browse to `http://localhost:8080`  
Then edit your code. Webpack will reload it.

## Cordova Project setup

The app configurations and assets are in the `mobileAppConfigs` subfolders.
With this source code it is possible to generate from 1 to many separate apps:
in this setup you get the "default" I-REACT Open Core app (org.openCore.iReact).
You may add more configurations if you whish, by cloning and renaming this folder and its parameters,
and editing the build scripts accordingly. This allows, for instance, of having different brand assets
for each configuration.  
Anyway, the folder in `mobileAppConfigs` get copied in `mobile-app/<config>`, e.g. `mobile-app/default`, where
the cordova project si created.

### Quickstart

These steps describe how compile and launch the _default_ app for debugging.
Connect your Android phone, check it is working and enabled for development with `adb devices`

Then run the followin commands in order

```bash
npm run setup-cordova # setup the default I-REACT app cordova project in mobile-app/default
npm run cordova-prepare # download all cordova dependencies
npm run run-android # run the app on the device
```

**caution**: on Windows it may not recognize the global installation of cordova when launching scripts from npm. In this case just

1. `cd mobile-app\<default>` (select which is appropriate)
2. `cordova prepare`
3. `cordova run android --device` (or `cordova build android`, whatever you need to do...)

Open `chrome://inspect` in order to open a Chrome Dev Tools tab and inspect the mobile app.

Use the very same steps above when updating the app code and/or the `config.xml` file in `mobileAppConfg/default`

### Cordova Scripts explanation

Detailed explanation of all cordova-related scripts in `package.json`

- `setup-cordova`: build the bundle of the default app, with JS debug symbols, and create a cordova project in `mobile-app/default` if not existing, copying the `config.xml` file from `mobileAppConfigs/default` and the bundle in `mobile-app/default/www` folder, otherwise updating the `config.xml` and the `www` folder
- `setup-cordova-prod`: same as above, but the bundle is minified (no JS symbols, so difficult to inspect)
- `cordova-prepare`: runs `cordova prepare` in the default app folder, thus copying the `www` folder in each platform folder. If dependencies like plugins and engines are missing with respect to the `config.xml` file, it will download them.
- `build-android`: compile the default app with native debug symbols, thus making it inspectable from Chrome (Android) or Safari (iOS). You cannot use the apk for store deployment, but you can deploy to a development device using cli tools like `adb install <path to apk>`
- `run-android`: same as `build-android` but runs the app on the connected android device
- `compile-production-android`: compiles and signs the app for the PlayStore. You can upload the apk to the Google PlayStore making sure the android code version is bigger than the one already on store

### iOS build

Since the cordova command line do not work as easily with iOS as for Android, it is better to open the project with XCode from `mobileAppConfigs/default/platforms/ios/I-REACT Open Core.xcworkspace`, edit the information in XCode and follow regular debug/deploy instructions.

### Cordova app icon and splashscreen

If you nees order to generate new assets (icons and splashscreens), use [cordova-icon](https://github.com/AlexDisler/cordova-icon) and [cordova-splash](https://github.com/AlexDisler/cordova-splash).  
This is required only at first launch.

## Browser

These instructions allow for building the app web ui, which is useful for testing the feture in a browser. It is a web application and does not rely on cordova, but cannot also access some native features such as bluetooth or push notifications.

```bash
npm run build-debug # debug build unminified with source maps
npm run build # production build, minified
```

### Deploying APP UI

The App UI can be deployed for UI visual debugging on any static web server as a SPA.

### Apache

You will need an Apache http2 web server configured for serving static folders and a ssh acces for deployment, preferably with ssh key setup (otherwise you will have to insert the password on prompt).

Then in the .env file you need to set the following 3 variables

1. `DEPLOY_PATH`: remote url path (e.g. you want the app on https://yourserver/some/path/FOLDER_NAME --> `/some/path`)
1. `NIGHTLY_DEPLOY_REMOTE_PATH`: physical path on the machine were the dist folder will be unzipped (e.g. `/var/www/htm/path/to/folder`)
1. `DEPLOY_USER`: your ssh user, e.g. username@servername.com

Then run

```bash
npm run deploy-nightly
```

It will create a folder with the following folder name

`build_DATE_TIME_SUFFIX`

The suffix is `TP` for test backend, `DEV` when it points to dev backend

It will also add a `.htaccess` file so that Apache knows that it has to serve the page as SPA.

### Azure Web Application

You need to **unset** the `DEPLOY_PATH`, `NIGHTLY_DEPLOY_REMOTE_PATH` and `DEPLOY_USER`
variables in the .env file (just comment them using #)

Then run

```bash
npm run build
npm run deploy-ui <PATH_TO_PUBSETTINGS>
```
